from django.http import HttpResponse, HttpResponseNotFound
from django.template import Context, loader, Template
from assist.models import *
from crawl.models import *
from twitter.models import *
from remote.models import *
from django.db.models import Q
from django.conf import settings
from os.path import join, exists
from os import stat
import sys, os, htmlentitydefs, hashlib, json, re, sched, time, threading, random, urllib
from utils import *
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from assist import mail, reminder, register
from schedule.views import calendar
import traceback, pdb, urllib
import crawl.procmail
import ajax_feeds
from ajax_feeds import get_widget_manager
from lib.csrf.middleware import csrf_exempt, csrf_required
import payment_client
from django.core.urlresolvers import reverse


@csrf_required
def sf_auth(request):
    # TODO -- add Salesforce API support to fetch Contact details from Salesforce

    try:
        alias = request.session['alias']
        
        log('__sf_auth__ begin')
        enabled = bool(int(request.POST['enabled']))
        
        log("__sf_auth__ enabled: ", enabled)
            
        if enabled:            
            sf_settings = {\
                'username':         request.POST['username'].strip(),
                'password':         request.POST['password'].strip(),
                'token':            request.POST['token'].strip()
            }
        
            for key, value in sf_settings.items():
                if key != 'password':
                    log("__sf_auth__: sf_settings %s = %s" % (key, value))
            
            # TODO - store these in a new settings_json structure for Alias
            
            #sf_user.username = sf_settings['username']
            #sf_user.password_aes = CRYPTER.Encrypt(sf_settings['password'])      
            #sf_user.token_aes = CRYPTER.Encrypt(sf_settings['token'])      
                        
            try:
                #sf_user.login()
                #sf_user.enabled = enabled
                #sf_user.valid = True
                #sf_user.save()
                
                log('__sf_auth__: valid')
                    
                d = {
                    'success': True,
                    'msg': _('You are now linked to your Salesforce.com account')
                }

            except Exception, e:
                if type(e) == beatbox._beatbox.SoapFaultError:
                    msg = _('Cannot log into your SalesForce account. ')
                    if e.faultCode == 'INVALID_LOGIN':
                        msg += _('Please try valid Salesforce.com credentials.')
                    elif e.faultCode == 'API_DISABLED_FOR_ORG':
                        msg += _('Please contact Salesforce to enable API access on your account.')
                    msg += '\n\n' + e.faultString
                else:
                    msg = _('Please try valid Salesforce.com credentials.')
                    
                #sf_user.enabled = False
                #sf_user.valid = False
                #sf_user.save()
                
                d = {
                    'success': False,
                    'msg': msg
                }
                
            result = simplejson.dumps(d)
            return HttpResponse(result)
        else:
            #sf_user.enabled = False
                # Leave alone the sf_valid and other settings in this case
            #sf_user.save()
            
            d = {
                'success': True,
                'msg': _('Salesforce.com checking is now disabled')
            }
            result = simplejson.dumps(d)
            return HttpResponse(result)            
        
    except:
        log('sf_auth: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Please try valid credentials.')
        }
        
        result = simplejson.dumps(d)
        return HttpResponse(result)
    
@csrf_required
def twitter_link(request):
    try:
        log("twitter_link")
        alias = request.session['alias']
        TwitterUser.objects.link_alias_to_twitteruser(request, alias, request.session['tolink_twitteruser'])
        
        d = {
             'success': True,
        }
    
        return json_response(request,d)
    except Exception, e:
        log(traceback.format_exc())

        d = {
             'success': False,
             'msg':     e.message
        }
        
        return json_response(request,d)        
        
        
@csrf_required
def set_timezone(request):
    try:        
        alias = request.session['alias']
        if alias.account_user is None:
            raise Exception("Invalid alias.")
        timezonestr = request.REQUEST['time_zone']
        tz = timezone_from_timezonestr(timezonestr)
        if not tz:
            raise Exception("Invalid timezone.")
        alias.account_user.timezone = timezonestr
        alias.account_user.save()
        
        d = {
             'success': True,
        }
    
        return json_response(request,d)
    except Exception, e:
        log(traceback.format_exc())

        d = {
             'success': False,
             'msg':     e.message
        }
        
        return json_response(request,d)


@csrf_required
def expand_email(request):
    try:     
        if 'alias' in request.session:
            alias = request.session['alias']
            agent = get_agent(request)
            message_id = int(request.REQUEST['message_id'])
            email = Email.objects.get(id=message_id, owner_account=alias.account)
            if email.has_viewing_perms(alias):  
                subject = email.subject_normalized.strip()                
                content = email.get_body()
                if content is None:
                    raise NotifyUserException(_('Could not get content of your email.'))
                if (subject == content) or (email.source in [Email.SOURCE_QUICKBOX, Email.SOURCE_TWITTER]):  
                    t = Template('{% load account_tags %}{{content|urlizetrunc:25|target_blank:agent|safe|linebreaksbr}}')
                else:
                    t = Template('{% load account_tags %}{{subject|urlizetrunc:25|target_blank:agent|safe|linebreaksbr}}<br/>{{content|urlize|target_blank:agent|safe|linebreaksbr}}')
                c = Context({
                    'subject': email.subject_normalized,
                    'content': content,
                    'agent': agent
                })
                d = {
                    'success': True,
                    'body': t.render(c)
                }
                return json_response(request,d)

        raise Exception('Not authenticated')
    
    except NotifyUserException, e:
        d = {
            'success': False,
            'msg': e.message
        }
        
        return json_response(request,d)
        
    except:
        
        log(traceback.format_exc())
        
        d = {
            'success': False,
            'msg':      _("Could not process your request.")
        }
        
        return json_response(request,d)
    
    
@csrf_required
def set_modals(request):
    try:        
        if 'alias' in request.session:
            alias = request.session['alias']
            val = bool(int(request.REQUEST['val']))
            name = request.REQUEST['name']
            log("set_modals: name=%s, val=%d" % (name, val))

            if name == 'server':
                alias.show_modal_server = val
                alias.save()
            elif name == 'intro':
                alias.show_modal_intro = val
                alias.save()
            elif name == 'notice_upgrade':
                alias.show_notice_upgrade = val
                alias.save()
            else:
                raise Exception("set_modals: Unknown name")
        
        request.session['alias'] = alias
        
        d = {
            'success': True,
        }
        
        return json_response(request,d)
    
    except:
        
        log(traceback.format_exc())
        
        d = {
            'success': False,
        }
        
        return json_response(request,d)


@csrf_required
def terms(request):
    try:
        alias = request.session['alias']
        alias.beta_terms_date = now()
        alias.save()
        msg = _("Thanks for reading and accepting our terms.")

        d = {
            'success': True,
            'msg': msg
        }
        
        request.messages['notices'].append(msg)
        
        log(msg)
        
        return json_response(request,d)
            
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot save terms!')
        }
        
        log('failure')
        
        return json_response(request,d)   


@csrf_required
def edit_notification_settings(request):
    try:
        alias = request.session['alias']
        cmd = request.REQUEST['cmd']
        success = False
        if cmd == 'edit':
                        
            alias.send_confirmations = bool(int(request.REQUEST['send_confirmations']))
            alias.send_notifications = bool(int(request.REQUEST['send_notifications']))
            alias.save()
            success = True
        else:
            raise Exception('Not passing in a valid cmd')
        
        if not success:
            raise Exception('No command was executed.')
        else:
            d = {
                'success': True,
                'msg': _('Notification settings saved.')
            }
            return json_response(request,d)
            
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot save digest settings.')
        }
        return json_response(request,d)

    
@csrf_required
def disable_mailservers(request):
    alias = None
    try:
        alias = request.session['alias'] 
        log("disable_mailservers: alias = ", alias, alias.pk)
        
        alias.server_enabled = False
        log("Disabling server_smtp_enabled for ", alias)
        alias.server_smtp_enabled = False
        alias.show_modal_server = True
        alias.save()

        d = {
            'success': True,
            'msg': _('Incoming mail server checking is now disabled')
        }
        result = json.dumps(d)
        return HttpResponse(result)            
        
    except:
        log('disable_mailservers: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not disable mail checking for alias', alias)
        }
        
        result = json.dumps(d)
        return HttpResponse(result)

    
class BadCaptchaException(Exception):
    pass

   
@csrf_required 
def normalize_mail_settings(alias, mail_settings):
    log("normalize_mail_settings...")
    # Google Apps requires a full email address -- if not given, try the user's domain
    if mail_settings['service'] == MAILSERVICE_GOOGLE_APPS:
        if '@' not in mail_settings['username']:
            mail_settings['username'] += '@' + alias.email.split('@')[-1]
            log("normalize_mail_settings: adjusting to ", mail_settings['username'])
    

@csrf_required
def imap_auth(request):    
    alias = None
    try:            
        alias = request.session['alias'] 
        agent = get_agent(request)
        modified = False
        
        enabled = bool(int(request.REQUEST['enabled']))
        log("Enabled: ", enabled)
        
        if enabled:            
            port_str = request.REQUEST['port'].strip()
            mail_settings = {\
                'service':          request.REQUEST['server_service'].strip(),
                'server':           request.REQUEST['server'].strip(),
                'username':         request.REQUEST['username'].strip(),
                'password':         request.REQUEST['password'].strip(),
                'port':             int(port_str) if port_str else None,
                'use_ssl':          bool(int(request.REQUEST['use_ssl'])),
                'server_type':      request.REQUEST['server_type'].strip(),
                'tzinfo':           alias.account_user.get_timezone()
            }               
            normalize_mail_settings(alias, mail_settings)
                    
            for key, value in mail_settings.items():
                if key != 'password':
                    log("imap_auth: mail_settings %s = %s" % (key, value))
                                      
            # Save the new settings, no matter what
            if alias.server_service != mail_settings['service']:
                alias.server_service, modified = mail_settings['service'], True
            if alias.server_login != mail_settings['username']:
                alias.server_login, modified = mail_settings['username'], True
           
            if alias.server_server != mail_settings['server']:
                alias.server_server, modified = mail_settings['server'], True
            alias.server_port = mail_settings['port']
            alias.server_use_ssl = mail_settings['use_ssl']
            if alias.server_type != mail_settings['server_type']:
                alias.server_type, modified = mail_settings['server_type'], True
            if (not mail_settings['password']) or not all(c == PASSWORD_BULLET_CHAR for c in mail_settings['password']):
                alias.server_password_aes = CRYPTER.Encrypt(mail_settings['password'])
            else:
                if alias.server_password_aes:
                    mail_settings['password'] = CRYPTER.Decrypt(alias.server_password_aes)
            
            if modified:
                
                # New server, start crawling from the beginning
                # This is an UGLY HACK!
                # TODO: EmailFolders will belong to an EmailServer object, so
                # we won't have to delete them when we change servers
                log("imap_auth: modified=True, deleting old EmailFolder objects!")
                
                # Unlink emails from the emailfolder first, otherwise they'll be
                # deleted in the cascade!!!
                for e in Email.objects.filter(server_emailfolder__alias=alias):
                    e.server_emailfolder = None
                    e.save()
                                
                EmailFolder.objects.filter(alias=alias).delete()                
                
            request.session['last_crawled'] = None  # Allow a new crawl immediately
                
                
#            mail_settings['proxy_settings'] = alias.get_server_proxy_settings()     
#            alias.server_password_aes = Non
                        
            try:
                from crawl.tasks import MailServerAuthTask
                
                if mail_settings['service'] in [MAILSERVICE_GOOGLE_APPS, MAILSERVICE_GMAIL]:
                    if 'google_captcha' in request.session and request.REQUEST['captcha_answer']:
                        mail_settings['google_captcha'] = request.session['google_captcha']
                        mail_settings['google_captcha_answer'] = request.REQUEST['captcha_answer']
                    
                log("Calling MailServerAuthTask:")                                 
                result = MailServerAuthTask().delay(mail_settings).get()
                log("Returned from MailServerAuthTask get; result is:", result)
                
                if not result['success']:
                    log("imap_auth: received error from MailServerAuthTask. Here it is:")
                    log(result['error'])
                    raise Exception(result['error'])
                                
            except Exception, e:
                log("imap_auth exception:", e, e.message)
                log('imap_auth exception:', traceback.format_exc())
                
                alias.server_enabled = False
                alias.server_valid = False
                alias.save()
                                
                d = {
                    'success': False,
                    'captcha_needed': 'web login' in e.message.lower(), # GMail complains with "Web login required" if captcha needed
                    'msg': _('Please try valid credentials.')
                }
                
                result = json.dumps(d)
                return HttpResponse(result)
            
            alias.server_num_crawl_failures = 0
            alias.server_last_crawled = None
            alias.server_enabled = True
            alias.server_valid = True
            alias.save()
               
            log('__mail_auth__: valid')
            
            if agent == 'salesforce':
                if 'notices' not in request.session:
                    request.session['notices'] = []
                request.session['notices'].append(_('Please reload the Calenvy Sync tab for Salesforce to sync new messages.'))  
                
            d = {
                'success': True,
                'captcha_needed': False,
                'msg': _('We will now crawl your mail account daily and auto-parse important emails')
            }
            if alias in request.session.get('aliases_crawled', []):
                request.session['aliases_crawled'].remove(alias)        # Force a new crawl in views_cover.py
                
            request.session['alias'] = alias
            
            result = json.dumps(d)
            return HttpResponse(result)            
        
        else:
            if alias:
                alias.server_enabled = False
                # Leave alone the server_valid and other settings in this case
                alias.save()
            d = {
                'success': True,
                'captcha_needed': False,
                'msg': _('Incoming mail server connection: disabled.')
            }
            
            request.session['alias'] = alias
            
            result = json.dumps(d)
            return HttpResponse(result)            
        
    except:
        log('__mail_auth__: error',traceback.format_exc())
        d = {
            'success': False,
            'captcha_needed': False,
            'msg': _('Please try valid credentials.')
        }
        
        result = json.dumps(d)
        return HttpResponse(result)

  
@csrf_required  
def get_google_captcha(request):
    alias = None
    try:        
        log("get_google_captcha")
        alias = request.session['alias'] 
        mail_settings = {\
            'service':          request.REQUEST['service'],
            'username':         request.REQUEST['username'].strip(),    # the only setting we need for unlocking captcha
#            'proxy_settings':   alias.get_server_proxy_settings()
        }
        normalize_mail_settings(alias, mail_settings)
             
        log("get_google_captcha: username = ", mail_settings['username'])
        from crawl.tasks import GetCaptchaTask
                
        m = GetCaptchaTask().delay(mail_settings)
        result = m.get()
        if m.successful():
            captcha = result
            request.session['google_captcha'] = captcha
            d = {
                'success': True,
                'captcha_img_url': '/ajax/get_google_captcha_image/?slug=%s' % captcha['slug']
            }
        else:
            d = {
                'success': False,
                'msg': _('Please try valid credentials.')
            }
                
        result = json.dumps(d)
        return HttpResponse(result)
    except:
        d = {
            'success': False,
            'msg': _('Please try valid credentials.')
        }
                
        result = json.dumps(d)
        return HttpResponse(result)  
    
# Not AJAX, but might as well stuff it into this file
def get_google_captcha_image(request):
    if 'google_captcha' in request.session:    
        return HttpResponse(request.session['google_captcha']['img'], mimetype='image/jpeg')
    else:
        return HttpResponse('')
    
    
@csrf_required
def smtp_auth(request):    
    alias = None
    try:        
        alias = request.session['alias'] 
        log("smtp_auth: alias = ", alias)
        enabled = bool(int(request.REQUEST['enabled']))
        log("smtp_auth: enabled: ", enabled)
            
        if enabled:            
            port_str = request.REQUEST['port'].strip()
            mail_settings = {\
                # 'server_type':      MAILSERVER_SMTP,
                'service':          request.REQUEST['server_service'].strip(),
                'server':           request.REQUEST['server'].strip(),
                'username':         request.REQUEST['username'].strip(),
                'password':         request.REQUEST['password'].strip(),
                'port':             int(port_str) if port_str else None,
                'use_ssl':          bool(int(request.REQUEST['use_ssl'])),
            }
            normalize_mail_settings(alias, mail_settings)
                        
            for key, value in mail_settings.items():
                if key != 'password':
                    log("smtp_auth: mail_settings %s = %s" % (key, value))
                            
            # Save the new settings, no matter what
            alias.server_service = mail_settings['service']         # This gets set for both IMAP and SMTP
            alias.server_smtp_login = mail_settings['username']    
            if (not mail_settings['password']) or not all(c == PASSWORD_BULLET_CHAR for c in mail_settings['password']):
                alias.server_smtp_password_aes = CRYPTER.Encrypt(mail_settings['password'])
            else:
                if alias.server_smtp_password_aes:
                    mail_settings['password'] = CRYPTER.Decrypt(alias.server_smtp_password_aes)
            alias.server_smtp_port = mail_settings['port']          
            alias.server_smtp_server = mail_settings['server']
            alias.server_smtp_use_ssl = mail_settings['use_ssl']
                        
            try:
                if not mail_settings['server']:
                    raise Exception(_("Must specify host."))
                if settings.SERVER != 'local':          # Allow blank username and password for local testing
                    if (not mail_settings['username']) or (not mail_settings['password']):
                        raise Exception(_("Must specify username and password."))
                
                connection = mail.SMTPConnection(\
                    host=mail_settings['server'],
                    port=mail_settings['port'],
                    username=mail_settings['username'],
                    password=mail_settings['password'],
                    use_tls=mail_settings['use_ssl'],
                    fail_silently=False\
                )
                connection.open()
                connection.close()
                alias.server_smtp_enabled = True
                alias.server_smtp_valid = True
                alias.save()
                
                log('smtp_auth: valid')
                    
                d = {
                    'success': True,
                    'msg': _('We will now send outgoing mails through your SMTP server')
                }
            except:
                log("smtp_auth failed -- here's the traceback:", traceback.format_exc())
                log("smtp_auth: [1] disabling server_smtp_enabled for ", alias)
                alias.server_smtp_enabled = False
                alias.server_smtp_valid = False
                alias.save()
                
                d = {
                    'success': False,
                    'msg': _('Please try valid credentials.')
                }
            
            request.session['alias'] = alias
                
            result = json.dumps(d)
            return HttpResponse(result)
        else:
            if alias:
                log("smtp_auth: setting server_smtp_enabled=False for alias", alias, alias.pk)
                alias.server_smtp_enabled = False
                # Leave alone the smtp_server_valid and other settings in this case
                alias.save()
            d = {
                'success': True,
                'msg': _('Outgoing mail server connection: disabled.')
            }
            
            
            result = json.dumps(d)
            return HttpResponse(result)            
        
    except:
        log('smtp_auth: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Please try valid credentials.')
        }
        
        result = json.dumps(d)
        return HttpResponse(result)
    

@csrf_required
def followup_auth(request):
    """Called by the admin settings page, this is used to associate the account with
    a remote account on a Follow-up Robot server"""
        
    try:        
        alias = request.session['alias'] 
        log("followup_auth: alias = ", alias)
        enabled = bool(int(request.REQUEST['enabled']))
        log("followup_auth: enabled: ", enabled)
                
        try:
            # There can be only one account-wide followup RemoteUser
            ru = RemoteUser.objects.get(
                type=RemoteUser.TYPE_FOLLOWUP,
                account=alias.account,
                alias=None             
            )
        except:
            ru = RemoteUser(
                type=RemoteUser.TYPE_FOLLOWUP,
                account=alias.account,
                alias=None                                                       
            )
                
        if not enabled:
            ru.delete()
            d = {
                'success': True
            }
            result = json.dumps(d)
            return HttpResponse(result)                
        
        host = request.REQUEST['url'].strip().strip('/')
        username = request.REQUEST['username'].strip().lower()
        password = request.REQUEST['password']
        log(" host: ", host)
        log(" username: ", username)
        
        if not host:
            d = {
                'success': True,
            }
                
        from api.client import APIConsumer
        api = APIConsumer(domain=host, username=username, password=password, token=None, anonymous=False)
        
        try:
            cred = api.get_credential(RemoteUser.TYPE_CURECAL, remote_userid='account%d' % alias.account.id, remote_username='account%d' % alias.account.id, account_wide=True)
        except:
            raise NotifyUserException(_('Please try valid followup credentials.'))
        
        if not cred['success']:
            raise NotifyUserException(_('Please try valid followup credentials.'))
                                      
        ru.enabled = True
        ru.valid = True
        ru.host = host
        ru.username = cred['username']
        ru.save()
        
        ruc, created = RemoteCredential.objects.get_or_create_for_remote_user(None, ru, RemoteCredential.TYPE_BASIC_AUTH_TOKEN, incoming=False, token=cred['token'])
                      
        d = {
            'success': True
        }
        
        result = json.dumps(d)
        return HttpResponse(result)        
    
    except NotifyUserException, e:
        d = {
            'success': False,
            'msg': e.message
        }
        
        result = json.dumps(d)
        return HttpResponse(result)               
        
    except:
        d = {
            'success': False,
            'msg': _('Could not change your followup settings.')
        }
        
        result = json.dumps(d)
        return HttpResponse(result)        
        
        
#@csrf_required
def get_followup_campaigns(request):
    """
    If the account is associated with a follow-up robot server,
    retrieve a list of the available campaigns.
 
    Returns a structure like this:
        {
            "campaigns": [
                {
                    "campaign": {
                        "id": 2, 
                        "name": "Testing"
                    }
                }
            ], 
            "success": true
        }
    """
    log("get_followup_campaigns...")
    alias = request.session['alias']
    account = alias.account
        
    try:        
        # There can be only one account-wide followup RemoteUser
        ru = RemoteUser.objects.get(
            type=RemoteUser.TYPE_FOLLOWUP,
            account=alias.account,
            alias=None             
        )
        
        api = ru.get_api()
                
        result = json.dumps(api.get_campaigns())
        return HttpResponse(result)
    
    except:
        log("get_followup_campaigns fail")
        log(traceback.format_exc())
        d = {
            'success': False
        }
        
        result = json.dumps(d)
        return HttpResponse(result)               
      
@csrf_required  
def crawl_alias(request):
    """Return the NotedTaskMeta ID of the crawl currently running on the logged-in Alias.
    Optionally: Start a new crawl (for the Refresh button)"""
    alias = None
    try:
        log("crawl_alias start...")
        alias = request.session['alias'] 
        new_crawl = bool(int(request.REQUEST.get('new_crawl', 1)))
        
        queryset = NotedTaskMeta.objects.filter(alias=alias, type=NotedTaskMeta.TYPE_CRAWL, status=NotedTaskMeta.STATUS_STARTED)
        if queryset.count() > 0:
            noted_meta = queryset[0]
            request.session['last_crawled'] = noted_meta.date_started
        else:
            if new_crawl:
                log("crawl_alias calling create_job")
                import crawl.crawler
                if not alias.refresh_allowed(request):
                    raise NotifyUserException(_('Cannot refresh your newsfeed - please try again later.'))
                m, noted_meta = crawl.crawler.start_crawl_alias(alias)
                request.session['last_crawled'] = now()
            else:
                log("crawl_alias returning")
                d = {
                    'success': True,        # No crawl running at the moment, but don't start one
                    'id': None
                } 
                return json_response(request,d)
            
        log("crawl_alias returning")
        d = {
            'success': True,
            'id': str(noted_meta.id)
        }
        #result = json.dumps(d)
        return json_response(request,d)
        
        #return HttpResponse(result)
    
    except:
        log('__mail_auth__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot crawl alias.')
        }
        
        result = json.dumps(d)
        return HttpResponse(result)    
    

@csrf_required
def get_task_status(request):
    try:
        log("get_task_status")
        
        alias = request.session['alias']
        id = int(request.GET['task'])
        noted_meta = NotedTaskMeta.objects.get(id=id)       # Not the celery TaskID, just the DB pk for NotedTaskMeta
        if noted_meta.alias and noted_meta.alias != alias:
            raise Exception(_('Alias not authorized to view this job.'))
        
        new_emails = list(noted_meta.emails_crawled.all().order_by('date_sent'))
        noted_meta.emails_crawled.clear()       # not delete()!
        
        from ajax_feeds import WMNewEmails, get_widget_manager
        
        log("... WMNewEmails: ", ', '.join(str(e.pk) for e in new_emails))
        if new_emails:
            get_widget_manager(request).notify_all([ajax_feeds.WMNewEmails(new_emails), ajax_feeds.WMUpdateContacts()], request)
                
        if noted_meta.type == NotedTaskMeta.TYPE_CRAWL and noted_meta.status == NotedTaskMeta.STATUS_FINISHED:
            if 'aliases_crawled' in request.session and alias not in request.session['aliases_crawled']:
                request.session['aliases_crawled'].append(alias)
        
        d = {
            'success':     True,
            'message':     noted_meta.message,
            'job':         noted_meta.id,
            'status':      noted_meta.status,
            'job_success': noted_meta.success,
            'custom':      noted_meta.custom and json.loads(noted_meta.custom),
        }
            
        data = get_widget_manager(request).retrieve_updated(request, 'xml', extra_data=d, include_widgets=(noted_meta.status == NotedTaskMeta.STATUS_FINISHED))
            
        log("get_task_status: returning...")
        return HttpResponse(data, mimetype='text/xml')    
    except:
        log('get_task_status: error', traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot get job status.')
        }
        data = '<result><extradata>' + json.dumps(d) + '</extradata></result>'
                
        log("get_task_status: returning...")
        return HttpResponse(data, mimetype='text/xml')    
        
   
@csrf_required   
def edit_event(request):
    try:
        alias = request.session['alias']
        contact = alias.contact
        cmd = request.REQUEST['cmd']
        event_uid = request.REQUEST['uid']
        widget_id = request.REQUEST['widget_id']
        
        log("edit_event: alias=", alias, "cmd=", cmd, "event_uid=", event_uid)
        
        wm_updates = [ajax_feeds.WMUpdateEvents()]
        success = False
        if cmd == 'remove_from_event':
            event = Event.objects.get(pk=event_uid)
            Email2Contact.objects.filter(email=event.original_email, contact=alias.contact).delete()
            if event.get_owner() == contact:
                if event.status == EVENT_STATUS_FOLLOWUP_REMINDER:
                    wm_updates.append(ajax_feeds.WMUpdateContacts())
                event.status = EVENT_STATUS_CANCELED
            event.save()
            success = True
        elif cmd == 'change_status_fading':
            event = Event.objects.get(pk=event_uid)
            if (event.has_owner_perms(alias)):
                fading = int(request.REQUEST['fading'])
                if event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_SCHEDULED_FADING]:
                    event.status = EVENT_STATUS_SCHEDULED_FADING if fading else EVENT_STATUS_SCHEDULED
                elif event.status in [EVENT_STATUS_TENTATIVE, EVENT_STATUS_TENTATIVE_FADING]:
                    event.status = EVENT_STATUS_TENTATIVE_FADING if fading else EVENT_STATUS_TENTATIVE       
                #event.last_updated = now()
                event.save()
                success = True
        else:
            raise Exception('Not passing in a valid cmd')
                    
        widget_manager = get_widget_manager(request)
        widget_manager.notify_all_except(widget_id, wm_updates, request)

        if not success:
            raise Exception('Someone does not have privs to edit the event.')
        else:
            d = {
                'success': True,
                'msg': _('Event changed.')
            }
            return json_response(request,d)
            
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot switch alias.')
        }
        return json_response(request,d)
    

@csrf_required
def deactivate_event(request):
    try:
        log("deactivate_event")
        
        event_uid = int(request.REQUEST['id'])    
        alias = request.session['alias']
        event = Event.objects.get(pk=event_uid, owner_account=alias.account)
        
        if event.has_owner_perms(alias):
            event.deactivate_with_children()
        else:
            raise Exception(_("deactivate_event: Alias", alias, "has no permissions to deactivate event", event_uid))
        
        d = {
            'success': True,
            'msg': _('Event deactivated.')
        }
        return json_response(request,d)
            
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot deactivate event.')
        }
        return json_response(request,d)
    
   
@csrf_required 
def edit_email(request):
    try:
        log("edit_email")
                
        cmd = request.REQUEST['cmd']
        email_uid = int(request.REQUEST['id'])    
        alias = request.session['alias']
        email = Email.objects.get(pk=email_uid, owner_account=alias.account)
        
        for event in email.events.all():
            if not event.has_owner_perms(alias):
                raise Exception(_("edit_email: Alias", alias, "has no permissions to edit email", email_uid))
        
        if cmd == 'deactivate_with_children':
            for event in email.events.all():
                event.deactivate_with_children()
        
        elif cmd == 'set_perms':
            private = int(request.REQUEST['private'])
            for event in email.events.all():
                event.perms = EVENT_PERMS_PRIVATE if private else EVENT_PERMS_PUBLIC
                event.save()
            
        d = {
            'success': True,
            'msg': _('Email deactivated.')
        }
        return json_response(request,d)
            
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Cannot deactivate email.')
        }
        return json_response(request,d)
    
    
@csrf_required
def login(request):
    try:        
        agent = get_agent(request)
        email_input = request.REQUEST['email'].strip().lower()
        log('__login__ via email:',email_input)
        
        log("login: agent = ", get_agent(request))
        if 'password' in request.REQUEST and request.REQUEST['password'] != '':
            password = request.REQUEST['password'].strip()
        else:
            password = None
        
        # token = a web token (either legacy Outlook or SF... or new-style token)
        if 'token' in request.REQUEST and request.REQUEST['token'] != '':
            token = request.REQUEST['token'].strip()
        else:
            token = None            
        
        log("Login: got token ", token)
        
        # Twitter user to link this alias to
        if 'twitteruser_slug' in request.REQUEST and request.REQUEST['twitteruser_slug'] != '':
            twitteruser_slug = request.REQUEST['twitteruser_slug'].strip()
            tu = TwitterUser.objects.get(slug=twitteruser_slug)
        else:
            tu = None
            
        alias = None
        
        email_input = email_input.strip().lower()
        aliases = Alias.objects.filter(email=email_input).exclude(account=None)
                
        if 'alias' in request.REQUEST:
            alias_slug = request.REQUEST['alias']
            queryset = Alias.objects.active(include_inactive_accounts=True).filter(slug=alias_slug)
            if queryset.count() == 1:
                if queryset.get().email.lower() == email_input:
                    alias = queryset.get()
                    base_url = alias.account.base_url
               
        if alias is None:
            base_url = request.REQUEST['base_url']
       
            if aliases and base_url != '':
                log('base_url is defined')
                if len(aliases) > 1:
                    log('len aliases >1')
                    for a in aliases:
                        if a.account.base_url == base_url:
                            alias = a
                            break
            
            if not aliases:
                # Invalid password
                d = {
                    'success': False,
                    'msg': _('Could not validate your email address or password.')
                }     
                return json_response(request,d)
            
            if alias is None:
                active_aliases = [a for a in aliases if a.account.status == ACCOUNT_STATUS_ACTIVE]
                if active_aliases:
                    alias = active_aliases[0]
                else:
                    alias = aliases[0]          # Only inactive accounts -- this will take us to the info/order page
                base_url = alias.account.base_url
                
        if alias is not None:
#            account_user = alias.account_user
#            account = alias.account
#            if account.status != ACCOUNT_STATUS_ACTIVE:
#                d = {
#                    'success': True,
#                    'redirect': redirect_agent_url(agent, '/info/order'),
#                    'msg': ''
#                }   
##                request.messages['notices'].append(\
##                    _('Your account has expired and we have not received a payment from your payment gateway. Please make a payment below.'))
#                return json_response(request,d)
            send_email = True
                        
            return_msg = _('Success! We are sending you a secret access link via email right now.')
            if request.REQUEST['base_url'] and base_url and (base_url != request.REQUEST['base_url']):
                raise Exception('Cannot link email_input to base_url via Account')
            #elif password is None and alias.password_hash is None:
            #    log('letting the user in because they have no password setup yet.')
            #    request.session['alias'] = alias
            #    send_email = False
            elif password is not None and alias.validate_master_password(password.split('/')[0]):
                log("Using master password for override")
                clear_session(request)
                if '/' in password:
                    team_name = password.split('/')[-1]
                    for a in aliases:
                        if (a.account.name == team_name) or (a.account.base_url == team_name):
                            alias = a
                            break
                request.session['alias'] = alias
                user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
                auth.login(request, user)
                request.session['master_override'] = True
                send_email = False # because the alias is authenticated, email is not necessary.
                return_msg = _('Hello, master.')
            elif (password is not None) or (token is not None):
                validated = False
                for a in aliases:   
                    if a.validate_password(password) or a.validate_token(token, [RemoteCredential.TYPE_WEB_ONETIME_TOKEN, RemoteCredential.TYPE_LEGACY]):
                        
                        agent = get_agent(request)
                        
                        # Don't kick them out just yet if they don't have the right service level for this agent --
                        #  we're going to redirect them to the order page later. [eg 12/2]
#                        if agent == 'outlook' and not a.account.service.allow_outlook:
#                            d = {
#                                'success': False,
#                                'msg': _('Outlook plug-in is not supported with your service level.')
#                            }
#                            return json_response(request,d) 
#                        elif agent in MOBILE_AGENTS and not a.account.service.allow_mobile:
#                            log('login_agent:',agent)
#                            d = {
#                                'success': False,
#                                'msg': _('Mobile access is not supported with your service level.  You must upgrade to access the dashboard from your mobile device.')
#                            }
#                            return json_response(request,d) 
                        
                        validated = True
                        #clear_session(request)
                        user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
                        auth.login(request, user)
                        alias = a
                        alias.last_login_date = now()
                        log("******** Setting session key [1]:", request.session.session_key)
                        alias.session_key = request.session.session_key
                        alias.save()                            
                        request.session['alias'] = alias                        
                        send_email = False # because the alias is authenticated, email is not necessary.
                        return_msg = _('You are now authenticated!')
                if not validated:
                    if aliases and all([a.password_hash is None for a in aliases]):
                        raise NotifyUserException(_('Please check your inbox for an activation link from your automated assistant.  Search your inbox for the words "your new assistant" to find the link.'))
                    else:
                        raise Exception(_('Invalid password entered.')) # eg [8/29]
            #html = calendar(request, calendar_slug=account_user.calendar.slug, template='schedule/emails/calendar.html')
            #html = unicode(html).replace('Content-Type: text/html; charset=utf-8','')
            
            
            if tu:  
                TwitterUser.objects.link_alias_to_twitteruser(request, alias, tu)
                                                     
            if send_email:
                
                if aliases and all([a.password_hash is None for a in aliases]):
                    raise NotifyUserException(_('Please check your inbox for an activation link from your automated assistant.  Search your inbox for the words "your new assistant" to find the link.'))
                else:
                    raise Exception(_('Invalid password entered.')) # eg [8/29]
            
                to = [(alias.get_full_name(), alias.email)]
                assistant = alias.account.get_assistant()
                msg = mail.send_email('main_access_link.html', Context({
                'site': get_site(request),
                'account': alias.account,
                'alias': alias,
                #'calendar':html
                }), _('Your assistant is waiting on the web...'), 
                (assistant.get_full_name(), assistant.email), to, 
                reply_to=assistant.email, request=request, extra_headers=None, noted=None)
                if not msg:
                    raise Exception('Cannot not load template and send email')
            if send_email:
                redirect = False
            else:
                if (alias.account.status != ACCOUNT_STATUS_ACTIVE) and not request.session.get('master_override', False):
                    log('login: got here [A]')
                    if alias.account.service:
                        redirect = '/info/go/%s' % alias.account.service.name.lower()
                    else:
                        redirect = '/info/order'
                else:
                    if (alias.account.status != ACCOUNT_STATUS_ACTIVE) and request.session.get('master_override', False):
                        if 'notices' not in request.session:
                            request.session['notices'] = []
                        request.session['notices'].append(_("This account is expired. Master override enabled."))  
                    redirect = get_redirect_param(request)
                    if not redirect:
                        redirect = '/dash/%s/' % alias.account.base_url
            
            if (agent == 'outlook') and token is None:
                # Outlook plugin doesn't yet support the API for creating tokens --
                #  just create a simple web token
                remote_user, created = RemoteUser.objects.get_or_create(type=RemoteUser.TYPE_OUTLOOK, alias=alias, account=alias.account, remote_userid='12345')
                remote_user.enabled = True
                remote_user.valid = True
                remote_user.save()
                
                remote_credential, created = RemoteCredential.objects.get_or_create_for_remote_user(alias, remote_user, RemoteCredential.TYPE_LEGACY)
                token = remote_credential.token
                    
            redirect = redirect_agent_url(agent, redirect if redirect else '/dash/%s' % alias.account.base_url)  
            if bool(int(request.REQUEST.get(settings.PROFILER_KEY, '0'))):
                redirect = '/dash/%s/?%s=1' % (alias.account.base_url, settings.PROFILER_KEY)
            
            # Profiling flags         
            if 'disable_widgets' in request.REQUEST:
                request.session['disable_widgets'] = request.REQUEST['disable_widgets'].split(',')
            if 'summary_enabled' in request.REQUEST:
                request.session['summary_enabled'] = bool(int(request.REQUEST['summary_enabled']))
                
            request.session['last_crawled'] = None  # Allow crawl upon login
            d = {
                'success': True,
                'redirect': redirect,
                'user_email': email_input,
                'user_token': token,
                'msg': return_msg
            }
            
            log("login: returning this structure:", d)
#            if 'to' in request.GET:
#                to = request.GET['to']
#                return HttpResponseRedirectAgent(request, to)
            return json_response(request,d)
        else:
            raise Exception('Cannot lookup Alias by email')
    except NotifyUserException, e:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)
    except:
        log(traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not validate your email address or password.')
        }
       
        return json_response(request,d)


@csrf_required
def logout(request):
    try:
        auth.logout(request)      
        clear_session(request)
        d = {
            'success': True
        }
        return json_response(request,d)
    except:
        d = {
            'success': False
        }
        return json_response(request,d)
            
#def save_twitter(request):
#    try:
#        twitter_enabled = int(request.REQUEST['twitter_enabled'].strip())
#        twitter_username = request.REQUEST['twitter_username'].strip()
#        alias = request.session['alias']
#        
#        if twitter_enabled == 1:
#            alias.twitter_username=twitter_username
#            alias.save()
#        else:
#            alias.twitter_username=''
#            alias.save()
#        
#        d = {
#            'success': True,
#            'msg': _('We just saved your twitter settings! Thanks.')
#        }
#        return json_response(request,d)
#    except:
#        d = {
#            'success': False,
#            'msg': _('Could not save twitter settings')
#        }
#        return json_response(request,d)
            
      
@csrf_required      
def set_name_password(request):
    try:
        alias = request.session['alias']
        password = request.REQUEST.get('password', None)
        first_name = request.REQUEST.get('first_name', None)
        last_name = request.REQUEST.get('last_name', None)
        ez_name = request.REQUEST.get('ez_name', None)
        log('__set_password__: ', alias)
                
        # Can't use the same password on more than one alias belonging to the same AccountUser
        for other_alias in alias.account_user.get_aliases_for_display():
            if (other_alias != alias) and other_alias.validate_password(password):
                msg = _("You can't use the same password on more than one account; please try a different password.")
                d = {
                    'success': False,
                    'key': 'password',
                    'msg': msg
                }
                request.messages['notices'].append(msg)
                return json_response(request,d)
               
        if password and not all(c==PASSWORD_BULLET_CHAR for c in password):
            if not Alias.objects.is_strong_password(password):
                raise NotifyUserException(html=_('Your password must be at least %d characters long, and contain a letter and a digit.') % MIN_PASSWORD_LENGTH)
            alias.set_password(password)
            alias.reset_password_slug_hash = None
            alias.reset_password_slug_date = None
            alias.save()
        
        if ez_name:
            ez_name = ez_name.replace('@','').replace(' ','')
            for other_alias in Alias.objects.filter(account=alias.account).exclude(pk=alias.id):
                if (other_alias.ez_name == ez_name):
                    msg = _("You can't use the same @username as another user in your account; please try a different @username.")
                    d = {
                        'success': False,
                        'key': 'ez_name',
                        'msg': msg
                    }
                    request.messages['notices'].append(msg)
                    return json_response(request,d)
            alias.ez_name = ez_name
            alias.save()
            
        contact = alias.contact
        
        if contact:
            
            if first_name is not None:
                contact.first_name = first_name.strip()
                
            if last_name is not None:
                contact.last_name = last_name.strip()
                
            contact.save()
        
        msg = _('Your name and password have been set.')
        d = {
            'success': True,
            'ez_name': ez_name,
            'msg': msg
        }
        request.messages['notices'].append(msg)
        return json_response(request,d)

    except NotifyUserException, e:
        log('__set_password__: ',traceback.format_exc())
        d = {
            'success': False,
            'key':  'password',
            'html_msg': e.html_message,
            'msg': e.message,
        }
        return json_response(request,d)    

    except Exception:
        log('__set_password__: error',traceback.format_exc())
        d = {
            'success': False,
            'key':  'password',
            'msg': _('Could not set your password.')
        }
        return json_response(request,d)    


@csrf_required
def forgot_password(request):
    try:
        email = request.REQUEST['email'].strip().lower()
        #team_name = request.REQUEST['team_name'].strip()
        
        log("forgot_password: email=", email)
        
#        if not team_name:
#            alias = Alias.objects.active().get(email=email, account__name=None, account_user__timezone__icontains=timezone)
#        else:
#            alias = Alias.objects.active().get(email=email, account__name=team_name, account_user__timezone__icontains=timezone)
        
        # Don't filter by active() here -- password recovery needs to work if account has expired, so they can get in to pay
        aliases = Alias.objects.filter(email=email)
        
        for alias in aliases:
            recovery_slug = create_slug(length=20)
            alias.reset_password_slug_hash = hashlib.md5(recovery_slug).hexdigest()
            alias.reset_password_slug_date = now()
            alias.save()
            site = alias.get_site()
            c = Context({
                'alias':            alias,
                'recovery_slug':    recovery_slug,
                'site':             site,
                'has_ssl':          site.domain.lower() in settings.SSL_DOMAINS
            })
            
            subject = _("Reset your password on %s") % site.name
            assistant = alias.account.get_assistant()
            mail.send_email('main_reset_password.html', c, subject,
                (assistant.get_full_name(), assistant.email),
                (alias.get_full_name(), alias.email),
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_RESET_PASSWORD_LINK,
                    'owner_account': assistant and assistant.owner_account
                }
            )
            
        msg = _("We have sent an email to %s with a link to reset your password.") % email
            
        d = {
            'success': True,
            'msg': msg
        }       
            
        return json_response(request,d)
        
    except Exception:
        log('forgot_password: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not verify your information.')
        }
        return json_response(request,d) 
    
    
@csrf_required
@global_def
def send_registration(request):
    "The actual AJAX call for registration"
    d = register.register(request, request.REQUEST)
    return json_response(request, d)


@csrf_required
def parse(request):

    try:  
        alias = request.session['alias']
        log("parse start: alias=", alias)
        log(" str=", request.REQUEST.get('str'))
        log(" ref_contact_id=", request.REQUEST.get('ref_contact_id'))         # is this a note "in ref. to" a particular contact? if so, here's the id
        log(" ref_bucket_slug=", request.REQUEST.get('ref_bucket_slug'))
        log(" reply_to_all=", request.REQUEST.get('reply_to_all'))             # are we replying to all, or just the last person in the thread?
        log(" reply_id=", request.REQUEST.get('reply_id'))                     # id of the Email in the db we're replying to
        log(" public_twitter=", request.REQUEST.get('public_twitter'))         # is this a brand-new Twitter message? (default false)
        log(" require_at_symbol=", request.REQUEST.get('require_at_symbol'))   # is the @ symbol needed to make a scheduled event? (default true)
        
        
        parse_quick_consecutive = request.session.get('parse_quick_consecutive', 0)
                
        if alias.parse_last_date:
            since_last = now() - alias.parse_last_date
            if parse_quick_consecutive >= MAX_PARSE_QUICK and since_last < HOWLONG_PARSE_QUICK_BACKOFF:
                d = {
                    'success': False,
                    'msg': 'You have exceeded the maximum number of messages you can post this quickly. Please wait a minute and try again.'
                } 
                log("parse failed: exceeded max # of messages")
                return json_response(request, d)                        
            elif since_last < HOWLONG_PARSE_QUICK:
                request.session['parse_quick_consecutive'] = parse_quick_consecutive + 1
            else:
                request.session['parse_quick_consecutive'] = 0
                 
        alias.parse_last_date = now()
        alias.save()
        
        content = request.REQUEST['str'].strip()
        
        # Hidden fields (what this quickbox email is in reference to)
        try:
            ref_contact_id = str(int(request.REQUEST['ref_contact_id']))
        except:
            ref_contact_id = None
            
        # Hidden fields (what this quickbox email is in reference to)
        if request.REQUEST.get('ref_bucket_slug', 'false').strip() not in ['false', '']:
            ref_bucket_slug = request.REQUEST['ref_bucket_slug'].strip()
        else:
            ref_bucket_slug = None        
               
        if request.REQUEST.get('reply_id', '').strip() not in ['false', '']:
            reply_id = request.REQUEST['reply_id'].strip()
        else:
            reply_id = None
            
        if request.REQUEST.get('reply_to_all', '').strip().lower() == 'true':
            reply_to_all = True
        else:
            reply_to_all = False
        
        public_twitter = request.REQUEST.get('public_twitter', 'false').strip().lower() == 'true'
        require_at_symbol = request.REQUEST.get('require_at_symbol', 'true').strip().lower() == 'true'

        if content:
            log("Parse: ", content)
                                             
            mode = crawl.procmail.procmail_quickbox(alias=alias, content=content, \
                ref_contact_id=ref_contact_id, ref_bucket_slug=ref_bucket_slug, \
                reply_id=reply_id, reply_to_all=reply_to_all, mode={'public_twitter': public_twitter, 'require_at_symbol': require_at_symbol})
            
            new_email = mode.get('email', None)
            
            wm = (mode and mode.get('wmessages', []))
            if wm:
                wmessage = ' '.join(wm)
            else:
                wmessage = _('Error: could not process your input; try again.')
                # TODO: More notification for buckets/contacts?
            
            if mode and mode.get('refresh_contacts', False):
                get_widget_manager(request).notify_all([ajax_feeds.WMUpdateContacts()], request)
            
            if mode and mode.get('force_refresh_all', False):   # This happens when we invite a new user via the quickbox
                get_widget_manager(request).refresh_manager()
                        
            get_widget_manager(request).notify_all([ajax_feeds.WMNewEmails([new_email], wmessage)] + wm, request)
            
            if mode and 'alert' in mode:
                d = {
                     'success': False,
                     'msg': mode['alert']
                }
                return json_response(request, d)
            
            log("parse: done - group_id=", new_email.id if new_email else None)
            
            d = {
                'success': True,
                'msg': _('We just parsed your message! Thanks.'),
                'group_id': new_email.id if new_email else None
            }
        else:
            d = {
                'success': True,
                'msg': ''
            }
              
        return json_response(request, d)

    except:
        log('__parse__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not parse your message.')
        }
        return json_response(request,d)
        
    
@csrf_required    
def get_file_owner(request):
    try:
        from assist import reminder
        
        slug = request.REQUEST['slug'].strip()
        f = File.objects.get(slug=slug)
        alias = request.session['alias']
        
        log("get_file_owner: slug=", slug, "alias=", alias)
        owner_alias = f.get_owner_alias(alias)
        
        if f.email.owner_account != alias.account:
            raise Exception('hacking alert!')
        
        display_owner = owner_alias.get_tag_or_name_for_display(alias)
    
        d = {
            'success': True,
            'owner': display_owner
        }
        return json_response(request,d)

    except:
        log('get_file_owner: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not request file permission.')
        }
        return json_response(request,d)   
    

@csrf_required
def load_people(request):
    try:
        alias = request.session['alias']
        #if alias.account is None or alias.account.base_url == 'demo':
        #    raise Exception("Invalid alias.")
        
        people = []
        aliases = Alias.objects.active().filter(account=alias.account)
        for a in aliases:
            if a != alias:
                people.append({\
                    'ez_name':      a.get_tag_for_display(),
                    'full_name':    a.get_full_name(),
                    'email':        a.email
                })
        people = [p for p in people if p['ez_name']]
        people.sort(cmp=lambda p1, p2: cmp(p1['ez_name'], p2['ez_name']))
        
        d = {
             'success': True,
             'people':  people
        }
    
        return json_response(request,d)
    except Exception, e:
        log(traceback.format_exc())

        d = {
             'success': False,
             'msg':     e.message
        }
        
        return json_response(request,d)
    

# no csrf_required
def request_demo(request):
    try:
        # TODO: send calenvy@noted.cc an email with the person's email 
        email = request.REQUEST['email'].strip().lower()
        full_name = request.REQUEST['full_name'].strip().lower()
        flavor = request.REQUEST['try_type'].strip().lower()
        if not is_valid_email_address(email):
            raise Exception(_("Please enter a valid email address."))
        to = [('', settings.SALES_EMAIL)]
        msg = mail.send_email('main_request_demo.html', {
        'email': email,
        'full_name': full_name,
        'site': get_site(request),
        'flavor': flavor
        }, _('[DEMO] %s <%s>' % (email, full_name)), 
        (settings.SERVER_EMAIL), to, 
        reply_to=email, request=request, extra_headers=None, noted=None)
        if not msg:
            raise Exception('Cannot not load template and send email')
        
        d = {
             'msg': _("We will get back to you right away!"),
             'success': True,
        }
        return json_response(request,d)
    except Exception, e:
        log(traceback.format_exc())

        d = {
             'success': False,
             'msg':     e.message
        }
        
        return json_response(request,d)


# no csrf_required
def invite_demo(request):
    try:
        from assist import register
        email = request.REQUEST['email'].strip().lower()
        if not is_valid_email_address(email):
            raise Exception(_("Please enter a valid email address."))
        
        account = Account.objects.get(name=settings.ACCOUNT_DEMO)
        register.add_new_aliases([email], account, timezone=settings.TIME_ZONE)
     
        d = {
             'msg': _("Please check your email for more information."),
             'success': True,
        }
        return json_response(request,d)
     
    except Exception, e:
        log(traceback.format_exc())

        d = {
             'success': False,
             'msg':     e.message
        }
        
        return json_response(request,d)
    
    
    
# no csrf_required
def select_service(request):
    """
    Called by the /info/go to store the desired service into the session
    """
        
    agent = get_agent(request)
        
    try:
        log('[1]')
        alias = request.session.get('alias')        # If we're already logged in as a user, then we're upgrading
        
        service = Service.objects.get(name=request.GET['service'])
                
        if request.GET.get('promo'):
            try:
                promo = Promo.objects.get(code__iexact=request.GET['promo'])
            except:
                try:
                    promo = Promo.objects.get(name__iexact=request.GET['promo'])
                except:
                    promo = None            
        else:
            promo = None
        
        request.session['billing_service'] = service
        request.session['billing_promo'] = promo
        
        if (alias and alias.account) or service.is_payment_required(upgrade=(alias is not None), promo=promo):
            # If already logged in (upgrade/downgrade), force them to go to paypal
            d = {
                'success': True,
                'payment_required': True,
                'payment_optional': False
            }    
        elif service.is_free(promo=promo):
            d = {
                'success': True,
                'payment_required': False,
                'payment_optional': False
            }            
        else:               
            d = {
                'success': True,
                'payment_required': False,
                'payment_optional': True,
                'msg': _("Enter billing information now? Cancel to continue registration (no credit card required)."),
            }
        return json_response(request,d)

    except Exception, e:
        log('is_payment_required: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your subscription info.')
        }
        return json_response(request,d)    


# no csrf_required
def get_service_features(request):
    log("__get_service_terms__ start")
    try:
        agent = get_agent(request)
        service_name = request.GET.get('service', None).strip()
        alias = request.session.get('alias')
        account = alias and alias.account
        
        try:
            service = Service.objects.get(name=service_name)
        except:
            raise NotifyUserException(_('Sorry, we could not find the service by name %s.') % service_name)
        
        d = {
            'success':          True,
            'features':         service.get_features_for_display(),
            'paypal_allowed':   service.calc_discounted_price(account=account) > 0  # only services with a non-zero fixed monthly price can use Paypal
            #'terms_string':    service.get_fancy_terms_string()
        }
        return json_response(request,d)

    except NotifyUserException, e:
        log('__get_service_features__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message if e.message else _('Could not grab service features.')
        }
        return json_response(request,d)            
        
    except Exception, e:
        log(traceback.format_exc())
        log('__get_service_features__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        return json_response(request,d)    


@csrf_required
def set_billing_info(request):
    """Store or modify the account's credit card information.
    Starts or modifies a subscription on the payment server."""
    
    log("set_billing_info start")
    try:
        alias = request.session['alias']
        agent = get_agent(request)
        if not alias and alias.billing_allowed():
            raise Exception()
        account = alias.account
        
        if 'service' in request.REQUEST:
            service = Service.objects.get(name=request.REQUEST['service'])
        else:
            service = account.service
        
        params = {}
    
        params['gateway'] = settings.PAYMENT_GATEWAY
        params['email'] = alias.email
        params['description'] = _('Calenvy account: %s') % account.base_url
        
        params['card_number'] = strip_nondigits(request.REQUEST['card_number'].strip())
        params['expiration_date'] = request.REQUEST['expiration_date'].strip()             # YYYY-MM format
        params['card_code'] = request.REQUEST['card_code'].strip()           # 3 or 4 digit CVV code
        
        name = request.REQUEST['name'].strip()[:50]
        params['bill_first_name'], params['bill_last_name'] = get_first_last_name(name)
        
        address = request.REQUEST['address'].strip()
        address_2 = request.REQUEST['address_2'].strip()
        params['bill_address'] = (address + ' ' + address_2).strip()
        
        params['bill_city'] = request.REQUEST['city'].strip()[:40]
        params['bill_state'] = request.REQUEST['state'].strip()[:20]
        params['bill_zip'] = request.REQUEST['zip'].strip()[:20]
        params['bill_country'] = request.REQUEST['country'].strip()[:60]
        
        params['customer_id'] = payment_client.object_id(alias.account, prefix=payment_client.customer_id_prefix)   # The "external ID" of the customer relative to the payment gateway
        
        remote_customer_profile = json.loads(account.remote_customer_profile_json or '{}')
        if remote_customer_profile and (account.remote_gateway == params['gateway']):
            params['customer_profile'] = remote_customer_profile
             
        # For now, this code always starts or modifies a subscription
        params['subscription'] = True
        if account.remote_subscription_id:
            params['subscription_id'] = account.remote_subscription_id  # If a subscription exists, we're modifying it
            params['note'] = 'Modified: %s / %s %s - %s' % (alias.email, alias.contact.first_name, alias.contact.last_name, service.name)
        else:
            params['note'] = '%s / %s %s - %s' % (alias.email, alias.contact.first_name, alias.contact.last_name, service.name)
        params['subscription_bill_now'] = True  # make the first charge to the credit card right away
        
        num_users = account.num_active_aliases()
        if service.max_users_flat_rate is None:
            # unlimited users
            extra_users = 0
        else:
            extra_users = max(0, account.num_active_aliases() - service.max_users_flat_rate) 

        promo = account.promo
        if promo and promo.service and (promo.service != service):    # Don't count the promo if it doesn't match the service
            promo = None
            
        params['subscription_item'] = {
            'service': service.name, 
            'promo': promo and promo.code,
            'num_users': num_users,
            'extra_users': extra_users
        }
        params['subscription_price'] = service.calc_discounted_price(account=account)
        params['subscription_dmy'] = service.dmy
        params['subscription_num_dmy'] = service.num_dmy
        
        if not params['card_number']:
            raise NotifyUserException(_('Please enter a credit card number.'))
        if not params['card_code']:
            raise NotifyUserException(_('Please enter a credit card security code.'))
        if not params['bill_first_name'] and not params['bill_last_name']:
            raise NotifyUserException(_('Please enter your billing name.'))
        if not params['bill_address']:
            raise NotifyUserException(_('Please enter your billing address.'))
        if not params['bill_zip']:
            raise NotifyUserException(_('Please enter your billing zip code.'))
            
        # Store it into the payment gateway
        api = payment_client.get_api()
        
        result = api.set_profile(**params)
        
        # Even before checking for failure, check for a customer_profile structure and store it
        #  That way, if it failed because of a bad credit card, we can update the existing
        #  (newly created) customer profile on the gateway when the user corrects their
        #  credit card info    
        if 'customer_profile' in result:
            account.remote_gateway = settings.PAYMENT_GATEWAY
            account.remote_customer_profile_json = json.dumps(result['customer_profile'])
                
        if 'subscription_id' in result:
            # Start the subscription with the given service and promo
            log("Starting subscription for account: ", account, result['subscription_id'])
            account.remote_subscription_id = result['subscription_id']
            account.trial_period = False
            account.status = ACCOUNT_STATUS_ACTIVE
            account.date_expires = now() + paypal_timedelta('%d %s' % (service.num_dmy, service.dmy))
        
        account.save()
                
        if 'billing_event' in result:
            # Create the first Payment object
            # This may modify the account
            from assist import payment
            payment.handle_payment_notify(result['billing_event'])
        
        # Any alias that makes a payment becomes an admin    
        alias.is_admin = True
        alias.save()
        
        # Account and alias may be stale, reload them
        request.session['alias'] = Alias.objects.get(id=alias.id)
        
        if not result['success']:
            raise NotifyUserException(result['msg'])
            
        #api.start_subscription(**params)
        domain = get_site(request).domain
        
        redirect_url = 'http%s://%s%s?email=%s' % ('s' if domain in settings.SSL_DOMAINS else '', domain, '/', urllib.quote_plus(alias.email))
        if agent:
            redirect_url += '&agent=%s&msg=' % agent
            if 'notices' not in request.session:
                request.session['notices'] = []
            request.session['notices'].append(_("Your payment has been processed successfully."))  

        d = {
            'success':         True,
            'redirect_url':    redirect_url
        }
        return json_response(request,d)

    except NotifyUserException, e:
        log('set_billing_info: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message if e.message else _('Could not grab service features.')
        }
        return json_response(request,d)            
        
    except Exception, e:
        log(traceback.format_exc())
        log('set_billing_info: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        return json_response(request,d)    
    
    
@csrf_required
def get_paypal_url(request):
    """Get the url for starting/modifying a Paypal subscription."""
    
    log("get_paypal_url start")
    try:
        alias = request.session['alias']
        agent = get_agent(request)
        if not alias and alias.is_admin:
            raise Exception()
        account = alias.account
        
        if 'service' in request.GET:
            service = Service.objects.get(name=request.GET['service'])
        else:
            service = account.service
        promo = account.promo
        site = get_site(request)
        domain = site.domain

        if promo and promo.service and (promo.service != service):    # Don't count the service if it doesn't match the promo
            promo = None
            
        if not service.calc_discounted_price(account=account):
            # Can't use Paypal for services that have no fixed monthly price
            raise NotifyUserException(_("Cannot pay for this service level using Paypal."))
            
        from assist.payment import paypal_generate_url
                
        return_url = 'http%s://%s%s?email=%s' % ('s' if domain in settings.SSL_DOMAINS else '', domain, reverse('registration_complete'), urllib.quote_plus(alias.email))
        if agent:
            return_url += '&agent=%s' % agent
        
        cancel_url = 'http%s://%s%s' % ('s' if domain in settings.SSL_DOMAINS else '', domain, reverse('payment_cancel'))
        if agent:
            cancel_url += '?agent=%s' % agent        
        
        redirect_url = paypal_generate_url(alias, service, promo, return_url, cancel_url, site=site)
        
        d = {
            'success':         True,
            'redirect_url':    redirect_url
        }
        return json_response(request,d)

    except NotifyUserException, e:
        log('get_paypal_url: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message if e.message else _('Could not connect to Paypal.')
        }
        return json_response(request,d)            
        
    except Exception, e:
        log(traceback.format_exc())
        log('get_paypal_url: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        return json_response(request,d)    
        

# no csrf_required
def get_service_terms(request):
    log("__get_service_terms__ start")
    import urllib
                    
    try:
#        alias_slug = request.GET['alias']
        alias = request.session.get('alias')
        account = alias and alias.account
        agent = get_agent(request)
        service = Service.objects.get(name=request.GET['service'])
        
        promo_code = request.GET.get('promo', '').strip().upper()
#        alias = Alias.objects.active().get(slug=alias_slug)
#        if not alias.account:
#            raise Exception('Alias %s is missing account.' % alias_slug)
#        account = alias.account
        
        redirect = False

        if promo_code:
            try:
                promo = Promo.objects.get(code__iexact=promo_code)
            except:
                try:
                    promo = Promo.objects.get(name__iexact=promo_code)
                    redirect = True
                except:        
                    raise NotifyUserException(_('Sorry, we could not validate the promo code.'))
        else:
            promo = None
            
        if promo and promo.expiration and (now() > promo.expiration):
            raise NotifyUserException(_("Promo code %s has already expired.") % promo_code)
        if promo and promo.service and promo.service != service:
            raise NotifyUserException(_('Promo is only valid for the service: %s.') % promo.service.name)
        
        if not promo:
            # Promo not specified, use the account's existing promo
            # (It's okay if this is an expired promo)
            promo = account and account.promo
            
        # No trial period for folks modifying their account, or for folks using a promo
        disable_trial = bool(request.session.get('alias', None) or (promo and promo.disable_service_trial))
                    
        terms = service.get_fancy_terms_string(promo, disable_trial=disable_trial, account=account)
        free = service.is_free(promo)
                
        d = {
            'success':              True,
            'terms':                terms,
            'free':                 free,
            'promo_name':           promo and promo.name or '',
            'redirect':             redirect,
        }
        
        return json_response(request,d)

    except NotifyUserException, e:
        log('__get_service_terms__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': e.message if e.message else _('Could not get service terms.')
        }
        return json_response(request,d)            
        
    except Exception, e:
        log(traceback.format_exc())
        log('__get_service_terms__: error',traceback.format_exc())
        d = {
            'success': False,
            'msg': _('Could not process your request.')
        }
        return json_response(request,d)    

  
# Used by the Outlook plugin only

@csrf_required
def retrieve_email(request):
    try:
        alias = request.session['alias']
        if not alias:
            raise Exception()
        
        message_id = request.GET['message_id']
        e = Email.objects.get(id=message_id, contacts=alias.contact)
        
        d = {
             'success': True,
             'email':   mail.email_as_json(e)   # throws exception if cannot retrieve original mail
        }
        
        return json_response(request,d)   
        
    except:
        d = {
            'success': False,
            'msg': _('Could not retrieve email.')
        }
        return json_response(request,d)         
    
  


@csrf_required
def twitter_auth(request):
    
    try:        
        cmd = request.GET['cmd']
        alias = request.session.get('alias', None)
                   
        if cmd == 'unlink_alias':
            for tu in TwitterUser.objects.filter(alias=request.session['alias']):
                tu.alias = None
                if (tu.alias is None and tu.account is None):
                    tu.access_token = None
                tu.save()
            
            d = {
                'success': True
            }
        elif cmd == 'unlink_account':
            for tu in TwitterUser.objects.filter(account=request.session['alias'].account):
                tu.account = None
                if (tu.alias is None and tu.account is None):
                    tu.access_token = None
                tu.save()
            
            d = {
                'success': True
            }        
        elif cmd in ['link_alias', 'link_account']:
            if cmd == 'link_account':
                if not alias or not alias.is_admin:
                    raise Exception('You must be an admin to perform this operation.')
                                 
            request_token, url = TwitterUser.objects.get_auth_url(get_site(request))
            request.session['twitter_token'] = request_token    # This gets read in views_twitter.py
            request.session['twitter_return_cmd'] = cmd
            d = {
                'success': True,
                'url': url
            }
                                
        return json_response(request,d)
    except:
        d = {
            'success': False,
            'msg': _('Could not connect with Twitter.')
        }
        return json_response(request,d)            

    else:
        twitter_auth_url = '/'
        
      
@csrf_required  
def get_tracker_hash(request):
    
    try:
        log("get_tracker_hash")
        if not request.session.get('alias'):
            raise Exception()
        
        data = request.GET.get('data',None)
        if data is not None:
            result = CRYPTER_TRACKER.Encrypt(data)
            d = {
                'success': True,
                'data': result
            }
        else:
            d = {
                'success': False
            }
        return json_response(request,d)

    except:
        d = {
            'success': False    
        }
        return json_response(request,d)
       

@csrf_required
def set_rules_enabled(request):
    
    try:
        log("set_rules_enabled: ")
        alias = request.session['alias']
        type = request.REQUEST['type']
        val = bool(int(request.REQUEST['val']))
        
        log("set_rules_enabled: alias=", alias, "type=", type, "val=", val)
        
        if (type == EmailRule.TYPE_SF):
            alias.sf_rules_enabled = val
            alias.save()
        else:
            raise NotImplementedError
        
        d = {
            'success': True,
        }
        return json_response(request,d)

    except:
        d = {
            'success': False,
            'msg': _('Could not change your settings.') 
        }
        return json_response(request,d)
      


@csrf_required
def edit_rule(request):
    
    try:
        alias = request.session['alias']
        log("edit_rule: ", alias)
        cmd = request.REQUEST['cmd']
        id = request.REQUEST.get('id')
                        
        if cmd == 'delete':
            log("edit_rule: delete %s" % id)
            rule = EmailRule.objects.get(owner_account=alias.account, id=id)
            rule.delete()
        elif cmd in ['edit', 'new']:
            log("edit_rule: edit %s" % id)
            if cmd == 'edit':
                rule = EmailRule.objects.get(owner_account=alias.account, id=id)
            else:
                rule = EmailRule.objects.get_default_for_account(alias, alias.account)
                          
            if 'type' in request.REQUEST:
                if request.REQUEST['type'] not in [EmailRule.TYPE_SF, EmailRule.TYPE_FOLLOWUP]:
                    raise Exception()
                rule.type = request.REQUEST['type']
            
            if 'enabled_incoming' in request.REQUEST:
                rule.enabled_incoming = bool(int(request.REQUEST['enabled_incoming']))
            if 'enabled_outgoing' in request.REQUEST:
                rule.enabled_outgoing = bool(int(request.REQUEST['enabled_outgoing']))
            if 'enabled_contacts' in request.REQUEST:
                rule.enabled_contacts = bool(int(request.REQUEST['enabled_contacts']))
            if 'enabled_leads' in request.REQUEST:
                rule.enabled_leads = bool(int(request.REQUEST['enabled_leads']))
            
            match = rule.match()
            
            if 'match_addr_type' in request.REQUEST:
                
                # match_addr_type is one of 'team', 'str'
                
                if request.REQUEST['match_addr_type'] == 'team':
                    # Team member
                    # Data = None to match anyone on the team, or integer ID of alias to match that specific alias
                    match['addr'] = {
                        'matches':  True,               # True if matches, False to invert the sense of the test
                        'type': 'team',                 # 'external', 'str', 'regex'
                        'data': None if request.REQUEST['match_addr_data'] == '' else int(request.REQUEST['match_addr_data'])
                    }
                    
                elif request.REQUEST['match_addr_type'] == 'str':
                    data = request.REQUEST['match_addr_data'].strip().lower()
                    if not (RE_EMAIL.match(data)):
                        raise NotifyUserException(_('You must provide a valid email address.'))
                    match['addr'] = {
                        'matches':   True,
                        'type': 'str',
                        'data': data 
                    }
                    
                elif request.REQUEST['match_addr_type'] == 'domain':
                    data = request.REQUEST['match_addr_data'].strip().lower()
                    if not (RE_DOMAIN.match(data)):
                        raise NotifyUserException(_('You must provide a valid domain.'))
                    match['addr'] = {
                        'matches':   True,
                        'type': 'domain',
                        'data': data 
                    }
                    
                rule.match_json = json.dumps(match)
    
            if all([key in request.REQUEST for key in 'action_ignore', 'action_sync', 'action_sync_opp', 'action_create_contact', 'action_create_lead']):
                actions = []
                
                if bool(int(request.REQUEST['action_ignore'])):
                    actions.append({
                        'action':   'ignore'
                    })
                
                if bool(int(request.REQUEST['action_sync'])):
                    actions.append({
                        'action':   'sync',
                        'task_type':    request.REQUEST.get('action_sync_task_type')
                    })
                
                if bool(int(request.REQUEST['action_sync_opp'])):
                    actions.append({
                        'action':   'sync_opp',
                        
                    })
                                    
                if bool(int(request.REQUEST['action_create_contact'])):
                    actions.append({
                        'action':   'create',
                        'type':     'contact'
                    })                
                    
                if bool(int(request.REQUEST['action_create_lead'])):
                    actions.append({
                        'action':   'create',
                        'type':     'lead'
                        # TODO (eventually): a way to specify lead source
                    })
                    
                if bool(int(request.REQUEST['action_create_case'])):
                    actions.append({
                        'action':   'create',
                        'type':     'case',
                    })
                
                if bool(int(request.REQUEST['action_followup'])):
                    campaign_id = int(request.REQUEST['action_followup_campaign'])
                    if campaign_id <= 0:
                        raise NotifyUserException(_('You must specify a valid campaign ID.'))

                    actions.append({
                        'action':   'followup',
                        'campaign': request.REQUEST['action_followup_campaign']
                    })
                    rule.remote_user = RemoteUser.objects.filter(account=rule.owner_account, alias=None, enabled=True, type=RemoteUser.TYPE_FOLLOWUP)[0]
                
                if not actions:
                    raise NotifyUserException(_('You must specify at least one action.'))
                    
                rule.actions_json = json.dumps(actions)
            
            other_rules = EmailRule.objects.filter(owner_account=rule.owner_account)
            if rule.id:
                other_rules = other_rules.exclude(id=rule.id)
            
            for other in other_rules:
                if rule.is_equal(other):
                    raise NotifyUserException(_('This rule is a duplicate of rule #%d - cannot save.  Please use unique settings for each rule.') % other.id)
            
            rule.save()
            
            # Must do this part after the save, in case it's a new EmailRule
            #  (can't modify many-to-many relationship of an unsaved object)
            if 'regexes' in request.REQUEST:
                regex_ids = [int(i) for i in request.REQUEST['regexes'].split(',')] if request.REQUEST['regexes'] else []
                regexes = EmailRegex.objects.filter(id__in=regex_ids, owner_account=alias.account)
                if len(regexes) != len(regex_ids):
                    raise NotifyUserException(_('You do not have permission to access these data extractors.'))
                rule.regexes.clear()
                for r in regexes:
                    rule.regexes.add(r)
            
        else:
            raise NotImplementedError
        
        d = {
            'success': True,
        }
        return json_response(request,d)

    except NotifyUserException, e:
        d = {
            'success': False,
            'msg': e.message
        }
        return json_response(request,d)        
    except Exception, e:
        log(traceback.format_exc(e))
        d = {
            'success': False,
            'msg': _('Could not edit rules.') 
        }
        return json_response(request,d)
    

def clearsession(request):
    "For debugging"
    request.session.clear()
    return HttpResponse("ok")





        