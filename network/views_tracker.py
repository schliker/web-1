
#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import RequestContext, Context, loader, Template
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from lib.csrf.middleware import csrf_exempt
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os, pprint
from utils import *
from django.core import serializers
from assist.models import *
from assist.constants import SF_BUCKET_NAME
import traceback, pdb
from lib.ratelimitcache.ratelimitcache import ratelimit
    
@ratelimit(minutes = 30, requests = 2)
@csrf_exempt
def tracker_view(request, account_slug=None, owner_alias_id=None):
                    
    redirect_to = 'http://%s' % Site.objects.get_current().domain
    
    try:                     
        log("tracker_view start")
                            
        first_name = request.REQUEST.get('first_name', '')
        last_name = request.REQUEST.get('last_name', '')
        email = request.REQUEST.get('email', '').strip().lower()
        company = request.REQUEST.get('company', '')
        note_header = request.REQUEST.get('note_header', '')
        note_body = request.REQUEST.get('note', '')
        note_footer = request.REQUEST.get('note_footer', '')
        twitter = request.REQUEST.get('twitter', '')
        tag = request.REQUEST.get('tag', '')
        description = request.REQUEST.get('description', '')
        source = request.REQUEST.get('source', '')
        notify_alias_id = request.REQUEST.get('notify', '')
        reg = request.REQUEST.get('reg', '')
        referer = request.META.get('HTTP_REFERER', '')
        
        note = ('%s\n%s\n%s' % (note_header, note_body, note_footer)).strip()
        
        # For the followup API
        followup_campaign = request.REQUEST.get('followup_campaign', '')
        
        log(' email: ', email)
        log(' note: ', note)
        
        if not RE_EMAIL.match(email):
            msg = _('You must enter a valid e-mail address.')
            return HttpResponse("<script language='javascript'>alert('%s'); history.back(); </script>" % msg)
        
        if 'to' in request.REQUEST:
            redirect_to = request.REQUEST['to']
        elif 'to_c' in request.REQUEST:
            redirect_to = CRYPTER_TRACKER.Decrypt(request.REQUEST['to_c'])
        else:
            redirect_to = 'http://%s/info/forms/tracker_thanks/' % Site.objects.get_current().domain
            
        account = Account.objects.get(status=ACCOUNT_STATUS_ACTIVE, slug=account_slug)
        owner_alias = Alias.objects.active().get(pk=owner_alias_id, account=account)
                    
        last_tracked = (email, note, referer, account)
        
        log("[1]")
        
        # Only do tracker stuff if this isn't a dup
        if request.session and (request.session.get('last_tracked', None) != last_tracked):
            request.session['last_tracked'] = last_tracked
        
            log("[2]")
            
            contact, created = Contact.objects.get_or_create_smart(\
                first_name=first_name,
                last_name=last_name,
                description=description,
                email=email,
                company=company,
                twitter_screen_name=twitter,
                account=account,
                source_text=source,
                viewing_alias=owner_alias
            )
            
            if contact:
                #contact.update_last_contacted_date()
                contact.save()
            
                log("[3]")
                
                bucket, created = Bucket.objects.get_or_create_for_alias(owner_alias, 'tracker')
                bucket.contacts.add(contact)
                
                bucket_custom = None
                if tag:
                    bucket_custom, created = Bucket.objects.get_or_create_for_alias(owner_alias, tag)
                    bucket_custom.contacts.add(contact)
                
                if tag == SF_BUCKET_NAME:           # Adding a contact to the "salesforce" category sets it to sync
                    contact.sf_sync_pending = True
                contact.save()     
                
                if note:
                    log("[4]")
                    
                    # Make a note about this contact in the list. 
                    # If a row doesn't refer to a valid contact, just make a note.
                    if referer:
                        content = '%s -- %s' % (note, referer)
                    else:
                        content = note
                    import crawl.procmail
                    mode = crawl.procmail.procmail_quickbox(alias=owner_alias, content=content, \
                    ref_contact_id=contact.id,
                    ref_bucket_slug=bucket.slug if bucket else None)
                    
                    log('[5]')
                    
                    if bucket_custom and ('email' in mode):
                        contact.contact_buckets.add(bucket_custom)
                                
                if followup_campaign:
                    
                    try:
                        log("tracker_view: sending to followup campaign", followup_campaign," of alias ", contact.owner_alias)
                        
                        from remote.models import RemoteUser
                        ru = RemoteUser.objects.filter(type=RemoteUser.TYPE_FOLLOWUP, enabled=True, account=contact.owner_account)[0]
                        api = ru.get_api()
                        
                        api.followup([{
                            'email':        contact.email,
                            'first_name':   contact.first_name,
                            'last_name':    contact.last_name,
                            'company':      contact.company,
                            'source':       source or contact.source_text or ''          
                        }], campaign=followup_campaign)
                        
                    except:
                        log("tracker_view: could not send to followup campaign")
                        log(traceback.format_exc())
                
                if notify_alias_id:
                    try:
                        notify_alias = Alias.objects.active().get(id=notify_alias_id, account=account)
                        
                        c = Context({\
                            'alias': notify_alias,
                            'account': account,
                            'name': contact.get_full_name(),
                            'email': email,
                            'company': company,
                            'referer': referer,
                            'twitter': twitter,
                            'tag': tag,
                            'description': description,
                            'note': note,
                            'site': get_site(request)
                        })
                        
                        assistant = account.get_assistant()
                        log("Notifying alias %s about tracker submission" % str(notify_alias))
                        
                        from assist import mail    
                        mail.send_email('main_tracker_submit.html', c, 
                            _('Contact form: %s') % (contact.get_to_for_display()),
                            (assistant.get_full_name(), assistant.email),
                            (notify_alias.contact.get_full_name(), notify_alias.email),
                            noted={'message_type': EMAIL_TYPE_NOTED,\
                                'message_subtype': EMAIL_SUBTYPE_NOTED_NOTIFY_TRACKER,
                            }, reply_to=email
                        )
                    except:
                        log("Can't notify alias about the tracker form submission", notify_alias_id)
                        log(traceback.format_exc())
                                                
    except:
        log(traceback.format_exc())
        pass
                
    return HttpResponseRedirect(redirect_to)

