from django import template
from utils import *
from assist import constants
from network import ajax_feeds
from django.template import Context, loader, Variable
import pdb, traceback, copy, re
from assist import mail
from assist.models import *
from django.template.defaultfilters import escapejs, stringfilter
from django.utils.html import escape
from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
import hashlib

register = template.Library()

def last_slash(membership):
    """Given a 'membership' structure, return
    the name of the profile for the mouseover"""
    
    return membership['profile_url'].split('/')[-1]

def twitter_last_slash(membership):
    return '@' + last_slash(membership)
    
    
# Social sites for which we have icons
MEMBERSHIP_SITES = {
    'bebo': {
        'icon':                 'bebo.ico',
        'name':                 'Bebo'
    },
    'facebook': {
        'icon':                 'facebook.ico',
        'name':                 'Facebook',
        'profile_name_getter':  last_slash
    },    
    'flickr': {
        'icon':                 'flickr.ico',
        'name':                 'Flickr'
    },    
    'friendster': {
        'icon':                 'friendster.ico',
        'name':                 'Friendster'
    },
    'hi5': {
        'icon':                 'hi5.ico',
        'name':                 'Hi5'
    },
    'linkedin': {
        'icon':                 'linkedin.ico',
        'name':                 'LinkedIn',
        'profile_name_getter':  last_slash
    },
    'metroflog': {
        'icon':                 'metroflog.ico',
        'name':                 'MetroFlog'
    },
    'multiply': {
        'icon':                 'multiply.ico',
        'name':                 'Multiply'
    },
    'myspace': {
        'icon':                 'myspace.ico',
        'name':                 'MySpace',
        'profile_name_getter':  last_slash
    },
    'myyearbook': {
        'icon':                 'myyearbook.ico',
        'name':                 'myYearbook'
    },
    'plaxo': {
        'icon':                 'plaxo.ico',
        'name':                 'Plaxo'
    },
    'twitter': {
        'icon':                 'twitter.ico',
        'name':                 'Twitter',
        'profile_name_getter':  twitter_last_slash
    },
    'wordpress': {
        'icon':                 'wordpress.ico',
        'name':                 'WordPress'
    }
}


@register.filter
def membership_icon(membership):
    """Given a 'membership' structure from the social data,
    return the appropriate icon"""

    site_name = membership.get('site', '').split('.')[0].lower()
    if site_name in MEMBERSHIP_SITES:
        return MEMBERSHIP_SITES[site_name]['icon']
    else:
        return 'default.png'
    
@register.filter
def membership_title(membership):
    """Given a 'membership' structure from the social data,
    return a mouseover title for the icon"""

    site_name = membership.get('site', '').split('.')[0].lower()
    if site_name in MEMBERSHIP_SITES:
        if 'profile_name_getter' in MEMBERSHIP_SITES[site_name]:
            return '%s / %s' % (MEMBERSHIP_SITES[site_name]['name'], MEMBERSHIP_SITES[site_name]['profile_name_getter'](membership))
        else:
            return MEMBERSHIP_SITES[site_name]['name']
    else:
        return site_name.title()
    

    