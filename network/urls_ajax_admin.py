from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
admin.autodiscover()

urlpatterns = patterns('',
    (r'^edit_account_users/$', 'network.ajax_admin.edit_account_users'),
    (r'^edit_assistant_settings/$', 'network.ajax_admin.edit_assistant_settings'),
    (r'^edit_privacy_settings/$', 'network.ajax_admin.edit_privacy_settings'),
    (r'^edit_storage_settings/$', 'network.ajax_admin.edit_storage_settings'),
    (r'^display_assistant_email/$', 'network.ajax_admin.display_assistant_email'),
)
