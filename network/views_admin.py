try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os
from utils import *
from django.core import serializers
from assist.models import *
from twitter.models import *
from schedule.views import calendar
from assist.constants import *
from schedule.periods import *
from assist import mail, register
import traceback
from assist import constants

# We want to allow ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED account users to visit this page, so we do the global_def below with the special flag
def admin_view(request, subdomain=None):
    try:
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain) #TODO: create validate_admin_account function in utils
        if not alias.is_admin:
            raise Exception('Must be an admin to view this page.')
        
        # Only display the choices that the user can choose for this service level
        #  If the account has a value not in this list (because our team manually set it for them), 
        #  display that one too
        howlong_store_email_choices = []
        for val, text in Account.ACCOUNT_EMAIL_STORAGE_CHOICES:
            if val == -1:
                if (account.service.max_store_email_howlong is None) or (val == account.store_email_howlong):
                    howlong_store_email_choices.append((-1, text, (account.store_email_howlong == -1)))
            else:
                if (account.service.max_store_email_howlong is None) or (val <= account.service.max_store_email_howlong) or (val == account.store_email_howlong):
                    howlong_store_email_choices.append((val, text, (val == account.store_email_howlong)))
        
        try:
            twitteruser = TwitterUser.objects.get(account=account)
        except:
            twitteruser = None
                        
        try:
            from remote.models import RemoteUser
            followup_remote_user = RemoteUser.objects.get(account=account, alias=None, type=RemoteUser.TYPE_FOLLOWUP, enabled=True)
        except:
            followup_remote_user = None
        
        t = loader.get_template('main_admin_cover.html')
        c = Context({
            'base_url': subdomain,
            'alias': alias,
            'account': alias.account,
            'payments': Payment.objects.filter(account=alias.account).order_by('date'),
            'team_aliases': Alias.objects.filter(account=alias.account).order_by('date_created'),    # includes nonactive aliases
            'twitteruser': twitteruser,
            'followup_remote_user': followup_remote_user,
            'viewing_account_user': viewing_account_user,
            'howlong_store_email_choices': howlong_store_email_choices,
            'logo_url': alias.account.logo_url if (alias.account.logo_url and not alias.account.logo_url.startswith('http://')) else None # Don't show logos if they're from insecure domain

        })
        c.update(global_context(request))
        return HttpResponse(t.render(c))

    except:
        log('__admin_view__ fail: ', subdomain, traceback.format_exc())
        if subdomain:
            return HttpResponseRedirectAgent(request, '/dash/?name='+subdomain)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')
        
# Allow ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED folks to visit this page, so they can remove users to reactivate the account
# admin_view = global_def(admin_view, allow_inactive_max_users_exceeded=True)


@global_def
def approve_email(request, subdomain=None):
    try:
        alias, viewing_account_user, account = validate_account(request, subdomain=subdomain)
        if not alias.is_admin:
            raise Exception('Must be an admin to view this page.')
        if 'confirm_email' in request.GET:
            # Activate an Alias
            activate_email = request.GET['confirm_email']
                        
            old_status, activate_alias, already_active, success = register.activate_pending_alias(request,alias,activate_email)
            
            if already_active:
                msg = _("%s has already been activated in the %s network") % (activate_alias.email, activate_alias.account.base_url)
            elif not success:
                raise Exception()
            else:
                msg = _("%s has been activated in the %s network") % (activate_alias.email, activate_alias.account.base_url)
            request.messages['notices'].append(msg)
        
            return HttpResponseRedirectAgent(request, '/dash/'+alias.account.base_url+'/')
        else:
            raise Exception('missing confirmation email')
    except:
        log('__approve_email__ fail: ', subdomain, traceback.format_exc())
        
        msg = _("Failure: could not activate user")
        request.messages['notices'].append(msg)
        
        if subdomain:
            return HttpResponseRedirectAgent(request, '/dash/?name='+subdomain)
        else:
            return HttpResponseRedirectAgent(request, '/dash/')
