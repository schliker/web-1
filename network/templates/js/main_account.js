{% load i18n %}
{% load account_tags %}
{% minifyjs %}
var Page = this.Page = {
		
	agent: '{{agent|default_if_none:""}}',
	agent_url: '{{agent_url|default_if_none:""}}',					// URL of the containing window if we're in an iframe
	agent_url_root: '{{agent_url_root|default_if_none:""}}',		// Top of the domain of the URL of the containing window
	crawl_ongoing: ('{{crawl_ongoing}}' == 'True'),
	crawl_needed: ('{{crawl_needed}}' == 'True'),
	highlight_group_id:	null,
	
	total_new_emails: 0,		// conversations widget only
	increment_num_assigned_incomplete: function() {
		var val = $('#num_assigned_incomplete').text();
		if (val == '') {
			val = '1';
		} else {
			val = (parseInt(val) + 1) + '';
		}
		$('#num_assigned_incomplete').text(val).show();
	},
	
	expand_email: function(message_id) {
		Ajax.send_data('expand_email/', {message_id: message_id}, function(data) {
            if (data.return_data.success) {
                $('.message_text_'+message_id).html(data.return_data.body);
            } else {
                alert(data.return_data.msg);
            }
        }, "GET");
	},

	popup: function(URL) {
		day = new Date();
		id = day.getTime();
		eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=1,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=0,width=700,height=800,left=200,top=200');");
	},
	
	bind_actions: function() {

		//$('.action_icon').hide();
		//Page.kb_action_icons_enabled = true;
		//Page.kb_group = false;
		
		if (Page.agent == 'outlook') {
			// For newly loaded widgets, have Page.currentFocus track the currently focused thing
			//  so that the Tab key will work
			$("input").each(function() {
				$(this).focus(function () {
					Page.currentFocus = $(this);
				});
			});
		}
		
		//if (Page.agent == 'salesforce') return;		// no dropdown actions for Salesforce

		if (!/msie/i.test(navigator.userAgent))
			$('.actions_container').css({float:'right'});
		
		$('.message').mouseover(function(e) {
			var message = $(this);
			var msg_id = message.attr('id');
			if (msg_id)
                msg_id = msg_id.replace('message_','');
			
			/* find all other containers and hide them! */
			$('.actions_container').hide();
            if (msg_id)
			     $('#actions_container_'+msg_id).show();
			
		}).mouseout(function(e) {
			var message = $(this);
			//var container = message.find('.actions_container');
			//container.hide();
			
		});

	},
	
	unbind_actions: function() {
		$('.message').unbind('mouseover').unbind('mouseout');
	},
	
	reset_dashboard: function() {
		Page.view_followup_only(0);	// this clears Quickbox and resets search
	},
	
	go_back: function() {
		Page.update_selected_widgets('all', 'go_back');
        QuickBox.clear();
	},
	
	toggle_open_box: function() {
		var open_box_id = $('.open_box').attr('id');
	},

	bucket_add: function(callback, auto_select) {
		Prompt.prompt("{% trans 'Name your new tag:' %}", function(name) {
			if ((name != undefined) && (name != '')) {
				Ajax.send_data('edit_buckets/', {cmd: 'add', name: name, auto_select: auto_select ? 1 : 0}, function(data) {
					if (data.return_data.msg) {		// Print the message no matter what (if any)
						alert(data.return_data.msg);
					}
					if (data.return_data.success) {
						var update_widgets = true;
						if (callback != undefined) {
							update_widgets = callback(data.return_data.bucket_name, data.return_data.bucket_slug);
						}
						if (update_widgets) {
							Page.update_widgets();
						}
					}
				}, "GET");
			}
		});
	},

	bucket_remove: function() {
		var bucket_option = $('#bucket_select option:selected');
		var slug = bucket_option.attr('value');
		var name = bucket_option.text();
		if (slug == '') {
			alert("{% trans 'To delete a tag, first select it and then click the remove icon.' %}");
		} else {
			if (confirm("Are you sure you want to delete the tag " + name + "?")) {
				Ajax.send_data('edit_buckets/', {cmd: 'remove', slug: slug}, function(data) {
					if (data.return_data.success) {
						Page.update_widgets();
					} else {
						alert(data.return_data.msg);
					}
				}, "GET");
			}
		}
	},	
		
	request_perms: function(slug, name, has_perms) {
		if (has_perms) {
			Page.send_file_request(slug);
		} else {
			Ajax.send_data('get_file_owner/', {slug: slug}, function(data) {
	            if (data.return_data.success) {
	                if (data.return_data.owner) {
		                var confirm_message = "";
		                confirm_message += "{% trans 'Would you like to request permission to download the file' %} ";
		                confirm_message += name + "?\n";
		                confirm_message += "{% trans 'Click OK to send an email request to' %} ";
		                confirm_message += data.return_data.owner;
		                confirm_message += " {% trans 'for file access.' %}";
		                if (confirm(confirm_message)) {
			                Page.send_file_request(slug);
		                }		            
	        			//$('#modal_complete').hide();
	        			//$('#file_modal_slug').val(slug);
	        			//var file_obj = $('#file_'+slug)
	        			//$('#file_modal_name').text(name);
	        			//$('#alertcnt').show('fast');
	        			//$('.file_request_modal').jqm().jqmShow();
	                    //$('#file_modal_owner').html(data.return_data.owner);                    
	                } else {
	                    //$('#file_modal_owner').html('');
	                }
	            } else {
	                alert("{% trans 'Could not access this file.' %}");
	            }
	        }, "GET");
		}
    },
    request_perms_modal: function() {
        var slug = $('#file_modal_slug').val();
		Page.send_file_request(slug);
    },
    send_file_request: function(slug) {
    	Ajax.send_data('request_file_perms/', {slug: slug}, function(data) {
            if (data.return_data.success) {
                if (data.return_data.has_perms) {
                	if (Page.agent == 'firefox') {
                		window.location = 'http{% if has_ssl %}s{% endif %}://{{site.domain}}' + data.return_data.url;
                	} else {
                		window.location = data.return_data.url;
                	}
                } else {
                    //$('#alertcnt').hide();
                    //$('#modal_complete').show('fast');
                    alert("{% trans 'File access request has been sent.' %}\n" +
                          "{% trans 'Please provide the file owner some time to approve your access to the file.' %}");
                }
            } else {
                alert("{% trans 'Could not process your request.' %}");
            }
        }, "GET");
    },

    num_crawl_display_failures: 0,
    max_crawl_display_failures: 3,

    update_crawl_display: function(id) {
        Ajax.send_data('get_task_status/', {task: id}, function(data) {
            // Returns XML, with json inside of <extradata> tags
            var extradata = $("extradata", data.return_data).text();
            extradata = eval('(' + extradata + ')');
            
            if (extradata.success) {
                if (extradata.status == 'tSTA') {
	                $('#crawl_message').text(extradata.message);
	                if (extradata.custom) {
		                // Progress indicator (contacts, emails)
	                    $('#crawl_percent').text(extradata.custom.percent_done);
		                $('#crawl_loader').css({width:extradata.custom.percent_done+'%'});	                
						if (extradata.custom.date) {
							// Email subjects 
		                    $('#crawl_date').text(extradata.custom.date);
		                    $('#crawl_folder').text(extradata.custom.folder);
		                    $('#crawl_subject').text(extradata.custom.subject);
		                    $('#crawl_details').show();
						}
	                } else {
	                    $('#crawl_details').hide();
	                }
                } else if (extradata.status == 'tFIN') {
					if (Page.intval != undefined) {
						clearInterval(Page.intval);
						Page.intval = undefined;
					}
					if (extradata.job_success) {
						$('#crawl_icon').attr('src', '/static/vistaico/Symbol-Check-24.png');
						$('#crawl_loader').css({width:'100%'});
						$('#crawl_percent').text('100');
						$('#crawl_message').html("{% trans 'Done!' %} ");
						if (extradata.custom.refresh_needed > 0) {
							Page.force_refresh_selected_widget('Conversations');
						}
					} else {
						$('#crawl_icon').attr('src', '/static/vistaico/Symbol-Error-24.png');
						$('#crawl_message').text(extradata.message);
					}
					$('#crawl_details').hide();
	                Page.intval = setInterval('Page.hide_crawl_display(false);', 4000);
                }
                
            } else {
				Page.num_crawl_display_failures += 1;
				if (Page.num_crawl_display_failures >= Page.max_crawl_display_failures) {
					Page.hide_crawl_display(false);
					Ajax.failure_logout("{% trans 'Could not connect to your inbox.' %}");
				}
            }
		}, "GET", "xml");			
    },
    hide_crawl_display: function(refresh) {
		if (Page.intval != undefined) {
			clearInterval(Page.intval);
			Page.intval = undefined;
		}
		$('#crawl_display').hide('slow');
		if (refresh) {
			window.location = '?';
		}
    },
    intval:	undefined,
    crawling: false,
    
    crawl_alias: function(new_crawl) {
        Ajax.send_data('crawl_alias/', {new_crawl: new_crawl ? '1' : '0'}, function(data) {
            if (data.return_data.success) {
                if (data.return_data.id) {
                	$('#crawl_message').text('');
            		$('#crawl_icon').attr('src', '/static/images/ajax-loader-small.gif');
                	$('#crawl_details').hide();    	
                    $('#crawl_percent').text('0');
                    $('#crawl_loader').css({width:'0%'});
                    $('#crawl_date').text('');
                    $('#crawl_folder').text('');
                    $('#crawl_subject').text('');
                	$('#crawl_display').show();
	                Page.crawling = true;
	                Page.update_crawl_display(data.return_data.id);
	                Page.intval = setInterval('Page.update_crawl_display("' + data.return_data.id + '");', 5000);
                }
            } else {
                $('#crawl_message').text("Could not connect to your inbox.");
                Page.intval = setInterval('Page.hide_crawl_display(false);', 3000);
            }
		}, "GET");
    },
	
    _create_widget: function(id, position, html, mode) {
        var widget_html = "<div class='widget_position_" + position + "' id='widget_" + id + "'>" + html + "</div>";
        
		if (Page.agent != "" && Page.agent != "salesforce") {
			var widget_container = $('#widgets');
		} else {
			var widget_container = $('#widgets_'+position);
		}

		if (mode=='append') {
			widget_container.append(widget_html);
		} else if (mode=='prepend') {
			widget_container.prepend(widget_html);
		}
    },
    
    _update_widget: function(w, type) {

        if (type == 'xml') {
			var id = w.attr('id');
			var position = w.attr('position');
			var name = w.attr('name');
			var html = w.text();
			var action = w.attr('action');
        } else {
			var id = w.widget_id;
			var position = w.position;
			var name = w.name;
			var html = w.html;
			var action = w.action;
        }
        
        var widget_id = "widget_" + id;
        
        if (action == 'replace_or_prepend_widget') {
	        if ($('#'+widget_id).length > 0) {
	        	var old_position = $('#'+widget_id).attr('class');
	        	old_position = old_position.replace('widget_position_', '');
	        	
	        	if (old_position != position) {
	        		$('#'+widget_id).remove();
	    			Page._create_widget(id, position, html, 'prepend');
	        	} else {
	            	$('#'+widget_id).html(html);
	        	}
	        	
	        } else {
				Page._create_widget(id, position, html, 'prepend');
	        }   
            Page.unbind_actions();
            Page.bind_actions();
        } else if (action == 'replace_or_append_widget') {

	        if ($('#'+widget_id).length > 0) {
	        	var old_position = $('#'+widget_id).attr('class');
	        	old_position = old_position.replace('widget_position_', '');
	        	
	        	if (old_position != position) {
	        		$('#'+widget_id).remove();
	    			Page._create_widget(id, position, html, 'append');
	        	} else {
	            	$('#'+widget_id).html(html);
	        	}
	        	
	        } else {	
				Page._create_widget(id, position, html, 'append');
	        } 
            Page.unbind_actions();
            Page.bind_actions();
        } else if (action.match('append_within_')) {

        	$('.newsfeed_item_highlight').removeClass('newsfeed_item_highlight');

            var msg_id = $(html).find('.kb_group').attr('id');
            msg_id = msg_id.replace('kb_','');

			if (msg_id && msg_id != '' && (!$('#kb_'+msg_id).length > 0)) { // only do the append if no previous and existing msg id exists.
            	$('#'+action.replace('append_within_', '')).append(html);
            	//alert('appended!');
	            Page.unbind_actions();
	            Page.bind_actions();
			}
        } else if (action.match('prepend_within_')) {
        	$('.newsfeed_item_highlight').removeClass('newsfeed_item_highlight');
            //alert("ACTION: " + action);
        	//alert(id);
			//alert(position);
			//alert(name);
			//alert('XXXXXXXXXXXXX');
			//alert('Got here: prepend_within_');
			
			var msg_id = $(html).find('.kb_group').attr('id');
            msg_id = msg_id.replace('kb_','');

            //alert(msg_id);

            if (msg_id && msg_id != '' && (!$('#kb_'+msg_id).length > 0)) {
            	if ( $('#'+action.replace('prepend_within_', '')).length > 0) {
    				//alert('[]found');
               		$('#'+action.replace('prepend_within_', '')).prepend(html);
    			} else {
    				//alert('[]notfound');
    				//$('#widget_'+name).prepend(html);
    			}
            }
			
            Page.unbind_actions();
            Page.bind_actions();
        } else if (action.match('new_conversations_')) {
             // For the Conversations widget only
        	$('#'+widget_id+'_new_emails_container').show();
        	var num_new_emails = parseInt(action.replace('new_conversations_', ''));
        	Page.total_new_emails = num_new_emails;
        	$('#'+widget_id+'_new_emails_counter').text(Page.total_new_emails.toString());
        	$('#'+widget_id+'_new_emails').prepend(html);
        	Page.unbind_actions();
            Page.bind_actions();
       	} else if (action == 'none') {
            // do nothing
        }
    },

    _update_dash_display: function(search_display, bucket_display) {
        if (search_display != '') {
    		$('#dash_buttons_current_search').css('display','inline').text(search_display);
        } else {
     		$('#dash_buttons_current_search').hide();
        }
        
        if (bucket_display != '') {
    		$('#dash_buttons_current_bucket').css('display','inline').text(bucket_display);
        } else {
     		$('#dash_buttons_current_bucket').hide();
        }

        if (search_display != '' || bucket_display != '') {
    		$('#dash_buttons_back').css('display','inline');
        } else {
        	$('#dash_buttons_back').hide();
        }
    },

    _update_widgets: function(data, type) {
        if (typeof(type) == 'undefined')
            type = 'json';
        if (type == 'xml') {
			$("widget", data).each(function() {
				Page._update_widget($(this), type);
			});
			Page._update_dash_display($("search_display", data).text(), $("bucket_display", data).text());
        } else {
    		for (var i=0; i < data.widgets.length; i++) {
    			Page._update_widget(data.widgets[i], type);
    		}
    		Page._update_dash_display(data.search_display, data.bucket_display);
        }
    },
    
    _hide_loading_images: function () {
		$('.load_widgets').hide();
		$('.logout_link').show();		// iphone/outlook
    }, 
    
    detect_user_agent: function() {
        // Opera 8.0+
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;

        // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;  
        
    },
    
    update_selected_widgets: function(widget_ids, cmd, arg1, arg2, callback) {
		widget_ids = typeof(widget_ids) != 'undefined' ? widget_ids : false;
        cmd = typeof(cmd) != 'undefined' ? cmd : false;
		arg1 = typeof(arg1) != 'undefined' ? arg1 : false;
		arg2 = typeof(arg2) != 'undefined' ? arg2 : false;
                
		var type = ((false || !!document.documentMode) || (Page.agent == 'firefox')) ? 'json' : 'xml';

		Ajax.send_data('get_widgets/', {widget_ids: widget_ids, cmd: cmd, arg1: arg1, arg2: arg2, type: type}, function(data) {

			if (data.return_data) {
				
				var success;
				var msg;
				var more;
				if (type == 'json') {
					success = data.return_data.success;
					msg = data.return_data.msg;
					more = data.return_data.more;
				} else if (type == 'xml') {
					success = ($("success", data.return_data).text() == 'true');
					msg = $("msg", data.return_data);
					more = ($("more", data.return_data).text() == 'true');
					if (msg != null) {
						msg = msg.text();
					}	
				}
				
				if (success) {
					
					Page._update_widgets(data.return_data, type);
					
					if (more) {
						// This is invoked when we're getting one widget at a time (using the one_stale mechanism)
						// On subsequent calls, use "0" for arg1 to indicate this isn't the first call in the sequence
						Page.update_selected_widgets(widget_ids, cmd, '0', arg2, callback);
					} else {
						Page._hide_loading_images();
	       				if (typeof(callback) != 'undefined') {
	           				callback();
	       				}
					}
				
		            //$('.kb_'+Page.highlight_group_id).addClass('newsfeed_item_highlight');

				} else if (msg) {
					alert(msg);
				} else {
					Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
				}

			}
		}, "GET", type);
    },

    update_widgets: function() {
        Page.update_selected_widgets('updated');
    },

    update_all_widgets: function(callback, all_at_once) {
        if (all_at_once) {
        	Page.update_selected_widgets('all', undefined, undefined, undefined, callback);
        } else {
            // Update only one widget at a time, and keep doing AJAX calls until all are done
        	// The '1' means this is the first call in a chain
        	// The Ajax success function of update_selected_widgets calls again, but with 0
        	Page.update_selected_widgets('one_stale', undefined, '1', undefined, callback);
        }
    },
    
    force_refresh_all_widgets: function() {
        Page.update_selected_widgets('force_refresh_all');
    },

    force_refresh_selected_widget: function(widget_id) {
		Page.update_selected_widgets('force_refresh_' + widget_id);
    },
        
    toggle_size: function(widget_id) {
        
        if (Page.agent == '') {
        	Page.update_selected_widgets(widget_id, 'toggle_size');
        }
    },

    trim: function(str) {
    	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    },

    change_page: function(widget_id, page_num) {
        Page.update_selected_widgets(widget_id, 'page', page_num);
    },
    
    search: function(search_query, search_type, widget_id) {
		//alert(widget_id+' '+search_query);
		if (typeof(widget_id) == 'undefined') {
			widget_id = 'all';
		}
	    Page.update_selected_widgets(widget_id, 'search', search_query, search_type);
    },

    change_bucket: function(bucket_slug, bucket_name, widget_id) {
		if (typeof(widget_id) == 'undefined') {
			widget_id = 'all';
	        QuickBox.clear();
	        QuickBox.bucket_slug = bucket_slug;
	        QuickBox.bucket_name = bucket_name;
	        QuickBox.update_tooltip();
		}
        Page.update_selected_widgets(widget_id, 'bucket', bucket_slug);
    },

	activate_contact: function(contact_id, contact_name) {
    	var create = confirm("{% trans 'Would you like to create a contact for' %} " + contact_name + "?");
    	if (create) {	
        	// Create the contact and zoom into it
			Ajax.send_data('contacts_action/', {cmd: 'activate_contacts', contact_ids: contact_id }, function(data) {
				if (data.return_data.success) {				
	               	Page.update_widgets();
				} else {
					alert(data.return_data.msg);
					//Ajax.failure_logout("{% trans 'Could not connect to server.' %}");	// Don't log out here, Twitter may just be down
				}
			}, "GET"); 
    	} else {
			// Just zoom in
			Page.search(contact_id, '{{constants.SEARCH_TYPE_CONTACT}}');
    	} 
	},

	/*
	 * When creating a new SF object from the actions menu, call this 
	 *  type: one of 'lead', 'contact', 'case'
	 *  data: the structure returned by email_action
	 *  Thanks to http://salesforce.phollaio.com/2007/04/02/how_to_obtain_a_field_id/
	 */
	
	sf_create: function(type, data) {
		var full_url = Page.agent_url_root;
		if (full_url == "")
			full_url = 'https://na7.salesforce.com/'; //hard code the full url, just in case.
		
		var contact_data = data.return_data.contact;
		
		if (type == 'lead') {
			full_url += '00Q/e';
			full_url += '?name_firstlea2=' + escape(contact_data.first_name);
			// For required fields, don't provide them in the URL unless we have values -- 
			//  otherwise SF displays an error
			if (contact_data.last_name != '') full_url += '&name_lastlea2=' + escape(contact_data.last_name);
			if (contact_data.company != '') full_url += '&lea3=' + escape(contact_data.company);
			full_url += '&lea4=' + escape(contact_data.title);
			full_url += '&lea17=' + escape(contact_data.description);
			full_url += '&lea8=' + escape(contact_data.phone);
			full_url += '&lea11=' + escape(contact_data.email);
			full_url += '&lea12=' + escape(contact_data.website);
		} else if (type == 'contact') {
			full_url += '003/e';
			full_url += '?name_firstcon2=' + escape(contact_data.first_name);
			if (contact_data.last_name != '') full_url += '&name_lastcon2=' + escape(contact_data.last_name);
			full_url += '&con5=' + escape(contact_data.title);
			full_url += '&con20=' + escape(contact_data.description);
			full_url += '&con10=' + escape(contact_data.phone);
			full_url += '&con15=' + escape(contact_data.email);
		} else if (type == 'case') {
			full_url += '500/e?';
			full_url += '&cas11=Email';
			if (contact_data.name != '') full_url += '&cas3=' + escape(contact_data.name);
			full_url += '&cas14=' + escape(data.return_data.subject);
			full_url += '&cas15=' + escape(data.return_data.body);
		}
		
		Page.popup(full_url);
	},
	
    email_action: function(select_box, message_id) {
        var action = select_box.val();
        var value_text = select_box.find("option:selected").text();
        var completed = 0;
        var alias_ids = '';
        var contact_ids = '';
        var category_slugs = '';
        var twitter_screen_name = '';

        var params = {}
        params.message_id = message_id;
        
        if (action == 'incomplete') {
            params.cmd = 'mark_incomplete';
            params.completed = 0;
        } else if (action == 'completed') {
            params.cmd = 'mark_completed';
            params.completed = 1;
        } else if (action.match('assign_')) {
            params.cmd = 'add_aliases';
			params.alias_ids = action.replace('assign_', '');
        } else if (action.match('follow_')) {
			params.cmd = 'follow';
			params.contact_ids = action.replace('follow_', '');
        } else if (action == 'retweet') {
            params.cmd = 'retweet';
        } else if (action == 'delete') {
    		if (confirm("{% trans 'Are you sure?' %}")) {			
    			params.cmd = 'delete';
    		} else {
				select_box.val('default');
				return;
    		}
        } else if (action == 'delete_thread') {
    		if (confirm("{% trans 'Are you sure you want to delete this entire thread?' %}")) {			
    			params.cmd = 'delete_thread';		// for now, delete and delete_thread are the same command
    		} else {
				select_box.val('default');
				return;
    		}
            
        } else if (action.match('sf_create_')) {	// sf_create_lead, contact, case
            params.cmd = action;
        }
        
		Ajax.send_data('email_action/', params, function(data) {
			if (data.return_data.success) {				
				if (params.cmd == 'mark_completed') {
					$('.message_'+message_id).addClass('message_completed');	//.removeClass('message_incomplete');
					var container = $('.message_'+message_id).find('.actions_container');
					select_box.val('default');
				} else if (params.cmd == 'mark_incomplete') {
					$('.message_'+message_id).removeClass('message_completed');	//.addClass('message_incomplete');
					var container = $('.message_'+message_id).find('.actions_container');
					select_box.val('default');
				} else if (params.cmd == 'delete') {
					$('.message_'+message_id).hide(100);
				} else if (params.cmd == 'add_aliases') {
					if (data.return_data.updated) {
						$("<span></span>")
							.text(' {% trans "assigned to" %} ' + data.return_data.ez_names)
							.appendTo('.extra_text_'+message_id);
							
							if (params.alias_ids == '{{alias.pk}}') {
								Page.increment_num_assigned_incomplete();
							}
					}
					select_box.val('default');
					
				} else if (params.cmd == 'add_to_category') {
					if (data.return_data.updated) {
						var value_str = value_text.split(": ")[1];
						$("<span style='margin-right: 5px; background: #3465A4; padding: 0px 4px 0px 4px; font-size: .9em; color: #fff;' class='rounded_upper rounded_lower'></span>")
							.html('<span onclick=\'Page.change_bucket("'+category_slugs+'", "'+value_str+'"); return false;\' style="cursor: pointer;">'+value_str+'</span>')
							.appendTo('#bucket_list_'+message_id);
						select_box.val('default');
					}
				} else if (params.cmd.match('sf_create_')) {
					Page.sf_create(params.cmd.replace('sf_create_', ''), data);
					select_box.val('default');
				} else {
					select_box.val('default');
                	Page.update_widgets();
				}
			} else {
				alert(data.return_data.msg);
				//Ajax.failure_logout("{% trans 'Could not connect to server.' %}");	// Don't log out here, Twitter may just be down
			}
		}, "GET");  
    },

    view_followup_only: function(followup_only) {
		Page.update_selected_widgets('all', 'view_followup_only', followup_only);
        QuickBox.clear();
        $('.view_followup_only').removeClass('selected');
        $('#view_followup_only_'+followup_only).addClass('selected');
    },
    
    // iphone/outlook
	currentFocus:	null,
	attach_focus_all: function() {				// iphone/outlook
		$("input:visible").each(function() {
			$(this).focus(function () {
				Page.currentFocus = $(this);
			});
		});
	},
    logout: function() {

	    // For the Outlook plugin
        try {
        	window.external.Callback_ClearLoginData();
        	// Also do the window.location below
        } catch(err) {}

        // For the Firefox/Chrome plugin
        if (Page.agent == 'firefox') {
        	        	
    		// Log the user out and reload the sidebar.
    		Ajax.send_data('logout/', {},
    			function(data) {
    				// Don't check data.return_data.success - reload the sidebar even if the logout "fails"
    				//  (which can happen if we're already logged out)
	    			var button = document.getElementById('calenvy_reload_sidebar_button');
					if (button != null) {
						button.click();
					} else {	
						//window.location.reload();
					}
    		}, "GET");
    		
        } else {
        	// For everyone else
        	window.location = '/logout/?agent={{agent|urlencode}}&to=%2Flogin%2F';
        }
    },

    // Create pretty HTML of the features belonging to a service level
	get_service_features_html: function(features) {
    	
        var html = "";
        for (var i=0; i < features.length; i++) {
			// do something with features[i] 
			var is_checked = features[i].value;				
			if (is_checked) 
				html += "<input disabled='disabled' type='checkbox' checked='checked'/> " + features[i].name + "<br/>";
			else
				html += "<input disabled='disabled' type='checkbox'/> " + features[i].name + "<br/>";
        }
		return html;
	},
	
	// Make the expand parse box bigger when clicked on (for iphone/firefox/outlook)
	expand_parse_box: function(box_obj) {	
		$('#parse_submit').show();
		$(box_obj).css({color: '#000', height: '35px'});
		if ($(box_obj).val() == "{% trans 'Type something...' %}")
			$(box_obj).val('');
	},
	
	// Call this when main_account.html is loaded
	ready: function() {
		
		Page.update_all_widgets(function () {
			if (Page.crawl_needed) {
				Page.crawl_alias(true);
			}
		}, false);
		
		QuickBox.update_tooltip();

		$("#parse").click(function(e) {
			Page.expand_parse_box(this);
		});

		$("#parse").focus(function(e) {
			Page.expand_parse_box(this);
		});
	}

}

function search (widget_id, search_text) {
    Page.update_selected_widgets('all', 'search', search_text);
}


/*
 * Contacts widget 
 */

var Contacts = this.Contacts = {
	
	agent: '{{agent}}',
	widget_id: 'contacts',

	state: function() { 
		return ($('#contact_detail_id').length > 0) ? 'expanded' : 'false' 
	},

	contact_detail_id: function() { 
		if (Contacts.state() == 'expanded') {
			return $('#contact_detail_id').val();
		} else {
			return 0;
		}
	},

	contact_detail_slug: function() { return $('#contact_detail_slug').val(); },
	contact_detail_email: function() { return $('#contact_detail_email').val(); },
	contact_detail_first_name: function() { return $('#contact_detail_first_name').val(); },
	contact_detail_last_name: function() { return $('#contact_detail_last_name').val(); },
	contact_detail_name: function() { return $('#contact_detail_name').val(); },
	contact_detail_display_name: function() { return $('#contact_detail_display_name').val(); },
	prev_contact_detail_id: function() { return $('#prev_contact_detail_id').val(); },
	prev_contact_detail_display_name: function() { return $('#prev_contact_detail_display_name').val(); },
	
	cloned_buckets_mode: '',		// empty string, 'add_contact' or 'remove_contact'

	crunchbase: function() {
		$.getJSON("https://api.crunchbase.com/v/1/person/" + escape(Contacts.contact_detail_first_name() + Contacts.contact_detail_last_name()) + ".js",
		        function(data){
					$('#crunchbase_widget').show().html('Loading...');
	          		//$.each(data.items, function(i,item){
		          	//  $("<img/>").attr("src", item.media.m).appendTo("#images");
		          	//  if ( i == 3 ) return false;
		});
		     			
	},
	
	expand: function(id_or_slug, mode, email, contact_name, owner_name) {
		if (typeof(email) == 'undefined') email = '';
		if (typeof(contact_name) == 'undefined') contact_name = '';
		Ajax.send_data('expand_contact/', {widget_id: Contacts.widget_id, id_or_slug: id_or_slug, mode: mode, email: email, name: contact_name}, function(data) {
			if (data.return_data.success) {
				{% comment %}The order matters here: IE requires to do insertAtCaret before the Ajax{% endcomment %}
				QuickBox.clear_note_only();	// Leave the bucket selection in place
				if (mode == 'new') {
					QuickBox.contact_id = '';
					QuickBox.contact_name = '';
				} 
				//if ($("#note_box").is(':visible')) {
				//	$("#parse").val("").insertAtCaret(data.return_data.contact_email + " ");
				//}
				Page._update_widgets(data.return_data, 'json');
			} else {
				if ((Contacts.agent == 'outlook') || (Contacts.agent == 'firefox')) {
					Contacts.collapse();
				} else {
					alert(data.return_data.msg);
				}
			}
		});
	},
	
	remove_social_data: function(contact_id, key, target, callback) {
		if (typeof(target) != 'undefined' && target != null)
			$(target).fadeTo(1000,.5);
    	Ajax.send_data('edit_social_data/', {'cmd': 'delete', id: contact_id, key: key}, function(data) {
            if (data.return_data.success) {
        		if (typeof(target) != 'undefined' && target != null)
        			$(target).hide();
        		if (typeof(callback) != 'undefined' && callback != null) {
        			callback();
        		}
            } else {
                alert(data.return_data.msg);
            }
        }, "GET");
	},
	
	remove_social_image_callback: function() {
		// The callback for deleting the image from the Contacts widget
		$('#contact_image').attr('src', '/static/images/no_person.png').removeClass('has_image');
		$('#contact_image_delete').hide();
	},
		
	edit_secondary_email: function(id, cmd, email) {
		if (email && email != '') {
			Ajax.send_data('edit_contact_extra_info/', {id: id, cmd: cmd, widget_id: Contacts.widget_id, type: 'email', data: email},
				function(data) {
					if (data.return_data.success) {
						Page._update_widgets(data.return_data, 'json');
					} else {
						alert(data.return_data.msg);
					}
				}, "GET");
		}
	},
	
	add_secondary_email: function() {
		Contacts.edit_secondary_email(Contacts.contact_detail_id(), "add", $("#add_secondary_email").val());
	},
	
	update: function (id, create_new) {
		var email = $('#box_contact_email').val();
		var phone = $('#box_contact_phone').val();
		var title = $('#box_contact_title').val();
		var company = $('#box_contact_company').val();
		var website = $('#box_contact_website').val();
		var description = $('#box_contact_description').val();
		var name = $('#box_contact_name').val();
		var twitter_screen_name = $('#box_contact_twitter_screen_name').val();
		if ((email.replace(/^\s+|\s+$/, '') == '') && (twitter_screen_name.replace(/^\s+|\s+$/, '') == '')) {
			alert("{% trans 'Contact must have an email address or Twitter name.' %}");

		} else {
			Ajax.send_data('update_contact/', {id: id, widget_id: Contacts.widget_id, email: email, title:title, twitter_screen_name: twitter_screen_name, phone: phone, company:company, website: website, description: description, name:name, create_new: create_new},
				function(data) {
					if (data.return_data.success) {
						if (create_new) {
							QuickBox.clear_note_only();	// Leave the bucket selection in place
							QuickBox.contact_id = '';
							QuickBox.contact_name = '';
						} 
						Page._update_widgets(data.return_data, 'json');
					} else {
						alert(data.return_data.msg);
					}
				}, "GET"); 
		}
	},
	
	// Leave edit mode without saving the contact
	cancel_edit: function (id) {
		if (id == 0) {
			// For new contacts, there is no contact detail page to go back to -- just go to the contact list view
			Contacts.collapse();
		} else {
			Ajax.send_data('cancel_edit_contact/', {id: id, widget_id: Contacts.widget_id},
				function(data) {
					if (data.return_data.success) {
						Page._update_widgets(data.return_data, 'json');
					} else {
						alert(data.return_data.msg);
					}
				}, "GET");
		}
	},
	
	collapse: function() {	
		Page.go_back();
		/*
		Ajax.send_data('collapse_contact/', { widget_id: Contacts.widget_id }, function(data) {
			if (data.return_data.success) {
				QuickBox.contact_id = '';
				QuickBox.contact_name = '';
				QuickBox.clear_note_only();
				QuickBox.update_tooltip();
				$('#dash_buttons_back').hide();
				$('#dash_buttons_current_search').hide();
				Page._update_widgets(data.return_data);
			} else {
				Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
			}
		});*/			
	},

	delete_one_contact: function(id) {
		if (confirm("{% trans 'Are you sure you want to delete this contact?' %}")) {
			Ajax.send_data('contacts_action/', { cmd: "delete", widget_id: Contacts.widget_id, contact_ids: id }, function(data) {
				if (data.return_data.success) {
					$(this).attr("disabled", "disabled");
					$(".box_contact_details").fadeTo(20,.5); 
					Contacts.collapse();
				} else {
					Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
				}
			}, "GET");		
		} else {
			$('#box_contact_delete').attr('checked', false);
		}	
	},
	
	set_cloned_bucket_list_mode: function(mode) {
		if (Contacts.cloned_buckets_mode != '') {
			$("#box_contact_details_select_cat").hide();
			Contacts.cloned_buckets_mode = '';
		} else {
			Contacts.cloned_buckets_mode = mode;
			$("#box_contact_details_select_cat").show();	
		}
	},

	select_cloned_bucket: function(bucket_slug, contact_id) {
		if (bucket_slug == 'new') {
			Contacts.bucket_add();
			return;
		}
		if (Contacts.cloned_buckets_mode != '') {
			Ajax.send_data('edit_buckets/', { cmd: Contacts.cloned_buckets_mode, slug: bucket_slug, widget_id: Contacts.widget_id, contact_id: contact_id }, function(data) {
				if (data.return_data.success) {
					Page.update_widgets();
				} else {
					Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
				}
			}, "GET");					

			$("#box_contact_details_select_cat").hide();
			Contacts.cloned_buckets_mode = '';
		}
	},

	toggle_all_checkboxes: function() {
		if ($('#contact_checkbox_all').is(':checked')) {
			$('input[type="checkbox"].contact_checkbox').attr('checked', true);
			$('#contact_checkbox_all').attr('title', '{% trans "Unselect all contacts" %}');
		} else {
			$('input[type="checkbox"].contact_checkbox').attr('checked', false);
			$('#contact_checkbox_all').attr('title', '{% trans "Select all contacts" %}');
		}
		Contacts.pre_action_contact_select(); //shows or hides the action items if contacts are all checked
	},

	open_compose_window: function(contact_ids) {
		// Open the compose window - contact_ids is a string, a comma-separated list of contact IDs
		if (Contacts.agent == 'iphone') {
			var url = "/dash/{{alias.account.base_url}}/contacts_compose/?agent=iphone&contact_ids="+encodeURI(contact_ids);
			window.location=url;
		} else {
			// TODO: Add ?agent here if necessary
			var url = "/dash/{{alias.account.base_url}}/contacts_compose/?contact_ids="+encodeURI(contact_ids);
			window.open(url, "ContactsCompose", "menubar=0,width=450,height=530,toolbar=0,navigation=0,status=0,location=0,directories=0");
		}		
	},
	
	pre_action_contact_select: function() {
		// count number of selected and enable action items if selected
		var selected = 0;
		$('input[type="checkbox"].contact_checkbox').filter(':checked').each(function (i) {
			selected += 1;
		});
		
		if (selected > 0)
			$('#action_contact_bind').show();
		else
			$('#action_contact_bind').hide();
		return selected;
	},
	
	pre_action_contact_bind: function() {
		// run this on page load
		var selected = 0;
		$('input[type="checkbox"].contact_checkbox').change(function (i) {
			Contacts.pre_action_contact_select();
		});
	},
	
	action: function(action, contact_id) {
		var contact_ids = '';
		
		if (typeof(contact_id) == 'undefined') {
			// Get the list of contacts that are checked (in the collapsed view)
			$('input[type="checkbox"].contact_checkbox').filter(':checked').each(function (i) {
				var id = $(this).attr('id').replace('contact_checkbox_', '');
				contact_ids += (id + ',');
			});
			
		} else {
			// Single contact is specified
			contact_ids += (contact_id + ',');
		}

		contact_ids=contact_ids.replace(/,$/, '');
		
		if (contact_ids.length > 0) {

			if (action == 'delete') {
				var is_conf = confirm("{% trans 'Are you sure you want to delete the selected contact(s)?' %}");
				if (is_conf) {
					Ajax.send_data('contacts_action/', { cmd: action, widget_id: Contacts.widget_id, contact_ids: contact_ids }, function(data) {
						if (data.return_data.success) {
							$('input[type="checkbox"].contact_checkbox').filter(':checked').each(function (i) {
								var id = $(this).attr('id').replace('contact_checkbox_', '');
								$('#contact_item_'+id).hide();
							});
							//Page.update_widgets();
						} else {
							alert(data.return_data.msg);
						}
					}, "GET");		
				}
			} else if (action == 'compose') {	
				Contacts.open_compose_window(contact_ids);
			} else if (action == 'categorize') {
				var selection = $('option:selected','#box_contact_action_categorize');
				var bucket_slug = selection.val();
				if (bucket_slug == '') 
					return;
				else if (bucket_slug == 'new') {
					Contacts.bucket_add();
					return;
				}
				var is_conf = confirm("{% trans 'Are you sure you want to tag the selected contact(s) as:' %} " + selection.text());
				if (is_conf) {
					Ajax.send_data('contacts_action/', { cmd: "categorize", widget_id: Contacts.widget_id, contact_ids: contact_ids, bucket_slug: bucket_slug }, function(data) {
						if (data.return_data.success) {
							//Page.update_widgets();
						} else {
							Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
						}
					}, "GET");
				}				
			}
		} else {
			alert("{% trans 'Select contacts, then choose an action at the top (compose, delete, etc).' %}");
		}
		$('#box_contact_action_categorize').val('');
	},

    bucket_add: function() {
		// Wrapper for Page.bucket_add specific to the Contacts widget,
		//  adds the new category to the select list
		Page.bucket_add(Contacts.bucket_add_callback);
    }, 

    bucket_add_callback: function(name, slug) {
    	// The callback for bucket_add
    	$('.buckets_select').each(function(index, selector) {
        	var option = $('<option></option>');
        	option.text(name).val(slug);
        	option.appendTo(selector);
    	});
		$('.buckets_select').val(slug);
		Page.change_bucket(slug, name, Contacts.widget_id);
		Contacts.select_cloned_bucket(slug, Contacts.contact_detail_id());
		return false;		// don't reload the widgets
    },

    bucket_add_and_categorize: function() {
    	// Another wrapper for Page.bucket_add specific to the Contacts widget,
    	//  for use only in the expanded view of the widget
    	//  Adds the new category to the select list and categorizes the selected contacts into the new category
    	Page.bucket_add(Contacts.bucket_add_and_categorize_callback);
    }, 

    bucket_add_and_categorize_callback: function(name, slug) {
    	// The callback for bucket_add_and_categorize
    	$('.buckets_select').each(function(index, selector) {
        	var option = $('<option></option>');
        	option.text(name).val(slug);
        	option.appendTo(selector);
    	});
        var option = $('<option></option>');
        option.text(name).val(slug);
        option.appendTo($('#box_contact_action_categorize'));
    	$('#box_contact_action_categorize').val(slug);
        Contacts.action('categorize');
    },
    
    change_bucket: function(selector) {
        var selected = selector.find("option:selected");
        if (selected.val() == 'new') {
            Contacts.bucket_add();
        } else {
    		Page.change_bucket(selected.val(), selected.text(), Contacts.widget_id);
        }
    },
    
    follow: function(contact_id) {
        Ajax.send_data('contacts_action/', {cmd: "follow", widget_id: Contacts.widget_id, contact_ids: contact_id}, function(data) {
			if (data.return_data.success) {
				Page.update_widgets();
			} else {
				alert(data.return_data.msg);
				//Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
			}
		}, "GET");        
    },

    show_plaxo: function() {
		showPlaxoABChooser('plaxo_callback_text', '/plaxo/callback/');
    },
    
    process_plaxo: function(d) {
    	Ajax.send_data('import_plaxo/', {data: JSONLib.stringify(d)}, function(data) {
    		if (data.return_data.success) {
    			Contacts.collapse();
    			$('.wizard').hide();
    			// Switch to the Plaxo bucket
        		Page.change_bucket(data.return_data.bucket_slug, data.return_data.bucket_name, Contacts.widget_id);
    		} else {
    			alert(data.return_data.msg);
    		}
    	}, "GET");
    },
    
    set_reminder: function(contact_id, text) {
		Ajax.send_data('contacts_action/', { cmd: 'set_reminder', widget_id: Contacts.widget_id, contact_ids: contact_id, date_to_parse: text }, function(data) {
			if (data.return_data.success) {
				Page.update_widgets();
			} else {
				alert(data.return_data.msg);
			}
		}, "GET");		
    }, 
    
    cancel_reminder: function(contact_id) {
		Ajax.send_data('contacts_action/', { cmd: 'cancel_reminder', widget_id: Contacts.widget_id, contact_ids: contact_id }, function(data) {
			if (data.return_data.success) {
				Page.update_widgets();
			} else {
				alert(data.return_data.msg);
			}
		}, "GET");		    
    },

    search: function() {
    	Page.search($("#widget_contacts_search_text").val(), "{{constants.SEARCH_TYPE_TEXT}}", Contacts.widget_id);
    },

    clear_search: function() {
    	Page.search("", "{{constants.SEARCH_TYPE_TEXT}}", Contacts.widget_id);
    },

    social_data_unwrap: function() {
    	$('.social_sites').unbind('mouseover').mouseover(function (e) {
    		if (!$(this).hasClass('social_sites_unwrap')) {
    			$(this).addClass('social_sites_unwrap');
    			$(this).removeClass('social_sites');
    		}
    	});
    	$('#contact_image').unbind('mouseover').mouseover(function (e) {
    		if ($('#contact_image').hasClass('has_image')) 
    			$('#contact_image_delete').show();
    	});
    },
    
	/* Call this each time the Contacts widget HTML is loaded and is ready
	 */
	ready: function() {
    	Contacts.social_data_unwrap();
    	
		if (Contacts.agent == 'iphone') {
			$('.box_contact_container').jScrollPane({showArrows:true, scrollbarWidth: 15, arrowSize: 16});
			if (Contacts.state() == 'expanded') {
				$('#back').show();
			} else {
				$('#back').hide();
			}
		}

		$('#add_secondary_email').keydown(function (e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode == 13) {
				Contacts.add_secondary_email();		
				return false;
			}
		});

		$("#widget_" + Contacts.widget_id + "_search_text").keydown(function (e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode == 13) {
				Contacts.search();				
			}
		});

		QuickBox.clear_note_only();	// leave bucket selection in place
 		if (Contacts.contact_detail_id() != '0') {
     		QuickBox.contact_id = Contacts.contact_detail_id(); 
 			QuickBox.contact_name= Contacts.contact_detail_display_name();
 		} else {
     		QuickBox.contact_id = Contacts.prev_contact_detail_id(); 
 			QuickBox.contact_name= Contacts.prev_contact_detail_display_name();
 		} 
		QuickBox.update_tooltip();
		
	},
	
	/*
	 * Look up a contact using various social services and fill in the HTML upon return
	 */
	get_social_data: function(contact_id) {
		// Use contact_detail_name, not contact_detail_display_name, for the name (display name is Last, First which is not what we want)
		Ajax.send_data('get_social_data/', { id: contact_id }, function(data) {
			if (data.return_data.success) {
				if (data.return_data.memberships.length > 0) {
					$('#contact_social').show();
					$('#contact_social').html(data.return_data.memberships_html);
				}
				$('#contact_location').text(data.return_data.location);
				if (data.return_data.location)
					$('#contact_social_location').show().find("img.action").show();				
				
				$('#contact_age').text(data.return_data.age);
				if (data.return_data.age)
					$('#contact_social_age').show().find("img.action").show();				
					
				$('#contact_gender').text(data.return_data.gender);
				if (data.return_data.gender)
					$('#contact_social_gender').show().find("img.action").show();				
				
				if (data.return_data.image_url) {
					$('#contact_image').attr('src', data.return_data.image_url).addClass('has_image');
				}
				//Page.update_widgets();
				
				Contacts.social_data_unwrap();
				
			} else {
				//alert(data.return_data.msg
                
			}
		}, "GET");				
	}
};

{% comment %}
// Code for displaying social data returned from various sites
var ContactsSocial = this.ContactsSocial = {
	
	display_memberships: function(data) {
		for (var i=0; i<data.length; i++) {
			var html = "<a target='_blank' href='#'><img border='0' src='http://twitter.com/favicon.ico'/></a>";
			
			// TODO: Fix up the html and append it to the right place in the contacts widget
			alert(data[i].site);
		}
	},
	
	test_memberships: function() {
		var m = [{"exists": "true", "site": "facebook.com", "profile_url": "http://www.facebook.com/schliker"}, {"exists": "true", "site": "linkedin.com", "profile_url": "http://www.linkedin.com/in/schliker", "num_friends": "266"}, {"num_followers": "15", "exists": "true", "site": "twitter.com", "profile_url": "http://twitter.com/dotcc", "num_followed": "0"}];
		ContactsSocial.display_memberships(m);
	}	
};
{% endcomment %}

// Function with the magic name required by plaxo
function onABCommComplete(d) {
	Contacts.process_plaxo(d);
};

{% if include_contacts_hook %}

var ContactsHook = this.ContactsHook = {

	timeout: null,
	
	contact_slugs: {},		// a map of contact email addresses to slugs, for caching

	assistant_addr: '{{alias.account.get_assistant.email}}',
	
	mail_selected: function(contacts, rfc822headers) {

		timeout = null;
		var unknown_addrs = [];

		{% comment %} Don't use Sender: for now [eg 3/19/10]
		// Find the Sender: in the rfc822 headers
		if (rfc822headers != null) {
			var sender_re = /^Sender:.*$/mi;
			var sender_match = rfc822headers.match(sender_re);
			if (sender_match) {
				sender_match = sender_match[0];
				sender_re = /<[^>]+>/;
				var sender_email = sender_match.match(sender_re);
				if (sender_email) {
					sender_email = sender_email[0];
					sender_email = sender_email.substr(1, sender_email.length - 2);
				} else {
					sender_email = sender_match.substr(7);
				}
				sender_email = Page.trim(sender_email).toLowerCase();
				contacts.splice(0, 1, {email: sender_email, name: ''});
			}
		}
		{% endcomment %}
		
		// Step 1: Go through address list, find any unknown addrs
		for (var i=0; i < contacts.length; i++) {
			var addr = Page.trim(contacts[i].email).toLowerCase();
			if (addr != '') {
				// Search through the contacts for that person
				var contact_slug = ContactsHook.contact_slugs[addr];	
				if (typeof(contact_slug) == 'undefined') {
					unknown_addrs.push(addr);
				} 
			}
		}
		
		// Step 2: Get any unknown addrs from server
		if (unknown_addrs.length > 0) {
			var unknown_addresses = unknown_addrs.join(',');

			// Ask the server about the addresses we don't know
			Ajax.send_data('get_contact_slugs_by_emails/', { addrs: unknown_addresses }, function(data) {
				if (data.return_data.success) {
					for (var i=0; i < data.return_data.contact_slugs.length; i++) {
						// Contact_slug can be a string, false, or null (see ajax_feeds.py)						
						var contact_slug = data.return_data.contact_slugs[i];
						if (contact_slug != null) {
							// If it's null (contact not created), don't add it to ContactsHook.contact_slugs so that we're forced to
							//  look it up again later when user clicks on another mail
							ContactsHook.contact_slugs[unknown_addrs[i]] = contact_slug;
						}
					}
					{% comment %} This needs to happen in the AJAX callback so it comes strictly after {% endcomment %}
					ContactsHook.select_first(contacts);
				} else {
					ContactsHook.select_by_contact_slug(null);
					return;					
				}
			}, "GET", "json", true);	
		} else {
			ContactsHook.select_first(contacts);
		}

	},

	select_first: function(contacts) {
		// Select the first address in the list addrs, that isn't a forbidden address (i.e., email is the user's own email or the assistant address)
		for (var i=0; i < contacts.length; i++) {
			var addr = Page.trim(contacts[i].email).toLowerCase();
			if (addr != '') {
				// Search through the contacts for that person
				var contact_slug = ContactsHook.contact_slugs[addr];	
				if (typeof(contact_slug) == 'string') {
					// found it!
					ContactsHook.select_by_contact_slug(contact_slug);
					return;	
				} else if ((typeof(contact_slug) == 'undefined') || (contact_slug == null)) {
					// unknown, uncreated contact -- offer to create it
					ContactsHook.select_new_populated(contacts[i]);	
					return;
				}
			}
		}

		// All the contact slugs are False (forbidden address), just collapse
		//   (i.e., message from the assistant to the user)
		ContactsHook.select_by_contact_slug(null);
		return;
	},

	// Expand the contact with the given id
	select_by_contact_slug: function(contact_slug) {
		if (typeof(contact_slug) == 'string') {
			if (! (Contacts.state() == 'expanded' && Contacts.contact_detail_slug() == contact_slug) ) {
				Contacts.expand(contact_slug, "readonly");
			}
		} else {
			if (Contacts.state() == 'expanded')
				Contacts.collapse();
		}
	},
	
	// Edit a new contact populated with the given email address
	select_new_populated: function(contact) {
		Contacts.expand(0, "new", contact.email, contact.name);
	}

};

// Called by the external code -- can't be in any namespace
// Accepts a string of this format:
// Joe Smith:addr1@blah.com;Sue Jones:addr2@blah.com;noname@blah.com;;From: headers...
// Enforces a 200ms delay
function outlook_mail_selected(params) {

	// Wrapper for mail_selected, for Outlook plugin
	// Accepts a string of this format:
	// Joe Smith:addr1@blah.com;Sue Jones:addr2@blah.com;noname@blah.com;;From: headers...
	// Enforces a 200ms delay
	
	// Parse the parameter string	
	params = params.split(';;');

	var email_addresses = params[0]
	var rfc822headers = (params.length > 1 ? params[1] : null);	// null for older versions of Outlook plugin
							
	var contact_strings = email_addresses.split(';');
	var contacts = []

	for (var i = 0; i < contact_strings.length; i++) {
		var name_addr = contact_strings[i].split(':');
		contacts.push({
			name:	(name_addr.length > 1 ? name_addr[0] : ''),
			email:	(name_addr.length > 1 ? name_addr[1] : name_addr[0])
		})			
	}
	
	if (ContactsHook.timeout != null) {
		clearTimeout(ContactsHook.timeout);
	}

	ContactsHook.timeout = setTimeout(function() {
			ContactsHook.mail_selected(contacts, rfc822headers);
		}, 200);
		
}

//Called by the external code -- can't be in any namespace
function mail_selected(contacts, rfc822_headers) {
	return ContactsHook.mail_selected(contacts, rfc822_headers);
}
	
{% endif %}


/*
 * Coming Up widget (scheduled events)
 */

var ComingUp = this.ComingUp = {
	widget_id: 'comingup',
	remove_from_event: function(uid) {
		Ajax.send_data('edit_event/', {widget_id: ComingUp.widget_id, cmd: 'remove_from_event', uid: uid}, function(data) {
	        if (data.return_data.success) {
	            $('.event_'+uid).hide();
	            Page.update_widgets();
	        } else {
	            alert("{% trans 'Could not process your request.' %}");
	        }
	    }, "GET");
	},
	// for event owners
	change_status_fading: function(uid) {
		Ajax.send_data('edit_event/', {widget_id: ComingUp.widget_id, cmd: 'change_status_fading', uid: uid, fading: 1}, function(data) {
	        if (data.return_data.success) {
	            $('.event_'+uid).fadeTo(1, .5);
	            Page.update_widgets();
	        } else {
	            alert("{% trans 'Could not process your request.' %}");
	        }
	    }, "GET");
	},
	readd_event: function(uid) {
		Ajax.send_data('edit_event/', {widget_id: ComingUp.widget_id, cmd: 'change_status_fading', uid: uid, fading: 0}, function(data) {
	        if (data.return_data.success) {
	            $('.event_'+uid).fadeTo(1, 1);
	            Page.update_widgets();
	        } else {
	            alert("{% trans 'Could not process your request.' %}");
	        }
	    }, "GET");
	},
	submit_quick_event: function() {
		// Don't require the @ symbol for events entered this way
		Ajax.submit_parse($("#quick_event").val(), null, QuickBox.bucket_slug, null, false, false, false);
	},

	ready: function() {
		$('.event_disable').each(function() {
			$(this).bind("click", function(e) {
				var uid = $(this).attr('id');
				uid = uid.replace('event_disable_','');
				if ($(this).hasClass('event_followup')) {
					ComingUp.remove_from_event(uid);		// just kill the follow-up reminder immediately
				} else if ($(this).hasClass('event_owner')) {
					if ($(this).is(':checked'))	{
						ComingUp.change_status_fading(uid);
					} else {
						ComingUp.readd_event(uid);
					}					
				} else {
					ComingUp.remove_from_event(uid);
				}
			});
		});

		$('.time_zone').click(function(e) {
			$('.wizard_time_zone').show();
		});

		$('#quick_event').keydown(function (e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode == 13) {
				ComingUp.submit_quick_event();
			}
		});
	}
}

/*
 * Conversations widget
 */

var Conversations = this.Conversations = {

	widget_id: 'conversations',
	
	set_filter: function(filter) {
		Ajax.send_data('set_conversations_filter/', {widget_id: Conversations.widget_id, filter: filter}, function(data) {
			if (data.return_data.success) {
				Page._update_widgets(data.return_data);
			} else {
				Ajax.failure_logout("{% trans 'Could not connect to server.' %}");
			}
		});
	},
	ready: function() {
		$('textarea.rbox').click(function(e) {
			$(this).css({color: '#000', height: '40px'});
			if ($(this).val() == '{% trans "Write a reply..." %}') {
				$(this).val('');
			}
			$(this).next().show();
		});
	},

	/*
     * Reply to a thread.
     *  id - db id of the last existing message in the thread
     *  button - the jQuery "reply" button
     */
	reply: function(reply_id, button) {
		var text = button.parent().parent().find(".rbox").val();
		button.attr('disabled', 'disabled');
		
        Ajax.send_data('parse/', {str: text, reply_id: reply_id, reply_to_all: false}, function(data) {
        	button.removeAttr('disabled'); 
            if (data.return_data.success) {
                Page.update_widgets();
            } else {
                alert(data.return_data.msg);
            }
        }, "GET");
		
	}
			
};
{% endminifyjs %}
