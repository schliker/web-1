{% load i18n %}
{% if signup %}{% if modify %}Subscription Modification{% else %}Subscription Start{% endif %}{% else %}Subscription Payment{% endif %}
 
{% trans 'Date' %}: {{date}}
{% if not signup %}{% trans 'Amount' %}: {{amount}}
{% endif %}{% if modify %}{% trans 'New service type' %}: {{account.service.name}}
{% trans 'Old service type' %}:	{{old_service.name}}{% else %}{% trans 'Service type' %}: {{account.service.name}}{% endif %}
{% trans 'Network name' %}: {{account.name}}
{% trans 'Account admin email' %}: {{alias.email}}
{% trans 'Payer email' %}: {{payer_email}}
