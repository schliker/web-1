#!/home/noted/domains/staging.calenvy.com/noted/bin/python
import os, sys, site

sys.stdout = sys.stderr

# thanks to http://74.125.155.132/search?q=cache:ZbpmQsuQ4yEJ:www.arlongpark.net/blog/deploying-django-mod-wsgi-virtualenv/+mod_wsgi+virtualenv&cd=3&hl=en&ct=clnk&gl=us

prev_sys_path = list(sys.path)
site.addsitedir('/home/noted/domains/staging.calenvy.com/noted/lib/python2.6/site-packages')
sys.path.append('/home/noted/domains/staging.calenvy.com/noted/web')
sys.path.append('/home/noted/domains/staging.calenvy.com/noted/web/lib')

# reorder sys.path so new directories from the addsitedir show up first
new_sys_path = [p for p in sys.path if p not in prev_sys_path]
for item in new_sys_path:
    sys.path.remove(item)
sys.path[:0] = new_sys_path

os.environ['DJANGO_SETTINGS_MODULE'] = "settings"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
