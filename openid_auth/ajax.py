from django.http import HttpResponse, HttpResponseNotFound
from django.template import Context, loader, Template
from remote.models import *
from models import *
from django.db.models import Q
from django.conf import settings
import pdb, traceback
import sys, os, htmlentitydefs, hashlib, json, re, sched, time, threading, random, urllib, copy, pdb
from utils import *
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.http import HttpRequest
import json, re
from openid.consumer import consumer
from openid import message
from openid.extensions import ax

def get_xrds_url(email):
    """For a given Google email address (gmail or Google Apps), get the (hard-coded) location of the XRDS document
    (the first step of OpenID auth, before opening the popup)"""
        
    if re.search(r'(?i)@(google|gmail)\.', email):
        return 'https://www.google.com/accounts/o8/id'      # XRDS for individual Gmail users
    else:
        domain = email.strip().split('@')[-1].lower()
        return 'https://www.google.com/accounts/o8/site-xrds?hd=' + domain
    

def get_endpoint(request):
    """
    First step in OpenID authentication: return a URL which we're redirecting the user to for a popup window"
    The second step is in views.py"""
    
    try:               
        # Shouldn't authenticate logged-in users
        auth.logout(request)  
        
        # Stateful OpenID consumer:
        #    request.session is where we store the per-user state (for an AnonymousUser who hasn't logged in yet)
        #    None - we'll establish a shared secret every time, instead of storing it in our DB (for now)
                
        c = consumer.Consumer(request.session, None)
        auth_request = c.begin(get_xrds_url(request.GET['email']))
                
        # Attribute Exchange parameters - get the email, etc. of the user
        fetch = ax.FetchRequest()
        fetch.add(ax.AttrInfo('http://axschema.org/contact/email', required=True))
        fetch.add(ax.AttrInfo('http://axschema.org/namePerson/first', required=True))
        fetch.add(ax.AttrInfo('http://axschema.org/namePerson/last', required=True))        
        auth_request.addExtension(fetch)
            
        site = Site.objects.get_current()
        domain = 'http%s://%s' % ('s' if site.domain in settings.SSL_DOMAINS else '', site.domain)
        return_url = domain + '/openid_auth/openid_return/'
        
        url = auth_request.redirectURL(domain, return_url)
    
        # Hard-code the UI extension parameters for this URL
        #  (yuck, but I couldn't figure out how to do it with the python-openid package)
        url += '&openid.ns.ui=' + urllib.quote_plus('http://specs.openid.net/extensions/ui/1.0')
        url += '&openid.ui.mode=' + urllib.quote_plus('popup')
                
        d = {
            'success':  True,
            'url':      url
        }
        
        return json_response(request, d)
    
    except:
 
        d = {
            'success':  False,
            'msg':  _('Could not authenticate your account with Google.')
        }
        
        log(traceback.format_exc())
        return json_response(request, d)
       

