# -*- coding:utf-8 -*-

def messages(request):
    """
    Request message could be accessed via this context proccessor. It has three fields -- notices, warning and errors (lists).
    """
    return { 'messages': request.messages }

