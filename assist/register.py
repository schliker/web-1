from django.db import models
from django.db import connection
from django.db.models import *
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from django.contrib import admin
from django.template import Context, loader
from django.utils.translation import ugettext as _
import hashlib, random, email
import traceback, pdb
import json, pprint, copy
from assist import mail
from utils import *
from datetime import *
from models import *
from constants import *
from remote.models import *
from twitter.models import *

                             
def generate_base_url(email, require_unique=True):
    """
    Generate a base_url based on the given string.
    TODO: Deal with possible race condition here
    """
    
    base_url = email.split('@')[-1].split('.')[0].lower()
    base_url = strip_nonalnumunderscore(base_url)
    
    if is_common_base_url(base_url):
        # If base url is 'common' (gmail, hotmail etc.), then
        # generate a unique base url using a slug
        while True:
            result = base_url + '_' + create_slug(length=6)
            if Account.objects.filter(base_url=result).count() == 0:
                return result, None
                
    queryset = Account.objects.filter(base_url=base_url)
    if queryset.count() == 0:
        return base_url, None
    elif require_unique is False and (base_url != settings.SALES_ACCOUNT) and (base_url not in settings.DISALLOW_JOIN_ACCOUNT):
        return base_url, queryset[0]    # Let the new user join an existing account, but only if that account is a team account
    i = 1
    while True:
        result = base_url+str(i)
        queryset = Account.objects.filter(base_url=result) 
        if queryset.count() == 0:
            return result, None
        i += 1
        

def generate_assistant_email_address(first_name, last_name, domain):
    if first_name:  first_name = strip_nonascii(first_name)
    if last_name:   last_name = strip_nonascii(last_name)
    
    if first_name and not last_name:
        addr = (first_name.lower().strip()).replace(' ','')+'@'+domain
    elif first_name and last_name:
        addr = (first_name.lower().strip()+'.'+last_name.lower().strip()).replace(' ','')+'@'+domain
    elif not first_name and last_name:
        addr = (last_name.lower().strip()).replace(' ','')+'@'+domain 
    else:
        addr = None
    if not is_special_address(addr):
        return addr
    else:
        return None
    
def generate_random_assistant_email_address(domain):
    while True:
        assistant_first_name = random.choice(get_wordlist('names_female')).lower()
        while not (3 <= len(assistant_first_name) <= 8):
            assistant_first_name = random.choice(get_wordlist('names_female')).lower()
        assistant_last_name = random.choice(get_wordlist('surnames')[:250]).lower()

        assistant_email = generate_assistant_email_address(assistant_first_name, assistant_last_name, domain)
        if not assistant_email:
            raise NotifyUserException(_('Invalid assistant name.'))  
        
        if Assistant.objects.filter(email=assistant_email).count() == 0:
            break          
    
    return assistant_email


def format_account_directory_for_display(alias):
    account = alias.account
    aliases_admin = list(Alias.objects.active().filter(account=account).filter(is_admin=True))
    aliases_admin.sort(key=lambda x: x.ez_name)
    aliases_nonadmin = list(Alias.objects.active().filter(account=account).exclude(is_admin=True))
    aliases_nonadmin.sort(key=lambda x: x.ez_name)
    return aliases_admin + aliases_nonadmin


def get_or_create_account_user_for_alias(alias, footer='', timezone=None):
    '''Create an AccountUser and a Calendar for an Alias.
    Assumes the Alias doesn't already have an AccountUser!
    Does not actually point the Alias to the resulting AccountUser; the caller must do that upon return'''
    
    if alias.account_user:
        return alias.account_user, False
                
    old_aliases = Alias.objects.filter(email=alias.email).exclude(account_user=None)
    if old_aliases.count() > 0:
        created = False
        account_user = old_aliases[0].account_user
    else:
        created = True
        account_user = AccountUser()
        account_user.slug = create_slug()
        if timezone:
            account_user.timezone = timezone
        else:
            account_user.timezone = settings.TIME_ZONE
        account_user.save()
        log('saving account_user')
        
        account_user.save()
    
    return account_user, created
        
def get_or_create_contact_for_alias(alias, first_name, last_name): #, footer='', timezone=None):
    '''Create a Contact for an Alias, and point it at that Alias.'''
            
    contact, created = Contact.objects.get_or_create_smart(\
        account = alias.account,
        alias = alias,
        email = alias.email,
        first_name = first_name,
        last_name = last_name,
        source = Contact.SOURCE_ALIAS,
        status = Contact.STATUS_ACTIVE
    )

    alias.contact = contact
    alias.save()
    
    return contact, created

def activate_alias(alias, account, first_name, last_name):
    '''Take an existing Alias object, and activate it with a specific account,
    marking it as active. Also create the special Contact for this alias.'''
    
    user = User.objects.create_user(create_slug(length=10), alias.email, hashlib.md5(str(random.getrandbits(200))).hexdigest())
    # User object uses '' for no first or last name, we store None instead
    user.first_name = str_or_none(first_name)
    user.last_name = str_or_none(last_name)
    user.save()
     
    alias.account = account
    alias.status = ALIAS_STATUS_ACTIVE
    alias.last_login_date = now()
    alias.user = user
    alias.save()
    
    contact, created = get_or_create_contact_for_alias(alias, first_name, last_name)
    
    # This requires Contact so it can get first_name and last_name
    alias.ez_name = alias.calc_ez_name(Alias.objects.active().filter(account=account))
    alias.save()
    
    return contact
    
def delete_alias(alias, admin_alias):
    """Delete an alias from an account.
    Sets the account to STATUS_ACTIVE if it now has <= the number of users paid for, and returns True in this case."""
    
    log("register.delete_alias: marking alias %s: %s as inactive" % (str(alias), alias.slug))
    
    account = alias.account
    
    # Contacts that were owned by that person are now owned by an admin
    for c in Contact.objects.filter(owner_alias=alias):
        c.owner_alias = admin_alias
        c.save()
        
    contact = alias.contact
    contact.alias = None
    contact.save()

    alias.delete()    
    contact.delete()
    
    if (account.max_users is None) or ((account.status == ACCOUNT_STATUS_INACTIVE_MAX_USERS_EXCEEDED) and (account.num_active_aliases() <= account.max_users)):
        account.status = ACCOUNT_STATUS_ACTIVE
        account.save()
        return True
    else:
        return False
    
    
    
def activate_pending_alias(request,alias,activate_email):
    try:        
        viewing_account_user = alias.account_user
        account = alias.account
        
        success, already_active, old_status, new_alias = False, False, None, None
        query = Alias.objects.filter(email=activate_email, account=account)
        if query.count() == 1:
            new_alias = query.get()
            old_status = new_alias.status
            if old_status == ALIAS_STATUS_AWAITING_CONFIRMATION:
                activate_alias(new_alias, account)      # this also creates the special Contact
                success = True
            elif old_status == ALIAS_STATUS_ACTIVE:
                already_active = True
        else:
            new_alias = None

        if success:  
            # Send activation email
            assistant = new_alias.account.get_assistant()
            
            c = Context({
                'assistant_domain':     assistant.get_domain(),
                'assistant_email':      assistant.email,
                'assistant':            assistant,
                'site':                 get_site(request),
                'alias':                new_alias,
                'account_user':         viewing_account_user,
                'viewing_account_user': viewing_account_user,
                'account':              alias.account,
                'admin_alias':          alias,
                'team_aliases':         format_account_directory_for_display(alias)
            })               
                      
            assistant = alias.account.get_assistant()
            to = [(new_alias.get_full_name(), new_alias.email)]
            msg = mail.send_email('main_signup_company_approved.html', 
                c, 
                _('[Activated] you have been added to '+new_alias.account.base_url), 
                (assistant.get_full_name(), assistant.email), 
                to, reply_to=None, 
                request=request,
                extra_headers=None, 
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_CONFIRM_USER,
                    'owner_account': assistant and assistant.owner_account
                })
            
            to = [(alias.get_full_name(), alias.email)]
            msg = mail.send_email('main_signup_company_active_admin_notify.html', {
                'site': get_site(request),
                'activate_alias': new_alias,
                'admin_alias': alias,
                }, _('[Status] '+new_alias.email+' just added to '+alias.account.base_url), 
                assistant.email, to, reply_to=None, 
                request=request,
                extra_headers=None, 
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_ADMIN_NOTIFY,
                    'owner_account': assistant and assistant.owner_account
                })
            
        return old_status, new_alias, already_active, success
    except:
        return old_status, new_alias, already_active, success


def add_new_aliases(emails, account, timezone=settings.TIME_ZONE, mode=None, admin_alias=None, invite=True):    
    # Create aliases to be added to an existing account.
          
    assistant = account.get_assistant()
    new_aliases = []
    
    for email in emails: 
        email = email.lower().strip()
        if is_valid_email_address(email):
            new_alias, created = Alias.objects.get_or_create(email=email)
            
            if created:
                new_alias.account = account
                new_alias.is_admin = False
                new_alias.slug = create_slug(length=20)
                new_alias.slug_ics = create_slug(length=20)
            else:
                if new_alias.account != account:
                    raise NotifyUserException(_('User %s already exists.') % email)
                
            changed = False
            if new_alias.status != ALIAS_STATUS_ACTIVE:
                new_alias.status = ALIAS_STATUS_ACTIVE
                changed = True
            
            if created or changed:
                new_alias.account_user, created = get_or_create_account_user_for_alias(new_alias, timezone=timezone)
                new_alias.save()
                activate_alias(new_alias, account, '', '')  # don't know first and last name, leave them blank for now
                new_aliases.append(new_alias)
                send_welcome_email(new_alias, assistant, account.site, is_invite=invite, admin_alias=admin_alias)
                
                from network.ajax_admin import admin_ping, admin_followup
                t = loader.get_template('newsfeed_signup.txt')           
                c = Context({\
                    'alias':                    new_alias,
                    'account':                  new_alias.account,
                    'date':                     now().strftime('%m/%d/%Y'),
                    'invite':                   invite,               
                }) 
                content = t.render(c) 
                categories = ['customers']
                admin_ping(new_alias.contact, content, categories=categories)

            else:
                raise NotifyUserException(_('User %s already exists.') % email)
        else:
            raise NotifyUserException(_('Please enter a valid email address.'))
        
    if invite and mode and new_aliases:
        message = _("Sent invitation emails to %s") % (', '.join([a.email for a in new_aliases]))
        if account.name:
            message += ' ' + _('for the %s network') % account.name
        mode['wmessages'].append(message)
        mode['force_refresh_all'] = True     # tell Contact widget to refresh itself   
        
    return new_aliases 
        
def send_welcome_email(alias, assistant, site, is_invite=False, admin_alias=None):
    
    # Send welcome email
    c = Context({
        'assistant':            assistant,
        'assistant_domain':     alias.account.get_full_domain(),
        'assistant_email':      assistant.email,
        'assistant':            assistant,
        'site':                 site,
        'has_ssl':              site.domain.lower() in settings.SSL_DOMAINS,
        'admin_alias':          admin_alias,
        'alias':                alias,
        'account_user':         alias.account_user,
        'account':              alias.account,
        'team_aliases':         format_account_directory_for_display(alias)
    })
    
    # Welcome email #1: hello from assistant
    if is_invite:
        html_file = 'main_invite_alias.html'
        if alias.account.name:
            subject = _("[Invited] You have been added to %s.") % (alias.account.name)
        else:
            subject = _("[Invited] Hi! I'm %s, your new assistant.") % assistant.first_name
    else:
        subject = _("Hi! I'm %s, your new assistant.") % assistant.first_name
        html_file = 'main_signup_company.html'
    mail.send_email(html_file, c, subject,
        (assistant.get_full_name(), assistant.email),
        (alias.get_full_name(), alias.email),
        noted={'message_type': EMAIL_TYPE_NOTED,\
            'message_subtype': EMAIL_SUBTYPE_NOTED_REGISTER,
            'owner_account': assistant and assistant.owner_account
        },
        alias=admin_alias if is_invite else None)
    
    # Welcome email #2: Personal welcome    
#    txt_file = 'intro_email.txt'
#    subject = _("Getting started with %s") % (site.name)
#    
#    intro_msg = mail.send_email(txt_file, c, subject,
#        (settings.INTRO_NAME, "%s@%s" % (settings.INTRO_EMAIL_PREFIX, site.domain.replace(':3636', ''))),
#        [(alias.get_full_name(), alias.email), (assistant.get_full_name(), assistant.email)],
#        content_type='text/plain', allow_all_domains=True           # need allow_all_domains because we're sending to ourselves
#    )
   
    

#def send_admin_confirmation_email(alias, assistant, site):
#    account = alias.account
#    try:
#        admin = Alias.objects.active().filter(account=account, is_admin=True).all()[0]
#    except:
#        raise Exception('There is a missing admin in the account! This is terrible.')
#
#    c = Context({
#        'assistant_domain':     alias.account.get_full_domain(),
#        'assistant_email':      assistant.email,
#        'assistant':            assistant,
#        'site': site,
#        'alias': alias,
#        'account_user':         alias.account_user,
#        'account':              alias.account,
#        'admin':                admin
#    })
#   
#    html_file = 'main_signup_company_admin_confirm.html'
#    mail.send_email(html_file, c, _("[User Confirm] Please confirm:") + ' ' + alias.get_full_name_or_email(),
#        (assistant.get_full_name(), assistant.email),
#        (admin.get_full_name(), admin.email),
#        noted={'message_type': EMAIL_TYPE_NOTED,\
#            'message_subtype': EMAIL_SUBTYPE_NOTED_CONFIRM_USER,
#            'owner_account': assistant and assistant.owner_account
#        })
#    
#def send_alias_pending_email(alias, assistant, site):
#    log("send_alias_pending_email: This function shouldn't be called! Feature is disabled. alias = ", alias, "assistant =", assistant)
#    
#    account = alias.account
#    try:
#        admins = Alias.objects.active().filter(account=account, is_admin=True).all()
#    except:
#        raise Exception('There is a missing admin in the account! This is terrible.')
#
#    for admin in admins:
#        c = Context({
#            'assistant_domain':     alias.account.get_full_domain(),
#            'assistant_email':      assistant.email,
#            'assistant':            assistant,
#            'site': site,
#            'alias': alias,
#            'account_user':         alias.account_user,
#            'account':              alias.account,
#            'admin':                admin
#        })
#    
#        html_file = 'main_signup_company_pending_confirm.html'
#        mail.send_email(html_file, c, _("[Pending Activation] Please wait for approval at: "+alias.account.base_url),
#            (alias.account.get_assistant().get_full_name(), alias.account.get_assistant().email),
#            (alias.get_full_name(), alias.email),
#            noted={'message_type': EMAIL_TYPE_NOTED,\
#                'message_subtype': EMAIL_SUBTYPE_NOTED_PENDING_USER})
                # TODO: owner_account
#                            
#    
     
def _register(email, service, promo, assistant_email, base_url, user_first_name='', user_last_name=''):
    """Create a new account and make addr the first alias.
    base_url and assistant_email must be unique"""
    
    try:
        log("_register: ", email, service, promo, assistant_email, base_url)
            
        site = Site.objects.get_current()
                
        alias = Alias(email=email)
        alias.slug = create_slug(length=20)
        alias.slug_ics = create_slug(length=20)
        
        # email addresses must be unique these days, but keeping this old code for now 
        other_aliases = Alias.objects.filter(email=email)
        if other_aliases.count() > 0:
            alias.account_user = other_aliases[0].account_user
            account_user = alias.account_user
        else:
            account_user, account_user_created = get_or_create_account_user_for_alias(alias, timezone=settings.TIME_ZONE)
            log('saved account_user')
        
        account, created = Account(), True
        account.status = ACCOUNT_STATUS_ACTIVE
        account.base_url = base_url
        account.slug = create_slug()
        account.transparent = False                 # emails are private by default
                         
        # Create Account if necessary
        if created:  
            log('creating account')          
            alias.is_admin = True
            account.site = site
            account.name = None                     # changed behavior: new accounts don't have a name (they can pay for one later) [eg 11/11]
            admin_domain_parts = alias.email.split('@')[-1].split('.')
            if admin_domain_parts:
                admin_domain = admin_domain_parts[-2]
                account.public_domains = json.dumps([admin_domain])
            account.start_service(service, promo)
            account.save()
            assistant = Assistant.objects.create_assistant(assistant_email, account)            
        else:
            alias.is_admin = False
            assistant = account.get_assistant()
                        
        # Update the alias with the Account and the AccountUser
        log('creating alias')
        alias.account_user = account_user
        alias.account = account
        alias.save()
        
        log('creating contact')
        contact = activate_alias(alias, account, user_first_name, user_last_name) 
        
        if created:
            # Populate the initial bucket names
            for bucket_name in AUTO_BUCKET_NAMES:
                Bucket.objects.get_or_create_for_alias(alias, bucket_name)
            if account.service.allow_sf:
                #Bucket.objects.get_or_create_for_alias(alias, SF_BUCKET_NAME)
                log('not creating tag for sfdc')
                pass
                
            # Populate the initial Salesforce rules
            EmailRule.objects.create_initial_for_account(account)
                        
            # Populate our friendly sales person into the new account
            if settings.SALES_AUTO_CONTACT and account.alias.filter(email=settings.SALES_AUTO_CONTACT['email']).count() == 0:
                auto_contact = copy.copy(settings.SALES_AUTO_CONTACT)
                auto_contact.update({
                    'account': account,
                    'viewing_alias': alias,
                    'status': Contact.STATUS_ACTIVE
                })    
                Contact.objects.get_or_create_smart(**auto_contact)
         
        send_welcome_email(alias, assistant, site)
                            
        log('register._register DONE')
        return alias
    
    except:
        log('register._register FAIL: ',traceback.format_exc())
        
        c = Context({
            'full_name': email
        })
        mail.send_email('main_signup_fail.html', c, _('Failure with registration'),
            (settings.NOREPLY_EMAIL),
            email,
            noted={'message_type': EMAIL_TYPE_NOTED,\
                'message_subtype': EMAIL_SUBTYPE_NOTED_REGISTER_FAIL})
    
        return None
    

def register(request, data):
    """
    Helper function for sending registration --
    used by the AJAX call and by the API call.
    
    Provides extra functionality above and beyond the bare-bones
    _register function which it calls, like:
     * validation (throwing NotifyUserException errors if needed)
     * generating the assistant name/email address and the base_url
     * creating the initial RemoteUser if they registered via Salesforce (etc.), 
     * doing the admin ping.
    
    Data: a dictionary containing these keys:
        email
        service_name
        agent                        (optional)
        first_name                   (optional)
        last_name                    (optional)
        terms_agree                  (optional - if omitted, defaults to True)
                                       can be boolean, or '0' / '1'
        promo                        (optional)
        password                     (optional)
        remote_usertype              (optional)
        remote_userid                (optional)
        remote_organizationid        (optional)
        cred_type                    (optional)
        version                      (optional)
    """
    
    try:                             
        log("register.register START:")
        #team_name = data['team_url'].strip().lower()
        team_name = None                                # All new team names are None, for now [eg 11/11]
        email = data['email'].strip().lower()
        first_name = data.get('first_name', '').strip()
        last_name = data.get('last_name', '').strip()
        terms_agree = data.get('terms_agree', True)
        terms_agree = terms_agree and terms_agree != '0'
        agent = data.get('agent')
        version = data.get('version')
        
        log("team_name:", team_name)
        log("email:", email)
        log("agent:", agent)
        log("version:", version)            # version of the agent, if any
        
        alias = request.session.get('alias', None)
        
        if alias:
            log("register.register: service BEFORE = ", alias.account.service)
                    
        if not terms_agree:
            raise NotifyUserException(_('Please agree to the terms of service.'))
            
        if (alias is None) and Alias.objects.filter(email=email).count() > 0:
            raise NotifyUserException(_('Email address %s already registered.') % email)
             
        if team_name and Account.objects.filter(name=team_name).count() > 0:
            raise NotifyUserException(_('Team name %s already in use.') % team_name)
        
        if ('password' in data) and not Alias.objects.is_strong_password(data['password']):
            raise NotifyUserException(html=_('Your password must be at least %d characters long, and contain a letter and a digit.') % MIN_PASSWORD_LENGTH)

        m = RE_EMAIL.match(email)
        if not m:
            raise NotifyUserException(_('Invalid email address.'))
        
        domain = get_site(request).domain.split(':')[0]
        if team_name:
            team_name = strip_nonalnum(team_name)
            full_domain = team_name + '.' + domain
        else:
            team_name = ''
            full_domain = domain
                
        # Change require_unique to True to disable the join-existing-account feature
        base_url, account = generate_base_url(email, require_unique=False)
        
        if account is None:
            # we'll create a new account
            log("register.register: [1]", base_url)

            if 'service_name' not in data:
                d = {
                    'success':  False,
                    'full_registration_required':   True
                }
                return d
                
            service_name = data['service_name']
            service = Service.objects.get(name__iexact=service_name)
                             
            if data.get('promo'):            
                try:
                    promo = Promo.objects.get(code__iexact=data['promo'])
                except:
                    try:
                        promo = Promo.objects.get(name__iexact=data['promo'])
                    except:
                        promo = None
            else:
                promo = None
            
            assistant_email = generate_random_assistant_email_address(full_domain)
            log("assistant_email:", assistant_email)            
            alias = _register(email, service, promo, assistant_email, base_url, user_first_name=first_name, user_last_name=last_name)
            joined_existing = False
            
        else:
            log("register.register: [2]", account.pk)

            # Join the existing account
            assistant_email = account.get_assistant().email
            new_aliases = add_new_aliases([email], account, timezone=account.get_default_admin().account_user.timezone, invite=False)
            alias = new_aliases and new_aliases[0] or None
            joined_existing = True
            
        if not alias:
            raise NotifyUserException(_('Could not process your registration. Please contact founders@%s and let us know.') % get_site(request).domain)

        log("register.register [3]", email)

        if alias.is_admin:
            tu = request.session.get('tolink_twitteruser', None)
            if tu:
                # alias.beta_terms_date = now()
                TwitterUser.objects.link_account_to_twitteruser(request, alias.account, tu)

        # If they registered over the web site, they've already accepted the terms. [eg 11/22]
        
        if terms_agree:
            alias.beta_terms_date = now()
            
        alias.agent_created = agent or ''
        if 'password' in data:                      # Optional: setting the password as part of the reg process
            alias.set_password(data['password'])    #  (usually it's left blank for now)
        alias.save()
        
        log("register.register [4]", email)

        # Create a login token (optional)
        # Used by the streamlined registration wizard for Firefox, Outlook and SF    
        token = None    
                    
        if 'remote_usertype' in data:
            remote_user, created = RemoteUser.objects.get_or_create(type=data['remote_usertype'], remote_userid=data['remote_userid'], alias=alias, account=alias.account)
            remote_user.remote_organizationid = data.get('remote_organizationid')
            remote_user.enabled = True
            remote_user.valid = True
            remote_user.version = version
            remote_user.save()
            
            if 'cred_type' in data:
                token, created = RemoteCredential.objects.get_or_create_for_remote_user(alias, remote_user, data['cred_type'])
                log("register.register -- created token of type ", data['cred_type'], " starting with", token.token[:6])
              
        log("register.register [5]", email)
          
        try:  
            from network.ajax_admin import admin_ping, admin_followup
            t = loader.get_template('newsfeed_signup.txt')           
            c = Context({\
                'alias':                    alias,
                'account':                  alias.account,
                'date':                     now().strftime('%m/%d/%Y'),
                'agent':                    agent,
                'session_referer':          request.session.get('referer', '')
            }) 
            content = t.render(c) 
            categories = ['customers']
            if agent in ['salesforce', 'firefox', 'iphone', 'outlook', 'desktop']:
                categories.append(agent)
              
            log("register.register [6]", email)
            admin_ping(alias.contact, content, categories=categories)
            
            log("register.register [7]", email)
            #admin_followup(alias.contact, settings.FOLLOWUP_CAMPAIGN_ANYQUESTIONS, source="Calenvy registration", cc=assistant_email)
        except:
            log("register.register: couldn't create admin ping for alias", alias, traceback.format_exc())
                

        if joined_existing:
            msg = _('Registration successful! Check your email at %s to activate your account.') % email
        else:
            msg = _('%s has been registered!') % email

        d = {
            'success': True,
            'assistant_email': assistant_email,
            'msg': msg,
            'base_url': team_name,
            'joined_existing': joined_existing
        }
                
        log("register.register [8]", email)

        # For existing users, don't log them in immediately -- require them to click on the link in their email, for security
        if joined_existing:
            d['redirect_url'] = 'http://%s%s?email=%s' % (get_site(request).domain, '/dashboard/', urllib.quote_plus(email))
            if agent:
                d['redirect_url'] += '&agent=%s' % agent
        else:
            # Actually log the user in
            request.session['alias'] = alias
            user = auth.authenticate(user=alias.user)        # dummy authentication, just fill in the 'backend' attribute
            auth.login(request, user)
            
            d['token'] = token and token.token
        
        if alias:
            log("register.register: service AFTER = ", alias.account.service)
        
        log("register.register [9]", email)

        #request.messages['notices'].append(msg)
        
        return d

    except NotifyUserException, e:
        log('__register__: error [1]',traceback.format_exc())

        d = {
            'success': False,
            'msg': e.message
        }
        return d

    except Exception, e:
        
        log('__register__: error [2]',traceback.format_exc())
        msg = _('Could not process your registration. Please contact founders@%s and let us know.') % get_site(request).domain
        # request.messages['notices'].append(msg)
        
        # Send a mail to the admins -- this is bad news
        content = 'register error [2]:\n\n'
        content += pprint.pformat(data.items())
        content += '\n\n'
        content += traceback.format_exc()
        mail.mail_admins('Registration failed', content)
        
        d = {
            'success': False,
            'msg': msg
        }
        return d   
        
        
        

