from django.core.management.base import NoArgsCommand, BaseCommand 
from django.core.management.color import no_style
from django.utils.datastructures import SortedDict
from optparse import make_option
from south import migration
from django.core.management.commands import syncdb
from django.conf import settings
from django.db import models
from django.db.models.loading import cache
from django.core import management
from assist.models import *
import sys, pdb

from optparse import make_option

class Command(BaseCommand):
    help = 'Delete an account and all its data.'
    args = '[base_url]'

    def handle(self, *app_labels, **options):
        
        print
        
        try:
            base_url = app_labels[0]
        except:
            print "Specify a base_url."
            return
                
        try:
            acc = Account.objects.get(base_url=base_url)
        except:
            print "No account with base_url ", base_url
            return
        
        print "Delete account ", base_url, " with these users:"
        for a in acc.alias.all():
            print a
            
        print "and this many emails: ", Email.objects.filter(owner_account=acc).count(), "? (yes/no)",
                
        if raw_input().lower().strip() == 'yes':
            print "Deleting account..."
            acc.delete()
            print "...done!"
        else:
            print "Not deleting account (must type 'yes')."
                
        
