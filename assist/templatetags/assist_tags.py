from django import template
from django.utils.translation import ugettext as _
from utils import *
from assist import mail
from assist.models import *
import copy

register = template.Library()

@register.filter
def replace_relative_links(html):
    return html # TODO
    
@register.filter
def tagname(person, viewing_alias):
    """Return the EZ_name of the alias, but only if it's in the same account as viewing_alias.
    Otherwise return the name or the email address."""
    
    if type(person) is Alias:
        return person.get_tag_or_name_for_display(viewing_alias)
    elif type(person) is Contact:
        return person.get_display_name()
    else:
        return ''
    
@register.filter
def tagname_with_email(person, viewing_alias):
    """Return the EZ_name of the alias, but only if it's in the same account as viewing_alias.
    Otherwise return the name or the email address."""
    
    if type(person) is Alias:
        return person.get_tag_or_name_for_display(viewing_alias, include_email=True)
    elif type(person) is Contact:
        return person.get_display_name(include_email=True)
    else:
        return ''
@register.filter
def color_from_time(time_string):
    if time_string == "default":
        return "red"
    elif time_string == "allday":
        return "#2B2825"
    elif time_string == "soon":
        return "#CCC"
    elif time_string == "far":
        return "#EEE"
    else:
        return ""


@register.filter
def days_remain(dt):
    remain = dt - now()
    if remain < timedelta(0):
        remain = timedelta(0)
    days = remain.days + 1
    if days == 1:
        return _("%d day") % days
    else:
        return _("%d days") % days

    
def quote_email(context):
    context_new = copy.copy(context)
    email = context['e']
    old_emails = context['old_emails']
    is_latest = (email == old_emails[-1])
    is_reply = (email != old_emails[0])
    display_email = mail.format_email_for_display(context['e'], tzinfo=context.get('tzinfo', None), include_filelinks=False, contact=context.get('contact', None)) 
    context_new.update({
        'email':            display_email,
        'is_latest':        is_latest,
        'is_reply':         is_reply
    })
    return context_new

# Register the custom tag as an inclusion tag with takes_context=True.
register.inclusion_tag('emails/quoted_email.html', takes_context=True)(quote_email)


