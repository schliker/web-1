import re, copy
from utils import *
from datetime import *
import traceback, pdb
from handlers import *
from django.conf import settings

# Noted's extension to ParseDate: finds the "best" span anywhere in a bunch of text,
# combines dates / times that are separated, etc.

class ParseDateExtended(ParseDate):

       
    def calc_token_heatmap(self, tokens):
        length = len(tokens)
        for i in xrange(length):
            tokens[i].heat = 0
            for j in xrange(length):
                if tokens[j].tags:
                    tokens[i].heat += (len(tokens[j].tags)**0.3)*(0.6**(abs(j-i)))
        return tokens
        
    def find_heatmap_peaks(self, tokens):
            
        length = len(tokens)
        if length == 0:
            pass
        elif length == 1:
            tokens[0].peak_type = CHRONIC_PEAK_REGULAR
        else:
            for i in xrange(len(tokens)):
                tokens[i].peak_type = CHRONIC_PEAK_NONE
                
                if tokens[i].get_tag(SeparatorSlashOrDash) or tokens[i].get_tag(OrdinalDay) or tokens[i].get_tag(RepeaterTime):
                    tokens[i].peak_type = CHRONIC_PEAK_OTHER
                    
                if (i == 0 and tokens[i].heat >= tokens[i+1].heat):
                    tokens[i].peak_type = CHRONIC_PEAK_REGULAR
                elif (i == len(tokens)-1 and tokens[i].heat >= tokens[i-1].heat):
                    tokens[i].peak_type = CHRONIC_PEAK_REGULAR
                elif ((i>0 and i<len(tokens)-1) and tokens[i].heat >= tokens[i-1].heat and tokens[i].heat >= tokens[i+1].heat):
                    tokens[i].peak_type = CHRONIC_PEAK_REGULAR
                        
                if (len(tokens[i].orig_word) >= 2) and tokens[i].orig_word[0] == '@' and tokens[i].tags:
                    tokens[i].peak_type = CHRONIC_PEAK_TAGGED
                elif (tokens[i].orig_word in ['@', '*@*', '*@', '@*']):          # @ all by itself indicates a date word
                    tokens[i].peak_type = CHRONIC_PEAK_TAGGED
             
        return tokens
            
    def span_around_peak(self, tokens, peak_index):
        if settings.DEBUG_CHRONIC: log("spans_around_peak: peak_index = %d" % peak_index)
        best_span = None
        best_phrase_length = None
        best_num_tagged_tokens = None
        span_peak_type = tokens[peak_index].peak_type
        
        for left in range(max(0, peak_index-4), peak_index+1+1):  
            for right in range(peak_index-1, min(peak_index+4, len(tokens)-1)+1): 
                if right < left:
                    continue
                if not (tokens[left].tags and tokens[right].tags):
                    continue
                if tokens[peak_index].tags:
                    # and not all([isinstance(tag, Separator) for tag in tokens[peak_index].tags]):
                    if (peak_index < left or peak_index > right):
                        continue
                subphrase = []
                for t in tokens[left:right+1]:
                    t = copy.copy(t)
                    t.tags = [tag for tag in t.tags if tag.get_within_min_peak_type() <= span_peak_type]
                    subphrase.append(t)
                self.reset_tokens(subphrase)
                if settings.DEBUG_CHRONIC: log("[%d - %d]" % (left, right), subphrase)
                phrase_length = right-left+1
                num_tagged_tokens = sum([int(bool(token.tags)) for token in subphrase]) 
                span = self.tokens_to_span(subphrase, remove_untagged=True)
                # log(" ... tried %d to %d, phrase=%s, got %s" % (peak_index-left, peak_index+right, str(subphrase), str(span)))
                if span:
                    if settings.DEBUG_CHRONIC: log("-- found a span:", span)
                    if best_span:
                        if span.strong and (not best_span.strong):
                            best_span = span
                            best_phrase_length = phrase_length
                            best_num_tagged_tokens = num_tagged_tokens
                            if settings.DEBUG_CHRONIC: log("-- replaced best span!")
                        elif (not span.strong) and best_span.strong:
                            continue
                        if span.width() < best_span.width() * 3 / 4:        # New span has to be sufficiently narrower than old one
                            best_span = span
                            best_phrase_length = phrase_length
                            best_num_tagged_tokens = num_tagged_tokens
                            if settings.DEBUG_CHRONIC: log("-- replaced best span!")
                        elif span.width() == best_span.width() and num_tagged_tokens > best_num_tagged_tokens:
                            best_span = span
                            best_phrase_length = phrase_length
                            best_num_tagged_tokens = num_tagged_tokens
                            if settings.DEBUG_CHRONIC: log("-- replaced best span!")          
                    else:
                        best_span = span
                        best_phrase_length = phrase_length
                        best_num_tagged_tokens = num_tagged_tokens
                        if settings.DEBUG_CHRONIC: log("-- first best span")
                        
        if settings.DEBUG_CHRONIC: log( " ... returning %s" % best_span)
        
        if best_span:
            best_span.peak_index = peak_index
            best_span.peak_type = span_peak_type
        
        return best_span
        
    def combine_spans(self, spans, min_peak_type):    
     
        # Combine neighboring spans which represent a date and a time.
        # This lets us handle stuff like "Meeting Thursday in varsity at 5pm"
        
        # spans is a list of tuples (i, peak_type, span) where:
        #   i is the position of some token in that span (we use this later for word distance stuff)
        #   peak_type indicates the priority of the span
        #   span is the actual span found there
        # min_peak_type means we only return spans where at least one element of the span is of this
        #   peak_type or higher
            
        unique_spans = []
        for span in spans:
            if not unique_spans:
                unique_spans.append(span)
            else:
                if span == unique_spans[-1]:
                    if span.peak_type > unique_spans[-1].peak_type:
                        unique_spans[-1] = span
                else:
                    unique_spans.append(span)
        
        combined_spans = []
            
        for span in unique_spans:
            combined = False
            if combined_spans:
                last_span = combined_spans[-1]
                if (abs(span.peak_index - last_span.peak_index) <= HOWFAR_COMBINE_SPANS) and ((span.peak_type >= min_peak_type) or (last_span.peak_type >= min_peak_type)):
                    new_peak_type = max(span.peak_type, last_span.peak_type)
                    if last_span.is_date_only() and span.is_time_only():
                        last_span.combine_with_time(span)
                        combined_spans[-1] = last_span
                        combined_spans[-1].peak_type = new_peak_type
                        combined = True
                    elif last_span.is_time_only() and span.is_date_only():
                        last_span.combine_with_date(span)
                        combined_spans[-1] = last_span
                        combined_spans[-1].peak_type = new_peak_type
                        combined = True
                    elif last_span.type and span.type and ((last_span.type & span.type & Span.DATE_TIME) == 0):
                        if last_span.type > span.type:
                            new_tokens = last_span.tokens + span.tokens
                        else:
                            new_tokens = span.tokens + last_span.tokens
                        new_span = self.tokens_to_span(new_tokens, remove_untagged=True)
                        if new_span:
                            combined_spans[-1] = new_span
                            combined_spans[-1].peak_index = last_span.peak_index
                            combined_spans[-1].peak_type = new_peak_type
                            combined = True
                    if not combined:    
                        if last_span.includes(span):
                            combined_spans[-1] = span
                            combined_spans[-1].peak_type = new_peak_type
                            combined = True
                        elif span.includes(last_span):
                            combined_spans[-1] = last_span
                            combined_spans[-1].peak_type = new_peak_type
                            combined = True             # just drop this span
            if not combined:
                combined_spans.append(span)
            
        combined_spans = [span for span in combined_spans if span.peak_type >= min_peak_type]
        combined_spans = uniqify(combined_spans)        # This tests only "start" and "end" fields
                
        final_span = None
        for span in combined_spans:
            if not final_span:
                final_span = span
            else:
                # Prefer spans with a narrower width, but not if the narrower span occurs too much later in the message body
                if (span.width() < final_span.width()) and (span.peak_index - final_span.peak_index < HOWMANY_TOKENS_GUESS_UNTAGGED_DATETIMES):
                    final_span = span
            
        return [final_span] if final_span else None
    
    def spans_around_peaks(self, tokens):
        # print "spans_around_peaks:", tokens
         
        spans = []
        for peak_index, token in enumerate(tokens):
            if token.peak_type > 0:
                span = self.span_around_peak(tokens, peak_index)
                if span:
                    # log ("Span type:", span.type)
                    spans.append(span)
            
        result = self.combine_spans(spans, CHRONIC_PEAK_TAGGED)
        if (not result):
            result = self.combine_spans(spans, CHRONIC_PEAK_REGULAR)
        
        if result:
            return result
        else:
            return []
        
        
    def find_spans(self, words):
        orig_chronic_tokens = self.tokens_from_words(words)            # Chronic tokens 
        chronic_tokens = self.pre_normalize(orig_chronic_tokens)       # Do some preprocessing
        chronic_tokens = self.scan_all(chronic_tokens)                 # Tag them up
        chronic_tokens = self.calc_token_heatmap(chronic_tokens)        
        chronic_tokens = self.find_heatmap_peaks(chronic_tokens)
        spans = self.spans_around_peaks(chronic_tokens)
        return orig_chronic_tokens, chronic_tokens, spans
    
        
