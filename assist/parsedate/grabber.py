import re
from tokens import *

class Grabber(Tag):
    # TODO: Where's the best place to lower-case the words?
    @staticmethod
    def scan(tokens):
        scanner = {
            r"^last$": PD_LAST,
            r"^this$": PD_THIS,
            r"^next$": PD_NEXT,
            #r"^every$": PD_EVERY,        # Disable recurring events for now [eg 7/22]
            #r"^each$":  PD_EVERY,
        }
        for token in tokens:
            for pattern in scanner:
                if re.match(pattern, token.word):
                    token.tag(Grabber(scanner[pattern]))
                    break
        return tokens
        
    def __repr__(self):
        return 'G:%s' % self.data
        

