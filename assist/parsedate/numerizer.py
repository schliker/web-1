
SCALAR_NUMS = [
    [r'zero', r'0'],
    [r'one', r'1'],
    [r'two', r'2'],
    [r'three', r'3'],
    [r'four(\W|$)', r'4\1'],  # The weird regex is so that it matches four but not fourty
    [r'five', r'5'],
    [r'six(\W|$)', r'6\1'],
    [r'seven(\W|$)', r'7\1'],
    [r'eight(\W|$)', r'8\1'],
    [r'nine(\W|$)', r'9\1'],
    [r'ten', r'10'],
    [r'eleven', r'11'],
    [r'twelve', r'12'],
    [r'thirteen', r'13'],
    [r'fourteen', r'14'],
    [r'fifteen', r'15'],
    [r'sixteen', r'16'],
    [r'seventeen', r'17'],
    [r'eighteen', r'18'],
    [r'nine?teen', r'19'],
    [r'twenty', r'20']
]

