import re, copy
from utils import *
from datetime import *
import traceback, pdb
from base import *
from django.conf import settings


def verify_day_of_week(handler):
    """Decorator, add this to any handler to make it be able to accept "Thursday [,]" before the date
    If that day of the week doesn't match the date, returns None"""
    
    def verifier(self, tokens, options):
        dayname = tokens[0].get_tag(RepeaterDayName)
        if dayname:
            if tokens[1].get_tag(SeparatorComma):
                result = handler(self, tokens[2:], options)
            else:
                result = handler(self, tokens[1:], options)
            if (result is not None) and (result.start.weekday() != dayname.symbol_to_number(dayname.data)):
                result = None
        else:
            result = handler(self, tokens, options)
        return result

    return verifier

class ParseDate(ParseDateBase):
    
    def easy_parse(self, s):
        "Parse a string already known to be a date/time (returns None if not a date/time"
        words = [m[0] for m in RE_TOKENIZE.findall(s)]
        orig_chronic_tokens = self.tokens_from_words(words)            # Chronic tokens 
        chronic_tokens = self.pre_normalize(orig_chronic_tokens)       # Do some preprocessing
        chronic_tokens = self.scan_all(chronic_tokens)                 # Tag them up
        span = self.tokens_to_span(chronic_tokens)
        return span
     
    def tokens_to_span(self, tokens, remove_untagged=False):
        
        self.reset_tokens(tokens)
        
        if remove_untagged:
            filtered_tokens = [token for token in tokens if token.tags]
        else:
            filtered_tokens = tokens
        
        try:
            result = None
            #re_str = Handler.match_string(filtered_tokens)
                        
            if not result:
                for handler in DEFINITIONS['date']:
                    if handler.match(filtered_tokens, DEFINITIONS):
                        if settings.DEBUG_CHRONIC: log("matched date")
                        good_tokens = [t for t in filtered_tokens if not t.get_tag(Separator)]
                        # TODO: Get ranges working 
                        #  (had to disable this for now because expressions like "12-13" (meaning Dec. 13) crash the parser)
                        # good_tokens = [t for t in filtered_tokens if not t.get_tag(Separator) or t.get_tag(Range)]
                        handler_method = eval('self.' + handler.handler_method)
                        result = handler_method(good_tokens, self.options)
                        break
            
            if not result:    
                for handler in DEFINITIONS['anchor']:
                    if handler.match(filtered_tokens, DEFINITIONS):
                        if settings.DEBUG_CHRONIC: log("matched anchor")
                        good_tokens = [t for t in filtered_tokens if not t.get_tag(Separator) or t.get_tag(Range)]
                        handler_method = eval('self.' + handler.handler_method)
                        result = handler_method(good_tokens, self.options)
                        break
                    
            if not result:
                for handler in DEFINITIONS['arrow']:
                    if handler.match(filtered_tokens, DEFINITIONS):
                        if settings.DEBUG_CHRONIC: log("matched arrow")
                        good_tokens = [t for t in filtered_tokens if t.get_tag(Range) or t.get_tag(Pointer) or not (t.get_tag(SeparatorBegin) or t.get_tag(SeparatorSlashOrDash) or t.get_tag(SeparatorComma))]
                        handler_method = eval('self.' + handler.handler_method)
                        result = handler_method(good_tokens, self.options)
                        break
                    
            if not result:
                for handler in DEFINITIONS['narrow']:
                    if handler.match(filtered_tokens, DEFINITIONS):
                        if settings.DEBUG_CHRONIC: log("matched narrow")
                        #good_tokens = [t for t in filtered_tokens if not t.get_tag(Separator)]
                        good_tokens = filtered_tokens
                        handler_method = eval('self.' + handler.handler_method)
                        result = handler_method(good_tokens, self.options)
                        break
            
            if result:
                if handler.strong:
                    result.strong = True
                result.words = ' '.join([token.word for token in filtered_tokens])
                
            # Check for invalid span
            if result and not result.valid():
                result = None
         
            if result:
                result.tokens = tokens
                
            return result
            
        except:
            log("Oops -- I was trying to parse", tokens)
            log("Caught exception (returning None): " + traceback.format_exc())
            return None

    def reset_tokens(self, tokens):
        for t in tokens:
            t.reset()
    
    def find_span_from_token(self, pointer, grabber, head, outer_span, options):        
        if outer_span:
            if (outer_span.width() > ONE_DAY) and (head.width() <= (ONE_DAY/2)):
                return None
            head.start(outer_span.start if pointer == PD_FUTURE else outer_span.end)
            span = head.this(None)
            span.rule = outer_span.rule
            if outer_span.type is not None:
                span.type |= outer_span.type
            if span:
                if outer_span.includes(span.start) and span.end > outer_span.end:
                    span.end = outer_span.end
        else:
            head.start(self.now)
            if grabber is None:
                span = head.this(None)
            elif grabber.data == PD_LAST:
                span = head.next(PD_PAST)
            elif grabber.data == PD_THIS:
                #span = head.this(pointer)            # Changed [Emil]
                span = head.this(None)                  # Changed[Emil]
                # if len(repeaters) > 0:
                #     span = head.this(None)
                # else:
                #     span = head.this(options['context'])
            elif grabber.data == PD_NEXT:
                span = head.next(PD_FUTURE)
            elif grabber.data == PD_EVERY:              # Added [Emil]
            
                # if len(repeaters) > 0:
                #     outer_span = head.every(None)
                # else:
                #     outer_span = head.every(options['context'])
                span = head.every(None)
            else:
                raise Exception("get_anchor: Invalid grabber")
            
            if span.type is not None and grabber and (not grabber.fake):
                span.type |= Span.DATE
        return span

    def find_within(self, tags_before, tags_after, span, pointer, grabber, options, through=False):
                
        if not tags_before:
            return span

        if type(span) == SpanSet:
            spans = span.spans
        elif type(span) == Span:
            spans = [span]
        elif span is None:
            spans = [span]
        else:
            raise NotImplementedError('find_within: unknown span type %s' % str(type(span)))
        
        results = []
        
        for s in spans:
            
            if not tags_after or tags_before[0].width() > tags_after[0].width():
                head, rest = tags_before[0], tags_before[1:]
                h = self.find_span_from_token(pointer, grabber, head, s, options) 
                if h and (not s or s.includes(h.start) or s.includes(h.end)):
                    results.append(self.find_within(rest, tags_after, h, pointer, grabber, options))                                        
            elif tags_before[0].width() < tags_after[0].width():
                head, rest = tags_after[0], tags_after[1:]
                h = self.find_span_from_token(pointer, grabber, head, s, options)
                if h and (not s or s.includes(h.start) or s.includes(h.end)):
                    results.append(self.find_within(tags_before, rest, h, pointer, grabber, options))
            else:
                head_before, rest_before = tags_before[0], tags_before[1:]
                head_after, rest_after = tags_after[0], tags_after[1:]
                h_before = self.find_span_from_token(pointer, grabber, head_before, s, options)
                h_after = self.find_span_from_token(pointer, grabber, head_after, s, options)
                if h_before and h_after and (not s or s.includes(h_before.start) or s.includes(h_before.end) or s.includes(h_after.start) or s.includes(h_after.end)):
                    span_before = self.find_within(rest_before, [], h_before, pointer, grabber, options)
                    span_after = self.find_within(rest_after, [], h_after, pointer, grabber, options)
                    if through:
                        span = Span(span_before.start, span_after.end)
                    else:
                        span = Span(span_before.start, span_after.start)
                    span.rule = span_before.rule        # TODO: Is this correct?
                    results.append(span)               
            
        if len(results) > 1:
            return SpanSet(results)
        elif len(results) == 1:
            return results[0]
        else:
            return None
        
                
    def get_repeaters(self, tokens):
        repeaters = []
        for token in tokens:
            t = token.get_tag(Repeater)
            if t:
                repeaters.append(t)
        repeaters.sort()
        repeaters.reverse()
        return repeaters
        

    def get_anchor(self, tokens, options):
        if settings.DEBUG_CHRONIC: log("get_anchor")
        pointer = PD_FUTURE
        
        grabber = None
        if len(tokens) > 0 and tokens[0].get_tag(Grabber):
            grabber = tokens.pop(0).get_tag(Grabber)
        
        repeaters_before = self.get_repeaters(tokens)
        repeaters_after = []
        through = False
        is_disambiguator = any(isinstance(r, RepeaterDayDisambiguator) for r in repeaters_before)

#        for i, t in enumerate(tokens):                # TODO: Get ranges to work
#            if t.get_tag(RangeInfix):
#                repeaters_before = self.get_repeaters(tokens[:i])
#                repeaters_after = self.get_repeaters(tokens[i+1:])
#                through = bool(t.get_tag(RangeThrough))
#                break
        
        span_forever = Span(datetime.min.replace(tzinfo=self.now.tzinfo), datetime.max.replace(tzinfo=self.now.tzinfo))        
        span = self.find_within(repeaters_before, repeaters_after, None, pointer, grabber, options, through=through)
        
        # New am/pm code (eg, 7/24)
        if (grabber is None) and span and span.type and not (span.type & Span.DATE) and is_disambiguator and options['context'] == PD_FUTURE:
            self.reset_tokens(repeaters_before)
            self.reset_tokens(repeaters_after)
            for r in repeaters_before:
                if isinstance(r, RepeaterDayDisambiguator):
                    r.set_data(options['now'].hour)
            near_span = self.find_within(repeaters_before, repeaters_after, None, pointer, grabber, options, through=through)
            if (near_span and near_span.start - options['now'] <= 3*ONE_HOUR) or (day_start(near_span.start) == day_start(options['now'])):
                span = near_span
            elif span.start < options['now']:
                span.start = rein(span.start + ONE_DAY)
                span.end = rein(span.start + ONE_DAY)               
            else:
                pass        # leave span alone
        return span
             
    # ================ dates
        
    def day_or_time(self, day_start, time_tokens, options):
        outer_span = Span(day_start, day_start + ONE_DAY, type=Span.DATE)
      
        if time_tokens:
            self.now = outer_span.start
            time = self.get_anchor(self.dealias_and_disambiguate_times(time_tokens, options), options)
            return time
        else:
            return outer_span

    def handle_m_d(self, month, day, time_tokens, options):
        month.start(self.now)
        span = month.this(options['context'])
        ds = rein(datetime(year=span.start.year, month=span.start.month, day=day, tzinfo=span.start.tzinfo))
        return self.day_or_time(ds, time_tokens, options)

    # Added [Emil]
    def handle_d(self, day, time_tokens, options):
        ds = rein(datetime(year=self.now.year, month=self.now.month, day=day, tzinfo=self.now.tzinfo))
        if ds < day_start(self.now):
            if ds.month == 12:
                ds.replace(year=ds.year+1, month=1)
            else:
                ds.replace(month=ds.month+1)
        return self.day_or_time(rein(ds), time_tokens, options)
        
    @verify_day_of_week    
    def handle_rmn_sd(self, tokens, options):
        return self.handle_m_d(tokens[0].get_tag(RepeaterMonthName), tokens[1].get_tag(ScalarDay).data, tokens[2:], options)

    @verify_day_of_week
    def handle_rmn_od(self, tokens, options):
        return self.handle_m_d(tokens[0].get_tag(RepeaterMonthName), tokens[1].get_tag(OrdinalDay).data, tokens[2:], options)
 
    @verify_day_of_week
    def handle_rod_mn(self, tokens, options):
        return self.handle_m_d(tokens[1].get_tag(RepeaterMonthName), tokens[0].get_tag(OrdinalDay).data, tokens[2:], options)
         
    # Added [Emil]
    @verify_day_of_week
    def handle_od(self, tokens, options):
        return self.handle_d(tokens[0].get_tag(OrdinalDay).data, tokens[1:], options)
        
    # # Added [Emil]
    # def handle_rdn_od(self, tokens, options):
    #     return self.verify_rdn(tokens, options, self.handle_od)
    #     
    # 
    #     
    #     result = self.handle_od(tokens[1:], options)
    #     dayname = tokens[0].get_tag(RepeaterDayName)
    #     if result.start.weekday() != dayname.symbol_to_number(dayname.data):
    #         # TODO: Smarter handling (maybe return a special exception, to notify user)
    #         return None
    #     else:
    #         return result
            
    def handle_rmn_sy(self, tokens, options):
        month = tokens[0].get_tag(RepeaterMonthName).index()
        year = tokens[1].get_tag(ScalarYear).data

        if month == 12:
            next_month_year = year + 1
            next_month_month = 1
        else:
            next_month_year = year
            next_month_month = month + 1
        
        try:
            return Span(\
                rein(datetime(year=year, month=month, day=1, tzinfo=self.now.tzinfo)),
                rein(datetime(year=next_month_year, month=next_month_month, day=1, tzinfo=self.now.tzinfo)))
        except:
            return None
           
    def handle_rdn_rmn_sd_t_tz_sy(self, tokens, options):
        month = tokens[1].get_tag(RepeaterMonthName).index()
        day = tokens[2].get_tag(ScalarDay).data
        year = tokens[5].get_tag(ScalarYear).data

        try:
            day_start = rein(datetime(year=year, month=month, day=day, tzinfo=self.now.tzinfo))
            return day_or_time(day_start, [tokens[3]], options)
        except:
            return None
    
    @verify_day_of_week 
    def handle_rmn_sd_sy(self, tokens, options):
        month = tokens[0].get_tag(RepeaterMonthName).index()
        day = tokens[1].get_tag(ScalarDay).data
        year = tokens[2].get_tag(ScalarYear).data
        time_tokens = tokens[3:]

        try:
            day_start = rein(datetime(year=year, month=month, day=day, tzinfo=self.now.tzinfo))
            return self.day_or_time(day_start, time_tokens, options)
        except:
            return None
            
    @verify_day_of_week            
    def handle_rmn_od_sy(self, tokens, options):
        month = tokens[0].get_tag(RepeaterMonthName).index()
        day = tokens[1].get_tag(OrdinalDay).data
        year = tokens[2].get_tag(ScalarYear).data
        time_tokens = tokens[3:]

        try:
            day_start = rein(datetime(year=year, month=month, day=day, tzinfo=self.now.tzinfo))
            return self.day_or_time(day_start, time_tokens, options)
        except:
            return None
        
    @verify_day_of_week
    def handle_rod_mn_sy(self, tokens, options):
        month = tokens[1].get_tag(RepeaterMonthName).index()
        day = tokens[0].get_tag(OrdinalDay).data
        year = tokens[2].get_tag(ScalarYear).data
        time_tokens = tokens[3:]

        try:
            day_start = rein(datetime(year=year, month=month, day=day, tzinfo=self.now.tzinfo))
            return self.day_or_time(day_start, time_tokens, options)
        except:
            return None
            
        
    # # Added [Emil]
    # def handle_rdn_rmn_sd_sy(self, tokens, options):
    #     if tokens[1].get_tag(SeparatorComma):
    #         result = self.handle_od(tokens[2:], options)
    #     else:
    #         result = self.handle_od(tokens[1:], options)
    #     dayname = tokens[0].get_tag(RepeaterDayName)
    #     if result.start.weekday() != dayname.symbol_to_number(dayname.data):
    #         # TODO: Smarter handling (maybe return a special exception, to notify user)
    #         return None
    #     else:
    #         return result
    #    
    
    @verify_day_of_week
    def handle_sd_rmn_sy(self, tokens, options):
        new_tokens = [tokens[1], tokens[0], tokens[2]]
        time_tokens = tokens[3:]
        return self.handle_rmn_sd_sy(new_tokens + time_tokens, options)
    
    @verify_day_of_week
    def handle_sm_sd_sy(self, tokens, options):
        month = tokens[0].get_tag(ScalarMonth).data
        day = tokens[1].get_tag(ScalarDay).data
        year = tokens[2].get_tag(ScalarYear).data
        time_tokens = tokens[3:]

        try:
            day_start = rein(datetime(year=year, month=month, day=day, tzinfo=self.now.tzinfo))
            return self.day_or_time(day_start, time_tokens, options)
        except:
            return None
    
    @verify_day_of_week
    def handle_sd_sm_sy(self, tokens, options):
        new_tokens = [tokens[1], tokens[0], tokens[2]]
        time_tokens = tokens[3:]
        return self.handle_sm_sd_sy(new_tokens + time_tokens, options)

    @verify_day_of_week
    def handle_sy_sm_sd(self, tokens, options):
        new_tokens = [tokens[1], tokens[2], tokens[0]]
        time_tokens = tokens[3:]
        return self.handle_sm_sd_sy(new_tokens + time_tokens, options)

    def handle_sm_sy(self, tokens, options):
        month = tokens[0].get_tag(ScalarMonth).data
        year = tokens[1].get_tag(ScalarYear).data

        if month == 12:
            next_month_year = year + 1
            next_month_month = 1
        else:
            next_month_year = year
            next_month_month = month + 1

        try:
            return Span(\
                rein(datetime(year=year, month=month, day=1, tzinfo=self.now.tzinfo)),
                rein(datetime(year=next_month_year, month=next_month_month, day=1, tzinfo=self.now.tzinfo)))
        except:
            return None
            
    # Added [Emil]
    @verify_day_of_week
    def handle_sm_sd(self, tokens, options):    
        month = tokens[0].get_tag(ScalarMonth).data
        day = tokens[1].get_tag(ScalarDay).data
        time_tokens = tokens[2:]

        try:
            start = rein(datetime(year=self.now.year, month=month, day=day, tzinfo=self.now.tzinfo))
            if start < rein(day_start(self.now)) - 30*ONE_DAY:
                start = start.replace(year=self.now.year+1)
            start = rein(start)
            return self.day_or_time(start, time_tokens, options)
        except:
            return None

#        # TODO: Delete the below
#        start = datetime(year=self.now.year, month=month, day=day, tzinfo=self.now.tzinfo)
#        if start < day_start(self.now):
#            start = start.replace(year=self.now.year+1)
#        try:
#            return Span(start, start + ONE_DAY)
#        except:
#            return None    
            
    # # Added [Emil]
    # def handle_rdn_sm_sd(self, tokens, options):
    #     result = self.handle_sm_sd(tokens[1:], options)
    #     dayname = tokens[0].get_tag(RepeaterDayName)
    #     if result.start.weekday() != dayname.symbol_to_number(dayname.data):
    #         # TODO: Smarter handling (maybe return a special exception, to notify user)
    #         return None
    #     else:
    #         return result
        
    # ================ anchors
            
    def handle_r(self, tokens, options):
        dd_tokens = self.dealias_and_disambiguate_times(tokens, options)
        return self.get_anchor(dd_tokens, options)

    def handle_r_g_r(self, tokens, options):
        new_tokens = [tokens[1], tokens[0], tokens[2]]
        return self.handle_r(new_tokens, options)

    def handle_r_r_g_r(self, tokens, options):
        new_tokens = [tokens[2], tokens[0], tokens[1], tokens[3]]
        return self.handle_r(new_tokens, options)
            
    def handle_st_r(self, tokens, options):
        return self.handle_r(tokens[1:], options)
        
    # ================ arrows

    def handle_srp(self, tokens, span, options):
        log("handle_srp")        
        distance = tokens[0].get_tag(Scalar).data
        repeater = tokens[1].get_tag(Repeater)
        pointer = tokens[2].get_tag(Pointer).data
        log("repeater:", repeater)
        return repeater.offset(span, distance, pointer)

    def handle_s_r_p(self, tokens, options):
        log("handle_s_r_p")
        repeater = tokens[1].get_tag(Repeater)
        span = Span(self.now, self.now)
        span.type = Span.DATE_TIME 
        return self.handle_srp(tokens, span, options)

    def handle_p_s_r(self, tokens, options):
        log("handle_p_s_r")
        new_tokens = [tokens[1], tokens[2], tokens[0]]
        return self.handle_s_r_p(new_tokens, options)

    def handle_s_r_p_a(self, tokens, options):
        log("handle_s_r_p_a")
        anchor_span = self.get_anchor(tokens[3:], options)
        return self.handle_srp(tokens, anchor_span, options)

    # ================ narrows

    def handle_orr(self, tokens, outer_span, options):
        repeater = tokens[1].get_tag(Repeater)
        repeater.start(outer_span.start - ONE_SECOND)      # TODO: What is this -ONE_SECOND for?
        ordinal = tokens[0].get_tag(Ordinal).data
        span = None
        for i in range(ordinal):
            span = repeater.next(PD_FUTURE)
            if span.start > outer_span.end:
                span = None
                break
        return span

    def handle_o_r_s_r(self, tokens, options):
        outer_span = self.get_anchor([tokens[3]], options)
        return self.handle_orr(tokens[0:2], outer_span, options)

    def handle_o_r_g_r(self, tokens, options):
        outer_span = self.get_anchor(tokens[2:4], options)
        return self.handle_orr(tokens[0:2], outer_span, options)

    def dealias_and_disambiguate_times(self, tokens, options):
            
        # handle aliases of am/pm
        # 5:00 in the morning -> 5:00 am
        # 7:00 in the evening -> 7:00 pm

        # # TODO: fix this
        # for i, t in enumerate(tokens):
        #     if t.get_tag(RangeInfix):
        #         log("dealias: We don't handle ranges yet")
        #         return tokens
                
        day_portion_index = None
        for i, t in enumerate(tokens):
            if t.get_tag(RepeaterDayPortion):
                day_portion_index = i

        time_index = None
        for i, t in enumerate(tokens):
            if t.get_tag(RepeaterTime):
                time_index = i    

#        day_index = None
#        for i, t in enumerate(tokens):
#            if t.get_tag(RepeaterDay) or t.get_tag(RepeaterDayName) or t.get_tag(Repeater):
#                day_index = i 
                        
        if (day_portion_index is not None) and (time_index is not None):
            t1 = tokens[day_portion_index]
            t1tag = t1.get_tag(RepeaterDayPortion)

            if t1tag.data == PD_MORNING:
                t1.untag(RepeaterDayPortion)
                t1.tag(RepeaterDayPortion(PD_AM))
            elif t1tag.data in [PD_AFTERNOON, PD_EVENING, PD_NIGHT]:
                t1.untag(RepeaterDayPortion)
                t1.tag(RepeaterDayPortion(PD_PM))

        # handle ambiguous times if 'ambiguous_time_range' is specified        
        if (options['ambiguous_time_range'] is not None):
            ttokens = []
            for i, t0 in enumerate(tokens):
                ttokens.append(t0)
                t1 = tokens[i+1] if i+1<len(tokens) else None
                # if t0.get_tag(RepeaterTime) and t0.get_tag(RepeaterTime).data.ambiguous and ((not t1) or (not t1.get_tag(RepeaterDayPortion))):
                if t0.get_tag(RepeaterTime) and t0.get_tag(RepeaterTime).data.ambiguous and (day_portion_index is None):
                    repeater_time = t0.get_tag(RepeaterTime)
                    # if repeater_time.this(context=options['context']).start.hour <= options['ambiguous_time_range'] + 12:          
                    if True:
                        distoken = Token('disambiguator')
                        tag = RepeaterDayDisambiguator(options['ambiguous_time_range'])
                        tag.fake = True
                        distoken.tag(tag)
                        ttokens.append(distoken)
            tokens = ttokens

        return tokens

    # ======================
    # None handler

    def handle_none(self, tokens, options):
        '''A handle that always returns None
        Use this for patterns we want to exclude from Chronic'''
        return None

class Handler(object):
    def __init__(self, pattern, handler_method, strong=False):
        self.pattern = pattern
        self.handler_method = handler_method
        self.strong = strong
        # self.re_pattern = self.calc_re_pattern(pattern)
    
    # TODO: Are we going to use this RE stuff?
       
    # def calc_re_pattern(self, pattern):
    #     pattern_string = '^'
    #     for element in pattern:
    #         if element[-1] == '?':
    #             element = element[:-1]
    #             optional = True
    #         else:
    #             optional = False 
    #                            
    #         if element[0] == ':':       # a class
    #             klass = element[1:]          
    #             pattern_string += r"(\[(\w+,)*" + klass + r"(,\w+)*\])"
    #             if optional:
    #                 pattern_string += r"?"
    #         elif element[0] == '#':
    #             pattern_string += r"%("+name+r")"  # Fill this in later with the right RE
    #         else:
    #             raise NotImplementedError('Unknown element %s' % element)
    #            
    #     pattern_string += '$'
    #     return pattern_string
    #   
    # @staticmethod
    # def match_string(tokens):
    #     """Turn a list of Chronic tokens into a string which can be used for regular expression matching"""
    #     token_tagsets = []
    #     for t in tokens:
    #         class_names = []
    #         for tag in t.tags:
    #             klass = tag.__class__
    #             while klass is not Tag:
    #                 class_names.append(klass.__name__)
    #                 klass = klass.__bases__[0]
    #         token_tagsets.append('[%s]' % ','.join(class_names))
    #     return ''.join(token_tagsets)
    #     
    # def match(self, re_str, definitions):
    #     re_str = match_string(tokens)
    #     return bool(re.match(self.re_pattern, re_str))
    #         
        
    
        
    def match(self, tokens, definitions):
        token_index = 0
        for element in self.pattern:
            if element[-1] == '?':
                element = element[:-1]
                optional = True
            else:
                optional = False
             
            if element[0] == ':':       # a class
                klass = eval(element[1:])
                 
                if token_index < len(tokens):
                    match = tokens[token_index].get_tag(klass)
                else:
                    match = False
                         
                if match:
                    token_index += 1
                    continue
                else:
                    if optional:
                        continue
                    else:
                        return False
                         
                if not match and not optional:
                    return False
                 
            elif element[0] == '#':     # a reference to another pattern type
                name = element[1:]
                 
                if optional and token_index == len(tokens):
                    return True
                     
                sub_handlers = definitions[name]
                for sub_handler in sub_handlers:
                    if sub_handler.match(tokens[token_index:], definitions):
                        return True
                return False
                 
            else:
                raise Exception('Handler.match: unknown element %s' % str(element))
                
                 
        if token_index != len(tokens):
            return False
        return True
        

DEFINITIONS = {

    'time': [ 
        Handler([':RepeaterTime', ':RepeaterDayPortion?', ':RangeInfix?', ':RepeaterTime?', ':RepeaterDayPortion?'], None),
    ],

    'date': [
        #Handler([':OrdinalDay'], 'handle_none'),
        Handler([':SeparatorDate?', ':OrdinalDay'], 'handle_od'),
        # Handler([':RepeaterDayName', ':RepeaterMonthName', ':ScalarDay', ':RepeaterTime', ':TimeZone', ':ScalarYear'], 'handle_rdn_rmn_sd_t_tz_sy'),
        Handler([':RepeaterMonthName', ':ScalarDay', ':SeparatorComma?', ':ScalarYear'], 'handle_rmn_sd_sy'),
      
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':RepeaterMonthName', ':ScalarDay', ':SeparatorComma?', ':ScalarYear', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rmn_sd_sy'),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':RepeaterMonthName', ':OrdinalDay', ':SeparatorComma?', ':ScalarYear', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rmn_od_sy'),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':RepeaterMonthName', ':ScalarDay', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rmn_sd'),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':RepeaterMonthName', ':OrdinalDay', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rmn_od'),
              
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':OrdinalDay', ':SeparatorIn?', ':RepeaterMonthName', ':SeparatorComma?', ':ScalarYear', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rod_mn_sy'),   # added [Emil]
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':OrdinalDay', ':SeparatorIn?', ':RepeaterMonthName', ':SeparatorComma?', ':SeparatorBegin?', '#time?'], 'handle_rod_mn'),     # added [Emil]

        Handler([':RepeaterDayName?', ':SeparatorComma?', ':OrdinalDay', ':SeparatorBegin?', '#time?'], 'handle_od'),   # added [Emil]
        Handler([':RepeaterMonthName', ':ScalarYear'], 'handle_rmn_sy'),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':ScalarDay', ':RepeaterMonthName', ':ScalarYear', ':SeparatorBegin?', '#time?'], 'handle_sd_rmn_sy', True),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':ScalarMonth', ':SeparatorSlashOrDash', ':ScalarDay', ':SeparatorSlashOrDash', ':ScalarYear', ':SeparatorBegin?', '#time?'], 'handle_sm_sd_sy', True),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':ScalarDay', ':SeparatorSlashOrDash', ':ScalarMonth', ':SeparatorSlashOrDash', ':ScalarYear', ':SeparatorBegin?', '#time?'], 'handle_sd_sm_sy', True),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':ScalarYear', ':SeparatorSlashOrDash', ':ScalarMonth', ':SeparatorSlashOrDash', ':ScalarDay', ':SeparatorBegin?', '#time?'], 'handle_sy_sm_sd', True),
        Handler([':RepeaterDayName?', ':SeparatorComma?', ':ScalarMonth', ':SeparatorSlashOrDash', ':ScalarDay', ':SeparatorBegin?', '#time?'], 'handle_sm_sd', True),   # added [Emil]  
        Handler([':ScalarMonth', ':SeparatorSlashOrDash', ':ScalarYear'], 'handle_sm_sy', True)  
        
    ],

    # tonight at 7pm
    'anchor': [
        Handler([':Grabber?', ':RepeaterTimeBareNumber', ':Separator?'], 'handle_none'),
        Handler([':SeparatorTime?', ':SeparatorIn?', ':Grabber', ':Repeater', ':SeparatorBegin?', ':RepeaterNamed?', ':SeparatorComma?', ':SeparatorDate?', ':RepeaterNamed?'], 'handle_r'),
        Handler([':SeparatorTime?', ':SeparatorIn?', ':RepeaterNamed', ':SeparatorBegin?', ':RepeaterNamed?', ':SeparatorComma?', ':SeparatorDate?', ':RepeaterNamed?'], 'handle_r'),
  
        Handler([':SeparatorTime?', ':SeparatorIn?', ':Grabber?', ':Repeater', ':Repeater', ':SeparatorBegin'], 'handle_none'),
  
        Handler([':SeparatorTime?', ':SeparatorIn?', ':Grabber', ':Repeater', ':RepeaterNamed', ':SeparatorBegin?', ':RepeaterNamed?', ':SeparatorComma?', ':SeparatorDate?', ':RepeaterNamed?'], 'handle_r'),
        Handler([':SeparatorTime?', ':SeparatorIn?', ':RepeaterNamed', ':RepeaterNamed', ':SeparatorBegin?', ':RepeaterNamed?', ':SeparatorComma?', ':SeparatorDate?', ':RepeaterNamed?'], 'handle_r'),
  
        Handler([':SeparatorTime?', ':SeparatorIn?', ':RepeaterNamed', ':SeparatorComma?', ':SeparatorDate?', ':Grabber', ':Repeater'], 'handle_r_g_r'),
        Handler([':SeparatorTime?', ':SeparatorIn?', ':RepeaterNamed', ':RepeaterNamed', ':SeparatorDate?', ':Grabber', ':Repeater'], 'handle_r_r_g_r')
    ],

    # 3 weeks from now, in 2 months
    'arrow': [
        Handler([':Scalar', ':Repeater', ':Pointer'], 'handle_s_r_p'),
        Handler([':Pointer', ':Scalar', ':Repeater'], 'handle_p_s_r'),
        Handler([':Scalar', ':Repeater', ':Pointer', '#anchor'], 'handle_s_r_p_a')
    ],

    # 3rd week in March
    'narrow': [
        Handler([':Ordinal', ':Repeater', ':SeparatorIn', ':Repeater'], 'handle_o_r_s_r'),
        Handler([':Ordinal', ':Repeater', ':Grabber', ':Repeater'], 'handle_o_r_g_r')
        # TODO: Handle "Third Thursday in next March"
    ]

}


