import re
from tokens import *
from utils import *
import pdb, traceback
 
rein = reinterpret_by_tz

YEAR_MONTHS =   12

class Repeater(Tag):
    @staticmethod
    def scan(tokens, options):
        for i in range(len(tokens)):
            token = tokens[i]
            pre_token = tokens[i-1] if i > 0 else None
            post_token = tokens[i+1] if i < len(tokens) - 1 else None
            tokens[i].tag(Repeater.scan_for_month_names(token, pre_token, post_token))
            tokens[i].tag(Repeater.scan_for_day_names(token, pre_token, post_token))
            tokens[i].tag(Repeater.scan_for_day_portions(token))
            tokens[i].tag(Repeater.scan_for_times(token, pre_token, post_token, options)) 
            tokens[i].tag(Repeater.scan_for_units(token, options))
        return tokens

    @staticmethod
    def scan_for_month_names(token, pre_token, post_token):
        scanner = {
            r"^jan\.?(uary)?$": PD_JANUARY,
            r"^feb\.?(ruary)?$": PD_FEBRUARY,
            r"^mar\.?(ch)?$": PD_MARCH,
            r"^apr\.?(il)?$": PD_APRIL,
            r"^may$": PD_MAY,
            r"^jun\.?e?$": PD_JUNE,
            r"^jul\.?y?$": PD_JULY,
            r"^aug\.?(ust)?$": PD_AUGUST,
            r"^sep\.?(t\.?|tember)?$": PD_SEPTEMBER,
            r"^oct\.?(ober)?$": PD_OCTOBER,
            r"^nov\.?(ember)?$": PD_NOVEMBER,
            r"^dec\.?(ember)?$": PD_DECEMBER
        }
        
        for pattern in scanner:
            if re.match(pattern, token.word):
                tag = RepeaterMonthName(scanner[pattern])
                
                # Extra care to avoid turning words like "may" into a month every time, unless
                # it's @tagged
                
                score = 0
                if token.orig_word.istitle():
                    score += 1
                if token.orig_word.islower():
                    score -= 0.5
                if (pre_token and pre_token.orig_word.isdigit()) or (post_token and post_token.orig_word.isdigit()):
                    score += 1
                if pre_token and re.match(r"(?i)(a|an|is|in|on|next|last|through|thru|from)$", pre_token.orig_word):
                    score += 1
                elif (not token.orig_word.istitle()) and ((pre_token and pre_token.orig_word.isalpha()) or (post_token and post_token.orig_word.isalpha())):
                    score -= 1                    
                if score <= 0:
                    tag.within_min_peak_type = CHRONIC_PEAK_TAGGED  # Only count it if it's tagged
                return tag
        return None
        
    @staticmethod
    def scan_for_day_names(token, pre_token, post_token):
        scanner = {                                     # These always count
            r"^m[ou]n(day)?$": PD_MONDAY,
            r"^mon\.?$": PD_MONDAY,
            r"^t(ue|eu|oo|u|)s(day)?$": PD_TUESDAY,
            r"^tues?\.?$": PD_TUESDAY,
            r"^we(dnes|nds|nns)(day)?$": PD_WEDNESDAY,
            r"^th(urs|ers)day$": PD_THURSDAY,
            r"^thu(\.|rs?\.?)?$": PD_THURSDAY,
            r"^fr[iy](day)?$": PD_FRIDAY,
            r"^fri\.?$": PD_FRIDAY,
            r"^sat[ue]rday$": PD_SATURDAY,
            r"^su[nm]day?$": PD_SUNDAY,
        }
           
        scanner_ambiguous = {                           # These only count if tagged or certain
            r"^weds?\.?$": PD_WEDNESDAY,                #  other criteria hold
            r"^sat\.?$": PD_SATURDAY,
            r"^sun\.?$": PD_SUNDAY
        }
                  
        for pattern in scanner:
            if re.match(pattern, token.word):
                return RepeaterDayName(scanner[pattern])
            
        for pattern in scanner_ambiguous:
            if re.match(pattern, token.word):
                tag = RepeaterDayName(scanner_ambiguous[pattern])            
                # Extra care to avoid turning words like "sat" into a day every time, unless
                # it's @tagged
                
                score = 0
                if token.orig_word.istitle():
                    score += 1
                if token.orig_word.islower():
                    score -= 0.5
                if (pre_token and pre_token.orig_word.isdigit()) or (post_token and post_token.orig_word.isdigit()):
                    score += 1
                if pre_token and re.match(r"(?i)(meet|meeting|meetup|a|an|is|in|on|next|last|through|thru|from)$", pre_token.orig_word):
                    score += 1
                elif (not token.orig_word.istitle()) and ((pre_token and pre_token.orig_word.isalpha()) or (post_token and post_token.orig_word.isalpha())):
                    score -= 1                    
                if score <= 0:
                    tag.within_min_peak_type = CHRONIC_PEAK_TAGGED  # Only count it if it's tagged
                return tag
            
        return None
       
    @staticmethod
    def scan_for_day_portions(token):
        scanner = {
            r"ams?$": PD_AM,
            r"pms?$": PD_PM,
            r"mornings?$": PD_MORNING,
            r"afternoons?$": PD_AFTERNOON,
            r"evenings?$": PD_EVENING,
            r"(night|nite)s?$": PD_NIGHT
        }

        for pattern in scanner:
            if re.match(pattern, token.word):
                return RepeaterDayPortion(scanner[pattern])
        return None

    @staticmethod
    def scan_for_times(token, pre_token, post_token, options):
        if re.match(r"^\d{1,2}([\.:]\d{2})([\.:]\d{2})?$", token.word):
            try:
                return RepeaterTimeColon(token.word, options)
            except:
                return None
        # Military times present a problem because they look like ordinary numbers,
        # and we don't want every number in an email to be parsed as a time.
        elif re.match(r"^\d{1,4}$", token.word):
            try:
                tag = RepeaterTimeNumber(token.word, options)
                if int(token.word) == 0 or len(token.word) > 2:
                    tag.within_min_peak_type = CHRONIC_PEAK_TAGGED  # Only count it if it's tagged
                if not (post_token and re.match('(?i)(a|p|am|pm)', post_token.word) or \
                       (pre_token and re.match('(?i)(at)', pre_token.orig_word))):
                    tag.within_min_peak_type = CHRONIC_PEAK_TAGGED  # Only count it if it's tagged
                return tag
            except:
                return None
             #            
             # if (pre_token and (pre_token.word.lower() in 'at @'.split())):
             #     try:
             #         return RepeaterTimeNumber(token.word, options)
             #     except:
             #         return None
        return None
                
    @staticmethod
    def scan_for_units(token, options):
        scanner = {
            r"years?$": RepeaterYear,
            r"months?$": RepeaterMonth,
            r"w(ee)?ks?$": RepeaterWeek,
            r"weekdays?$": RepeaterWeekday,
            r"weekends?$": RepeaterWeekend,
            r"da?ys?$": RepeaterDay,
            r"h(ou)?rs?$": RepeaterHour,
            r"min(ute)?s?$": RepeaterMinute,
            r"sec(ond)?s?$": RepeaterSecond
        }
        
        scanner_tagged_only = {
            r"(instants?|moments?)$": RepeaterInstant
        }
        
        for pattern in scanner:
            if re.match(pattern, token.word):
                klass = scanner[pattern]
                tag = klass(options)
                return tag
            
        for pattern in scanner_tagged_only:
            if re.match(pattern, token.word):
                klass = scanner_tagged_only[pattern]
                tag = klass(options)
                tag.within_min_peak_type = CHRONIC_PEAK_TAGGED  # Only count it if it's tagged
                return tag            

                
                
    def __cmp__(self, other):
        return cmp(self.width(), other.width())
        
    def __repr__(self):
        return 'R:%s' % str(self.data)

class RepeaterNamed(Repeater):
    pass


class RepeaterMonthName(RepeaterNamed):

    def __init__(self, data):
        Repeater.__init__(self, data)
        self.reset()
        
    def reset(self):
        self.current_month_begin = None
        
    def next(self, pointer):
       
        if self.current_month_begin is None:
            target_month = self.symbol_to_number(self.data)
            if pointer == PD_FUTURE:
                if self.now.month <= target_month:
                    self.current_month_begin = datetime(year=self.now.year, month=target_month, day=1, tzinfo=self.now.tzinfo)
                elif self.now.month > target_month:
                    self.current_month_begin = datetime(year=self.now.year+1, month=target_month, day=1, tzinfo=self.now.tzinfo)
            elif pointer is None:
                if self.now.month <= target_month:
                    self.current_month_begin = datetime(year=self.now.year, month=target_month, day=1, tzinfo=self.now.tzinfo)
                elif self.now.month > target_month:
                    self.current_month_begin = datetime(year=self.now.year+1, month=target_month, day=1, tzinfo=self.now.tzinfo)
            elif pointer == PD_PAST:
                if self.now.month >= target_month:
                    self.current_month_begin = datetime(year=self.now.year, month=target_month, day=1, tzinfo=self.now.tzinfo)
                elif self.now.month < target_month:
                    self.current_month_begin = datetime(year=self.now.year-1, month=target_month, day=1, tzinfo=self.now.tzinfo)
            
            if self.current_month_begin is None:
                raise Exception("RepeaterMonthName.next: current month should be set by now")
            
        else:
            if pointer == PD_FUTURE:
                self.current_month_begin = datetime(year=self.current_month_begin.year+1, month=self.current_month_begin.month, day=1, tzinfo=self.current_month_begin.tzinfo)
            elif pointer == PD_PAST:
                self.current_month_begin = datetime(year=self.current_month_begin.year-1, month=self.current_month_begin.month, day=1, tzinfo=self.current_month_begin.tzinfo)
        
        self.current_month_begin = rein(self.current_month_begin)
        
        cur_month_year = self.current_month_begin.year
        cur_month_month = self.current_month_begin.month
        if cur_month_month == 12:
            next_month_year = cur_month_year + 1
            next_month_month = 1
        else:
            next_month_year = cur_month_year
            next_month_month = cur_month_month + 1
        
        return Span(rein(self.current_month_begin), rein(datetime(year=next_month_year, month=next_month_month, day=1, tzinfo=self.current_month_begin.tzinfo)))

    def this(self, pointer=PD_FUTURE):
        if pointer == PD_PAST:
            return self.next(pointer)
        elif pointer == PD_FUTURE or pointer is None:
            return self.next(None)
        raise Exception('RepeaterMonthName.this: unknown pointer %s' % str(pointer))
        
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency':  'YEARLY',
            'bymonth': self.symbol_to_number(self.data)
        }
        return span
    
    def offset(self, span, amount, pointer):
        return None             # Nobody ever says "3 Junes from now", don't support it
    
    def width(self):
        return 30*ONE_DAY       # good enough for here  

    def index(self):
        return self.symbol_to_number(self.data)
        
    def symbol_to_number(self, sym):
        lookup = {
            PD_JANUARY: 1,      
            PD_FEBRUARY: 2,
            PD_MARCH: 3,
            PD_APRIL: 4,
            PD_MAY: 5,
            PD_JUNE: 6,
            PD_JULY: 7,
            PD_AUGUST: 8,
            PD_SEPTEMBER: 9,
            PD_OCTOBER: 10,
            PD_NOVEMBER: 11,
            PD_DECEMBER: 12
        }
        return lookup[sym]

    def __repr__(self):
        return 'Rm:%s' % str(self.data)
                
                
class RepeaterDayName(RepeaterNamed):
            
    def __init__(self, data):
        Repeater.__init__(self, data)
        self.reset()
    
    def reset(self):
        self.current_day_start = None
            
    def next(self, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        
        # TODO: Handle "Past"
        if self.current_day_start is None:
            today = day_start(self.now)
            self.current_day_start = today
            # Advance to the start of next week
            while self.current_day_start.weekday() != self.symbol_to_number(PD_SUNDAY):
                self.current_day_start = rein(self.current_day_start + ONE_DAY * direction)
            # Advance to the specific day-of-week we want (must be at least 3 days away) 
            day_num = self.symbol_to_number(self.data)
            while (self.current_day_start.weekday() != day_num) or (direction*(self.current_day_start-today) < 3*ONE_DAY):
                self.current_day_start = rein(self.current_day_start + ONE_DAY * direction)
        else:
            self.current_day_start = rein(self.current_day_start + ONE_WEEK * direction)
        
        return Span(self.current_day_start, self.current_day_start+ONE_DAY, type=Span.DATE)
    
    def this(self, pointer=PD_FUTURE):
        if (pointer is None) or (pointer==PD_FUTURE):
            self.current_day_start = day_start(self.now)
            day_num = self.symbol_to_number(self.data)
            while self.current_day_start.weekday() != day_num:
                self.current_day_start = rein(self.current_day_start+ONE_DAY)
            return Span(self.current_day_start, rein(self.current_day_start+ONE_DAY), type=Span.DATE)       
        else:
            return self.next(pointer)
      
    def offset(self, span, amount, pointer):
        self.now = span.start
        portion_span = self.next(pointer)
        direction = 1 if (pointer == PD_FUTURE) else -1
        result = portion_span + (direction * (amount - 1) * ONE_WEEK)
        result.type = span.type
        return result

    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'WEEKLY',
            'byweekday': self.symbol_to_number(self.data)\
        }
        return span
                
    def width(self):
        return ONE_DAY
    
    def symbol_to_number(self, sym):
        lookup = {
            PD_MONDAY: 0,            # Numbering used by date.weekday() function
            PD_TUESDAY: 1,           # Different from Ruby Chronic numbering!
            PD_WEDNESDAY: 2,
            PD_THURSDAY: 3,
            PD_FRIDAY: 4,
            PD_SATURDAY: 5,
            PD_SUNDAY: 6
        }
        return lookup[sym]
        
    def __repr__(self):
        return 'Rd:%s' % str(self.data)


class RepeaterWeekday(RepeaterNamed):

    def __init__(self, options):
        Repeater.__init__(self)
        self.reset()
    
    def reset(self):
        self.current_day_start = None

    def next(self, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        if self.current_day_start is None:
            self.current_day_start = day_start(self.now)
        self.current_day_start = rein(self.current_day_start + ONE_DAY * direction)
        while self.current_day_start.weekday() not in [0, 1, 2, 3, 4]:
            self.current_day_start = rein(self.current_day_start + ONE_DAY * direction)
    
        return Span(self.current_day_start, rein(self.current_day_start+ONE_DAY), type=Span.DATE)

    def this(self, pointer=PD_FUTURE):
        if pointer is None:
            if self.current_day_start is None:
                self.current_day_start = day_start(self.now)
            while self.current_day_start.weekday() not in [0, 1, 2, 3, 4]:
                self.current_day_start=rein(self.current_day_start+ONE_DAY) 
            return Span(self.current_day_start, rein(self.current_day_start+ONE_DAY), type=Span.DATE)
        else:
            return self.next(pointer)

    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'DAILY',
            'byweekday': [0, 1, 2, 3, 4]        # Monday - Friday
        }
        return span

    def width(self):
        return ONE_DAY

    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        result = span
        for i in range(amount):
            result = result+ONE_DAY*direction
            while result.start.weekday() not in [0, 1, 2, 3, 4]:
                result = result+ONE_DAY*direction
        result.type = span.type
        return result
        
    def __repr__(self):
        return 'Rwkday'


class RepeaterDayPortion(RepeaterNamed):
    
    def __init__(self, data):
        Repeater.__init__(self, data)
        self.reset()
        day_portions = dict((dp['key'], dp['hours']) for dp in DAY_PORTIONS)
        lookup = {
            PD_AM:           day_portions['am'],   
            PD_PM:           day_portions['pm'],  # TODO: Do these need a -1?
            PD_MORNING:      day_portions['morning'],
            PD_AFTERNOON:    day_portions['afternoon'],
            PD_EVENING:      day_portions['evening'],
            PD_NIGHT:        day_portions['night']
        }
        # An interesting use of Span ... it holds two timedeltas
        self.range = Span(timedelta(hours=lookup[data][0]), timedelta(hours=lookup[data][1]))
        self.type = Span.DAYPORTION
   
    def reset(self):
        self.current_span = None
                
    def next(self, pointer):
        if self.current_span is None:
            now_delta = self.now - day_start(self.now)
            if now_delta < self.range.start:
                if pointer == PD_FUTURE:
                    range_start = rein(day_start(self.now) + self.range.start)
                elif pointer == PD_PAST:
                    range_start = rein(day_start(self.now) - ONE_DAY + self.range.start)
            elif now_delta > self.range.end:
                if pointer == PD_FUTURE:
                    range_start = rein(day_start(self.now) + ONE_DAY + self.range.start)
                elif pointer == PD_PAST:
                    range_start = rein(day_start(self.now) + self.range.start)
            else:
                if pointer == PD_FUTURE:
                    range_start = rein(day_start(self.now) + ONE_DAY + self.range.start)
                elif pointer == PD_PAST:
                    range_start = rein(day_start(self.now) - ONE_DAY + self.range.start)
            range_end = rein(range_start + self.range.width())
            if (pointer == PD_FUTURE) and range_end < self.now:     # added [Emil 7/18]
                range_start = rein(range_start + ONE_DAY)
                range_end = rein(range_end + ONE_DAY)
            self.current_span = Span(range_start, range_end, type=self.type)
        else:
            if pointer == PD_FUTURE:
                self.current_span = Span(rein(self.current_span.start + ONE_DAY), rein(self.current_span.end + ONE_DAY), type=self.type)
            elif pointer == PD_PAST:
                self.current_span = Span(rein(self.current_span.start - ONE_DAY), rein(self.current_span.end - ONE_DAY), type=self.type)
        
        return self.current_span
                
    def this(self, pointer = PD_FUTURE):
        
        range_start = rein(day_start(self.now) + self.range.start)
        range_end = rein(range_start + self.range.width())
        if (pointer in [PD_FUTURE, None]) and range_end < self.now:     # added [Emil 7/18]
            range_start = rein(range_start + ONE_DAY)
            range_end = rein(range_end + ONE_DAY)        
        self.current_span = Span(range_start, range_end, type=self.type)

        if pointer == PD_FUTURE:
            if self.now < self.current_span.start:
                span = self.current_span
            else:
                span = None
        elif pointer == PD_PAST:
            if self.current_span.start <= self.now:
                span = Span(self.current_span.start, self.now, type=self.type)
            else:
                span = None
        elif pointer is None:
            span = self.current_span
        
        return span
          
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'DAILY'
        }
        return span
            
    def offset(self, span, amount, pointer):
        self.now = span.start
        portion_span = self.next(pointer)
        direction = 1 if (pointer == PD_FUTURE) else -1
        result = portion_span + (direction * (amount - 1) * ONE_DAY)
        result.type = span.type
        return result
        
    def width(self):
        if self.current_span:
            return self.current_span.width()
        else:
            return self.range.width()
        
    def __repr__(self):
        return 'Rdp:%s' % str(self.data)
        
        
class RepeaterDayDisambiguator(RepeaterDayPortion):
    '''An artificial token, inserted by dealias_and_disambiguate_times in handlers.py
    to resolve am/pm vagueness'''
    
    def __init__(self, data):
        Repeater.__init__(self, data)
        self.set_data(data)
        
    def set_data(self, data):
        self.reset()
        self.data = data
        self.range = Span(timedelta(hours=data), timedelta(hours=data+12))
        self.type = Span.NONE        
    

# ============================================
# Units

class RepeaterUnit(Repeater):
    pass

class RepeaterYear(RepeaterUnit):

    def __init__(self, options):
        self.reset()

    def reset(self):
        self.current_year_start = None
        
    def next(self, pointer):
        if self.current_year_start is None:
            if pointer == PD_FUTURE:
                self.current_year_start = rein(datetime(self.now.year + 1, month=1, day=1, tzinfo=self.now.tzinfo))
            elif pointer == PD_PAST:
                self.current_year_start = rein(datetime(self.now.year - 1, month=1, day=1, tzinfo=self.now.tzinfo))
        else:
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_year_start = rein(self.current_year_start.replace(year=self.current_year_start.year+direction))
    
        return Span(self.current_year_start, rein(self.current_year_start.replace(year=self.current_year_start.year+1)))
        
    def this(self, pointer = PD_FUTURE):
        
        if pointer == PD_FUTURE:
            this_year_start = self.now
            this_year_end = year_start(self.now).replace(year=self.now.year+1)
        elif pointer == PD_PAST:
            this_year_start = year_start(self.now)
            this_year_end = self.now
        elif pointer is None:
            this_year_start = year_start(self.now)
            this_year_end = year_start(self.now).replace(year=self.now.year+1)
            
        return Span(rein(this_year_start), rein(this_year_end))    
        
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'YEARLY'\
        }
        return span
            
    def offset(self, span, amount, pointer):   
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_begin = span.start.replace(year=span.start.year+amount*direction)
        new_end = span.end.replace(year=span.start.year+amount*direction)
        new_span = Span(new_begin, new_end)
        new_span.type = span.type
        return new_span
        
    def width(self):
        return 365*ONE_DAY       # good enough for here

    def __repr__(self):
        return 'Ryear'

class RepeaterMonth(RepeaterUnit):

    def __init__(self, options):
        self.reset()

    def reset(self):
        self.current_month_start = None
        
    def next(self, pointer):
      
        if not self.current_month_start:
            self.current_month_start = rein(self.offset_by(month_start(self.now), 1, pointer))
        else:
            self.current_month_start = rein(self.offset_by(self.current_month_start, 1, pointer))
      
        return Span(self.current_month_start, rein(self.offset_by(self.current_month_start, 1, PD_FUTURE)))

    def this(self, pointer = PD_FUTURE):
        
        if pointer == PD_FUTURE:
            this_month_start = self.now
            this_month_end = rein(self.offset_by(month_start(self.now), 1, PD_FUTURE))
        elif pointer == PD_PAST:
            this_month_start = rein(month_start(self.now))
            this_month_end = self.now
        elif pointer is None:
            this_month_start = rein(month_start(self.now))
            this_month_end = rein(self.offset_by(month_start(self.now), 1, PD_FUTURE))
        
        return Span(this_month_start, this_month_end)        
        
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'MONTHLY',
        }
        return span
            
    def offset(self, span, amount, pointer):      
        new_span = Span(rein(self.offset_by(span.start, amount, pointer)), rein(self.offset_by(span.end, amount, pointer)))
        new_span.type = span.type
        return new_span
        
    def offset_by(self, time, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1

        amount_years = direction * amount / YEAR_MONTHS
        amount_months = direction * amount % YEAR_MONTHS

        new_year = time.year + amount_years
        new_month = time.month + amount_months
        if new_month > YEAR_MONTHS:
            new_year += 1
            new_month -= YEAR_MONTHS
        return time.replace(year=new_year, month=new_month)
      

    def width(self):
        return 30*ONE_DAY       # good enough for here

    def __repr__(self):
        return 'Rmonth'
            
class RepeaterWeek(RepeaterUnit):

    def __init__(self, options):
        self.reset()

    def reset(self):
        self.current_week_start = None
        
    def next(self, pointer):
        if self.current_week_start is None:
            self.current_week_start = day_start(self.now)
            while self.current_week_start.weekday() != 6:
                self.current_week_start = rein(self.current_week_start-ONE_DAY)           
            if pointer == PD_FUTURE:
                self.current_week_start = rein(self.current_week_start+ONE_WEEK)
            elif pointer == PD_PAST:
                self.current_week_start = rein(self.current_week_start-ONE_WEEK)            
        else:
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_week_start = rein(self.current_week_start+ONE_WEEK*direction)
        
        return Span(self.current_week_start, rein(self.current_week_start + ONE_WEEK))
            
    def this(self, pointer = PD_FUTURE):

        this_week_start = day_start(self.now)
        while this_week_start.weekday() != 6:
            this_week_start = rein(this_week_start - ONE_DAY)
            
        if pointer == PD_FUTURE:
            return Span(self.now, rein(this_week_start + ONE_WEEK))
        elif pointer == PD_PAST:
            return Span(this_week_start, self.now)
        elif pointer is None:
            return Span(this_week_start, rein(this_week_start + ONE_WEEK))

    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'WEEKLY'\
        }
        return span
                
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_WEEK*direction*amount
        new_span.start = rein(new_span.start)
        new_span.end = rein(new_span.end)
        new_span.type = span.type
        return new_span      

    def width(self):
        return ONE_WEEK
        
    def __repr__(self):
        return 'Rweek'


class RepeaterWeekend(RepeaterUnit):

    def __init__(self, options):
        self.reset()

    def reset(self):
        self.current_week_start = None

    def next(self, pointer):
        if self.current_week_start is None:
            if pointer == PD_FUTURE:
                saturday_repeater = RepeaterDayName(PD_SATURDAY)
                saturday_repeater.start(self.now)
                next_saturday_span = saturday_repeater.next(PD_FUTURE)
                self.current_week_start = next_saturday_span.start
            elif pointer == PD_PAST:
                saturday_repeater = RepeaterDayName(PD_SATURDAY)
                saturday_repeater.start(rein(self.now + ONE_DAY))
                last_saturday_span = saturday_repeater.next(PD_PAST)
                self.current_week_start = last_saturday_span.start                
        else:
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_week_start = rein(self.current_week_start + ONE_WEEK*direction)

        return Span(self.current_week_start, rein(self.current_week_start + ONE_WEEKEND))

    def this(self, pointer = PD_FUTURE):

        if pointer == PD_FUTURE or pointer is None:
            saturday_repeater = RepeaterDayName(PD_SATURDAY)
            saturday_repeater.start(self.now)
            this_saturday_span = saturday_repeater.this(None)
            return Span(this_saturday_span.start, rein(this_saturday_span.start + ONE_WEEKEND))
        elif pointer == PD_PAST:
            saturday_repeater = RepeaterDayName(PD_SATURDAY)
            saturday_repeater.start(self.now)
            last_saturday_span = saturday_repeater.this(PD_PAST)
            return Span(last_saturday_span.start, rein(last_saturday_span.start + ONE_WEEKEND))
           
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'WEEKLY'\
        }
        return span
                 
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        weekend = RepeaterWeekend(PD_WEEKEND)
        weekend.start(span.start)
        start = rein(weekend.next(pointer).start + (amount - 1) * direction * ONE_WEEK)
        new_span = Span(start, rein(start + (span.end - span.start)))
        new_span.type = span.type
        return new_span
        
    def width(self):
        return ONE_WEEKEND

    def __repr__(self):
        return 'Rwknd'
        
class RepeaterDay(RepeaterUnit):

    def __init__(self, options):
        self.reset()
        self.options = options
        
    def reset(self):
        self.current_day_start = None

    def next(self, pointer):        
        if self.current_day_start is None:
            if self.now.hour < self.options['day_start']:
                self.current_day_start = rein(day_start(self.now) - ONE_DAY)
            else:
                self.current_day_start = day_start(self.now)
        direction = 1 if (pointer == PD_FUTURE) else -1
        self.current_day_start = rein(self.current_day_start + ONE_DAY * direction)
        return Span(self.current_day_start, rein(self.current_day_start+ONE_DAY), type=Span.DATE)

    def this(self, pointer = PD_FUTURE):

        if pointer == PD_FUTURE:
            day_begin = self.now
            day_end = rein(day_start(day_begin) + ONE_DAY)
        elif pointer == PD_PAST:
            day_begin = day_start(self.now)
            day_end = self.now
        elif pointer is None:
            day_begin = day_start(self.now)
            day_end = rein(day_begin + ONE_DAY)

        return Span(day_begin, day_end, type=Span.DATE)

    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'DAILY'\
        }
        return span
            
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_DAY*direction*amount
        new_span.start = rein(new_span.start)
        new_span.end = rein(new_span.end)
        new_span.type = span.type
        return new_span
    
    def width(self):
        return ONE_DAY

    def __repr__(self):
        return 'Rday'
        

class RepeaterHour(RepeaterUnit):

    def __init__(self, options):
        self.reset()
        
    def reset(self):
        self.current_hour_start = None

    def next(self, pointer):

        if not self.current_hour_start:
            if pointer == PD_FUTURE:
                self.current_hour_start = rein(hour_start(self.now) + ONE_HOUR)
            elif pointer == PD_PAST:
                self.current_hour_start = rein(hour_start(self.now) - ONE_HOUR)
        else:
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_hour_start = rein(self.current_hour_start + direction*ONE_HOUR)
            
        return Span(self.current_hour_start, rein(self.current_hour_start+ONE_HOUR), type=Span.TIME)

    def this(self, pointer = PD_FUTURE):
        if pointer == PD_FUTURE:
            hour_begin = self.now
            hour_end = rein(hour_start(self.now) + ONE_HOUR)
        elif pointer == PD_PAST:
            hour_begin = hour_start(self.now)
            hour_end = self.now
        elif pointer is None:
            hour_begin = hour_start(self.now)
            hour_end = rein(hour_start(self.now) + ONE_HOUR)

        return Span(hour_begin, hour_end, type=Span.TIME)
        
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'HOURLY'\
        }
        return span
            
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_HOUR*direction*amount
        new_span.type = span.type
        return new_span
        
    def width(self):
        return ONE_HOUR

    def __repr__(self):
        return 'Rhour'
        
class RepeaterMinute(RepeaterUnit):

    def __init__(self, options):
        self.reset()
        
    def reset(self):
        self.current_minute_start = None

    def next(self, pointer):

        if not self.current_minute_start:
            if pointer == PD_FUTURE:
                self.current_minute_start = rein(minute_start(self.now) + ONE_MINUTE)
            elif pointer == PD_PAST:
                self.current_minute_start = rein(minute_start(self.now) - ONE_MINUTE)
        else:
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_minute_start = rein(self.current_minute_start + direction*ONE_MINUTE)
            
        return Span(self.current_minute_start, rein(self.current_minute_start+ONE_MINUTE), type=Span.TIME)

    def this(self, pointer = PD_FUTURE):
        if pointer == PD_FUTURE:
            minute_begin = self.now
            minute_end = rein(minute_start(self.now) + ONE_MINUTE)
        elif pointer == PD_PAST:
            minute_begin = minute_start(self.now)
            minute_end = self.now
        elif pointer is None:
            minute_begin = minute_start(self.now)
            minute_end = rein(minute_start(self.now) + ONE_MINUTE)
        
        return Span(minute_begin, minute_end, type=Span.TIME)

    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'MINUTELY'\
        }
        return span
            
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_MINUTE*direction*amount
        new_span.start = rein(new_span.start)
        new_span.end = rein(new_span.end)
        new_span.type = span.type
        return new_span

    def width(self):
        return ONE_MINUTE

    def __repr__(self):
        return 'Rmin'        
        
class RepeaterSecond(RepeaterUnit):

    def __init__(self, options):
        self.reset()
        
    def reset(self):
        self.second_start = None

    def next(self, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        
        if self.second_start is None:
            self.second_start = rein(self.now + ONE_SECOND*direction)
        else:
            self.second_start = rein(self.second_start + ONE_SECOND*direction)
            
        return Span(self.second_start, rein(self.second_start+ONE_SECOND), type=Span.TIME)
        
    def this(self, pointer = PD_FUTURE):
        return Span(self.now, rein(self.now + ONE_SECOND), type=Span.TIME)
      
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'SECONDLY'\
        }
        return span
              
    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_SECOND*direction*amount
        new_span.type = span.type
        return new_span

    def width(self):
        return ONE_SECOND

    def __repr__(self):
        return 'Rsec'
        


class RepeaterInstant(RepeaterUnit):
    '''This class serves mostly to support the word "now"
    (which gets pre-processed into "this instant"), to make a zero-width span.
    - Emil (2/2)
    '''
    
    def __init__(self, options):
        self.reset()
        
    def reset(self):
        pass
    
    def next(self, pointer):
        return Span(self.now, self.now, type=Span.TIME)
 
    def this(self, pointer = PD_FUTURE):
        return Span(self.now, self.now, type=Span.TIME)

    def every(self, pointer=PD_FUTURE):
        return None                     # "every instant" doesn't make sense
        
    def offset(self, span, amount, pointer):
        return span

    def width(self):
        return timedelta(0)

    def __repr__(self):
        return 'Rsec'
        
class Tick(object):

    def __init__(self, td, ambiguous=False):
        self.td = td                    # a timedelta               
        self.ambiguous = ambiguous      # a Boolean

    def __mul__(self, other):
        return Tick(self.td*other, self.ambiguous)

    def __str__(self):
        return '%s%s' % (repr(self.td), '?' if self.ambiguous else '.')


class RepeaterTime(RepeaterNamed):

    def __init__(self, time, options={}):
        self.reset()
        t = time.replace(':', '')

        if len(t) == 1 or len(t) == 2:
            hours = int(t)    
            if hours > 12:
                raise ValueError        # added [Emil]
            if hours == 12:
                self.data = Tick(timedelta(0), True)
            else:
                self.data = Tick(hours*ONE_HOUR, True)

        elif len(t) == 3:
            hours = int(t[0])
            minutes = int(t[1:3])
            if hours > 23 or minutes > 59:
                raise ValueError
            else:
                self.data = Tick(hours*ONE_HOUR+minutes*ONE_MINUTE, True)

        elif len(t) == 4:
            hours = int(t[0:2])
            minutes = int(t[2:4])
            ambiguous = (int(t[0]) != 0)
            if hours > 24 or minutes > 59:          # allow 24:00 for 'midnight'
                raise ValueError
            elif hours == 12:
                self.data = Tick(minutes*ONE_MINUTE, ambiguous)
            else:
                self.data = Tick(hours*ONE_HOUR+minutes*ONE_MINUTE, ambiguous)

#        elif len(t) == 5:
#            hours = int(t[0])
#            minutes = int(t[1:3])
#            seconds = int(t[3:5])
#            if minutes > 59 or seconds > 59:
#                raise ValueError
#            else:
#                self.data = Tick(hours*ONE_HOUR+minutes*ONE_MINUTE+seconds*ONE_SECOND, True)
#
#        elif len(t) == 6:
#            hours = int(t[0:2])
#            minutes = int(t[2:4])
#            seconds = int(t[4:6])
#            ambiguous = (':' in time) and (int(t[0]) != 0) and (int(t[0:2]) <= 12)
#            if hours > 23 or minutes > 59 or seconds > 59:
#                raise ValueError
#            elif hours == 12:
#                self.data = Tick(minutes*ONE_MINUTE+seconds*ONE_SECOND, ambiguous)
#            else:
#                self.data = Tick(hours*ONE_HOUR+minutes*ONE_MINUTE+seconds*ONE_SECOND, ambiguous)                        

        else:
            raise Exception('RepeaterTime.init: Time %s cannot exceed six digits' % time)

    def reset(self):
        self.current_time = None

    def next(self, pointer):
                    
        FULL_DAY = timedelta(hours=24)
        HALF_DAY = timedelta(hours=12)

        first = False

        if self.current_time is None:
            first = True
            midnight = day_start(self.now)
            yesterday_midnight = rein(midnight - FULL_DAY)
            today_noon = rein(midnight + HALF_DAY)
            tomorrow_midnight = rein(midnight + FULL_DAY)

            while self.current_time is None:
                if pointer == PD_FUTURE:
                    if self.data.ambiguous:
                        for t in [midnight+self.data.td, today_noon+self.data.td, tomorrow_midnight+self.data.td]:
                            if t >= self.now:
                                self.current_time = t
                                break
                    else:
                        for t in [midnight+self.data.td, tomorrow_midnight+self.data.td]:
                            if t >= self.now:
                                self.current_time = t
                                break                        
                else:       # pointer == PD_PAST
                    if self.data.ambiguous:
                        for t in [today_noon+self.data.td, midnight+self.data.td, yesterday_midnight+self.data.td*2]:
                            if t <= self.now:
                                self.current_time = t
                                break
                    else:
                        for t in [midnight+self.data.td, yesterday_midnight+self.data.td]:
                            if t <= self.now:
                                self.current_time = t
                                break
                            
                self.current_time = rein(self.current_time)

        if not first:
            increment = HALF_DAY if self.data.ambiguous else FULL_DAY
            direction = 1 if (pointer == PD_FUTURE) else -1
            self.current_time = rein(self.current_time + increment*direction)

        return Span(self.current_time, rein(self.current_time+self.width()), type=Span.TIME)    

    def this(self, pointer=PD_FUTURE):
        if pointer is None:
            pointer=PD_FUTURE
        return self.next(pointer)

    def offset(self, span, amount, pointer):
        direction = 1 if (pointer == PD_FUTURE) else -1
        new_span = span + ONE_DAY*direction*amount
        new_span.start = rein(new_span.start)
        new_span.end = rein(new_span.end)
        new_span.type = span.type
        return new_span
    
    def every(self, pointer=PD_FUTURE):
        span = self.this(pointer=pointer)
        span.rule = {\
            'frequency': 'DAILY'\
        }
        return span
            
    def width(self):
        return timedelta(0)     # TODO: Is this okay for point events? [Emil]

    def __repr__(self):
        return 'Rt:%s' % str(self.data) 


class RepeaterTimeColon(RepeaterTime):
    pass
    
class RepeaterTimeNumber(RepeaterTime):
    pass
    
class RepeaterTimeBareNumber(RepeaterTimeNumber):
    pass
