"""
Code for parsing dates/times
Inspired by Chronic for Ruby (http://chronic.rubyforge.org/)
"""

from utils import *
from datetime import *
import re, pdb

from tokens import *
from grabber import *
from ordinal import *
from pointer import *
from scalar import *
from separator import *
from range import *
from repeater import *

class ParseDateBase(object):
    
    def __init__(self, options={}):
        self.set_options(options)
    
    def set_options(self, options={}):
        self.options = {
            'context':  PD_FUTURE,
            'now':      now(),
            'guess':    False,
            'ambiguous_time_range': 8,          # If a time is given without am/pm, assume it's within this range (am-pm)
            'day_start':    3                   # Between midnight and this time, "tomorrow" will actually mean the same day.
        }
        self.options.update(options)
        self.now = self.options['now']
    
    def tokens_from_words(self, words, tagged_word_lists=None):
        "Convert a list of words to a Chronic-style list of Tokens."
    
        tokens = [Token(word, index=i) for i, word in enumerate(words)]
        
        if tagged_word_lists:
            for key, tagged_words in tagged_word_lists.items():
                for i, tagged_word in enumerate(tagged_words):
                    setattr(tokens[i], key, tagged_word[1])
                
        return tokens
        

    def pre_normalize(self, tokens):
              
        # TODO: Move all these RE's outside
        SUBS = [
            # The following lines from Chronic don't need to be implemented here, because
            # the tokenizer has already taken care of this for us:  (???)
            # normalized_text.gsub!(/['"\.]/, '')
            # normalized_text.gsub!(/([\/\-\,\@])/) { ' ' + $1 + ' ' }

            (1, r'zero\b', r'0'),
            (1, r'one\b', r'1'),
            (1, r'two\b', r'2'),
            (1, r'three\b', r'3'),
            (1, r'four\b', r'4'),
            (1, r'five\b', r'5'),
            (1, r'six\b', r'6'),
            (1, r'seven\b', r'7'),
            (1, r'eight\b', r'8'),
            (1, r'nine\b', r'9'),
            (1, r'ten\b', r'10'),
            (1, r'eleven\b', r'11'),
            (1, r'twelve\b', r'12'),
            (1, r'thirteen\b', r'13'),
            (1, r'fourteen\b', r'14'),
            (1, r'fifteen\b', r'15'),
            (1, r'sixteen\b', r'16'),
            (1, r'seventeen\b', r'17'),
            (1, r'eighteen\b', r'18'),
            (1, r'nine?teen\b', r'19'),
            (1, r'twenty\b', r'20'),

            (1, r'first\b', r'1st'),
            (1, r'second\b', r'2nd'),
            (1, r'third\b', r'3rd'),
            (1, r'fourth\b', r'4th'), 
            (1, r'fifth\b', r'5th'),
            (1, r'sixth\b', r'6th'),
            (1, r'seventh\b', r'7th'),
            (1, r'eighth\b', r'8th'),
            (1, r'ninth\b', r'9th'),
            (1, r'tenth\b', r'10th'),
            (1, r'eleventh\b', r'11th'),
            (1, r'twelfth\b', r'12th'),
            (1, r'thirteenth\b', r'13th'),
            (1, r'fourteenth\b', r'14th'),
            (1, r'fifteenth\b', r'15th'),
            (1, r'sixteenth\b', r'16th'),
            (1, r'seventeenth\b', r'17th'),
            (1, r'eighteenth\b', r'18th'),
            (1, r'nine?teenth\b', r'19th'),
            (1, r'twentieth\b', r'20th'),

            (1, r"today\b", 'this day'),
            (1, r"tomm?orr?ow\b", 'next day'),
            (1, r"yesterday\b", 'last day'),
            (1, r"noon\b", '12:00 pm'),
            (1, r"midnight\b", '24:00'),
            (2, r"before now\b", 'past'),
        
            (1, r"now\b", 'this instant'),
            #(1, r"(ago|before)\b", 'past'),
            (2, r"this past\b", 'last'),
            (2, r"this last\b", 'last'),

            (3, r"(?:in|during) the (morning)\b", '$1'),
            (2, r"(?:at|@) (afternoon|evening|night)\b", '$1'),
            (3, r"(?:in the|during the) (afternoon|evening|night)\b", '$1'),
            (1, r"tonight\b", 'this night'),
            #(1, r"(?=\w)([ap]m|oclock)", '$1'),
            (1, r"(\d+:?\d*:?\d*)am?", '$1 am'),
            (1, r"(\d+:?\d*:?\d*)pm?", '$1 pm'),
            #(1, r"(hence|after|from)\b", 'future'),
        
        ]
        MAX_TOKENS = max([ntokens for ntokens, pattern, replacement in SUBS]) 
    
        # TODO: numericize_numbers
                      
        t = list(enumerate(tokens[:]))
        result = []
        while t:
            replaced = False
            s = ' '.join([token.word for i, token in t[0:MAX_TOKENS]])
            for sub in SUBS:
                ntokens, pattern, replacement = sub
                match = re.match(pattern, s)
                if match:
                    # TODO: Is there a better way to do this, using re.sub?
                    try:
                        replacement = replacement.replace('$1', match.group(1))
                    except:
                        replacement = replacement
                    add_at = (any([old_token.orig_word.startswith('@') for i, old_token in t[0:ntokens]]))
                    for word in replacement.split():
                        if add_at:
                            word = '@' + word
                        result.append(Token(word, index=t[0][1].index))
                    for i, del_token in t[0:ntokens]:
                        tokens[i].pre_normalized = True
                    del t[0:ntokens]
                    replaced = True
                    break
            if not replaced:
                i, append_token = t.pop(0)
                result.append(append_token)
                tokens[i].pre_normalized = False
                     
        # TODO: numericize_ordinals   
        t = result
        result = []
        last_colon = False
        for (i, token) in enumerate(t):
            if token.word == ':':
                if result and re.match('^\d+$', result[-1].word):
                    result[-1].word += ':'
                else:
                    result.append(token)
                last_colon = True
            else:
                if last_colon and re.match('^\d+$', token.word):
                    result[-1].word += token.word
                else:
                    result.append(token)
                last_colon = False
            
        return result
    
    
    def scan_all(self, tokens):
        Repeater.scan(tokens, self.options)
        Grabber.scan(tokens)
        Pointer.scan(tokens)
        Scalar.scan(tokens)
        Ordinal.scan(tokens)
        Separator.scan(tokens)
        Range.scan(tokens)
        return tokens
    

    def tokens_to_span(self, tokens, options):
        raise NotImplementedError("tokens_to_span is abstract")
    
