import re
from tokens import *

class Ordinal(Tag):
    @staticmethod
    def scan(tokens):
        for i in range(len(tokens)):
            token = tokens[i]
            pre_token = tokens[i-1] if i > 0 else None
            post_token = tokens[i+1] if i < len(tokens)-1 else None
            tokens[i].tag(Ordinal.scan_for_ordinals(token, post_token))
            tokens[i].tag(Ordinal.scan_for_days(token, pre_token, post_token))
        return tokens

    @staticmethod
    def scan_for_ordinals(token, post_token):
        match = re.match(r"^(\d+)(st|nd|rd|th)$", token.word)
        if match:
            return Ordinal(int(match.group(1)))
        return None

    @staticmethod
    def scan_for_days(token, pre_token, post_token):
        match = re.match(r"^(\d+)(st|nd|rd|th)$", token.word)
        # Filter out expressions like "6th and Colorado" that are addresses.
        # We might have to revisit this later if we want to allow "Meet me on the 6th and 7th".
        post_match = re.match(r"(?i)^(and|\&)$", post_token.word) if post_token else None
        pre_match = re.match(r"(?i)^(and|at)$", pre_token.word) if pre_token else None
        if match and int(match.group(1)) <= 31 and not (post_match or pre_match):
            return OrdinalDay(int(match.group(1)))
        return None

    def __repr__(self):
        return 'O:%s' % str(self.data)

class OrdinalDay(Ordinal):
    def __repr__(self):
        return 'Od:%s' % str(self.data)
    

    