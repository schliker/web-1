from django.utils.translation import ugettext as _
from assist.models import *
from models import *
from twitter.models import *
from assist import register
from assist import reminder
from assist import mailthread
from utils import *
from django.db.models import Q
from django.core import serializers
import hashlib
import re
from datetime import *
import random
import json
import traceback, pdb
from collections import defaultdict
from assist import parsedate
from assist import mail, mailthread
import email
from django.conf import settings

# ===========================================================
# processing an incoming email

RE_DELIVERY = re.compile(r"(?i)delivery (status|subsystem)")


def get_bounced_email_address(e, edata, extras):
    if 'X-Failed-Recipients' in extras['headers']:
        return extras['headers']['X-Failed-Recipients'].split(',')[0].strip().lower()
    if re.search(r"(?i)(undelivered mail|delivery status)", e.subject):
        m = re.search(r"(?i)<(.*)>:\s*unknown", edata)
        if m:
            return m.groups()[0].strip().lower()
    return None
    

def is_processable(e, extras): 
#    if e.subject and RE_DELIVERY.search(e.subject):
#        return False
#    if e.from_name and RE_DELIVERY.search(e.from_name):
#        return False
    if is_ignorable_from_address(extras['addr_space'], e.from_address, ignore_mailer_daemon=False):
        return False
    
    # Ignore mail from the assistant... unless it's a forwarded message.
    # (We handle forwarded messages because these are usually quickbox replies from another account.
    # If they are from the same account, we'll detect it as a dup later on and ignore it.)
    if 'X-Mailer' in extras['headers'] and extras['headers']['X-Mailer'] == EMAIL_X_MAILER_NOTED:
        if not 'X-Noted-Forward-ID' in extras['headers']:
            return False
    
    if e.source in [Email.SOURCE_CRAWLED, Email.SOURCE_API]:
        if len(extras['tocc_noted']) > 0:
            return False
    
    return True

    

def is_duplicate(e, account):
    '''Return True if this message is a dup of an existing message in the DB for this account'''
    # Avoid using count() here -- that's very slow in Postgres
    
    try:
        # Existing message with this message ID?
        Email.objects.get(message_id=e.message_id, owner_account=account)
        return True
    except:
        pass
        
    try:
        # Existing message with the same subject, form address and date sent?
        Email.objects.get(from_address=e.from_address, subject=e.subject, date_sent=e.date_sent, owner_account=account)
        return True
    except:
        pass
    
    return False

    
def procmail(e, edata, extras, debug_print=False, debug=False, mode=None, edata_is_rfc822=False):
    '''Legacy function to process mails that arrive either via the quickbox, or directly emailed to the assistant.
    Crawled messages use a completely different mechanism - procmail_message_dict below.
    TODO: GET RID OF THIS and do everything through procmail_message_dict!
    
    e: an Email object
    edata: either the plain-text content of the message, or (if edata_is_rfc822=True) the rfc822 source of the message
    mode: optional - a dictionary of mode flags, this gets populated as the mail is processed
    '''
        
    log ('--- procmail ---')
        
    if mode is None:
        mode = {}
            
    if 'stats' not in mode:
        mode['stats'] = TimeStats()
        
    mode.update({
        'success':      False,
        'wmessages':    [],
    })
    
    stats = mode['stats']
        
    stats.start('procmail_intro')

    threader = mailthread.EmailThreader()
    
    try:
        # Some preliminaries
        
#        f = open('/Users/calenvy/weird.log', 'a')
#        print >> f, 'Date:', e.date_sent
#        print >> f,'From:', e.from_address
#        print >> f,'To:', e.to_address
#        print >> f,'Subject:', e.subject_normalized
#        print >> f,'Message-ID:', e.message_id
#        print >> f,'In-Reply-To:', e.in_reply_to
#        print >> f,'Folder', e.server_emailfolder.name if e.server_emailfolder else None
#        print >> f,'Path', e.server_msg_path
#        f.close()
            
        stats.start('procmail_intro_1')     
        if debug:   pdb.set_trace()     # EXCUSE
            
        log("&&&& procmail: master_override = ", mode.get('master_override', False))
        
        if not is_processable(e, extras):
            # Don't process mail from one of our own assistants
            log("^^^^^ not processable: mail from %s" % e.from_address)
            mode['success'] = False
            return mode
                    
        if e.source == Email.SOURCE_DIRECT and not e.crawled_alias and edata_is_rfc822:
            mail.send_forwarded_mail_to_incoming(e, edata)      # for debugging
          
        stats.end('procmail_intro_1')     

        stats.start('procmail_intro_2')     

        if e.type == Email.TYPE_RFC822 and edata_is_rfc822:
            py_message = email.Parser.Parser().parsestr(str(strip_nonascii(edata)))
            ics_files = mail.get_ics_files_from_message(py_message)
        else:
            ics_files = []
        
        processed = False
        processed_reply = False
        processed_files = False
        private = False
        processed_registration = False
                        
        mode.update({
            'source':           e.source,
            'is_bcc':           False,          # We might set this to True later
            'is_file':          False,          # We might set this to True later
            'is_registration':  False,          # We might set this to True later,
            'is_reply':         bool((e.in_reply_to and e.in_reply_to.strip()) or (mail.is_subject_reply(e.subject_normalized))),
            'public_twitter':   mode.get('public_twitter', False),
            'forwarded_mode':   None,
            'autoextract_contact_fields': False
        })
                           
        # Figure out:
        #  assistant_account -- the account of the assistant who appears to be handling this email
        #  account -- the account that is really handling this email (i.e., who are we working for)
        #  e.owner_alias -- the alias who "owns" this email
        #  e.from_contact -- the contact who sent this mail (if any -- None if it's a registration on a new account)
        
        # Determine assistant_account
        
        stats.end('procmail_intro_2')     

        assistant_account = None
        assistant_base_url = None
        e.from_contact = None
        
        # TODO: RIP out this whole block of code!
        if e.source == Email.SOURCE_TWITTER:
            if extras.get('twitter_email_type', None) in ['direct_message', 'reply', 'retweet']:
                try:
                    to_id = extras['twitter_recipient_id']
                    from_id = extras['twitter_sender_id']
                    to_tu = TwitterUser.objects.get(user_id=to_id)
                    try:
                        from_tu = TwitterUser.objects.exclude(alias=None).get(user_id=from_id)
                        e.owner_alias = Alias.objects.active().get(pk=from_tu.alias.pk)
                    except:
                        from_tu = None
                        e.owner_alias = None
                    if to_tu.is_noted and e.owner_alias:
                        # @calenvy DM -- for now, we only support tweets from Aliases   
                        e.from_contact, mode['autoextract_contact_fields'] = e.owner_alias.contact, False
                        assistant_account = e.owner_alias.account
                        assistant_base_url = assistant_account.base_url
                    elif to_tu in TwitterUser.objects.get_crawlable():
                        # Customer support tweet address
                        assistant_account = to_tu.alias.account if to_tu.alias else to_tu.account
                        assistant_base_url = assistant_account.base_url
                        e.owner_alias = assistant_account.get_default_admin()
                        e.from_contact, mode['autoextract_contact_fields'] = Contact.objects.get_or_create_smart(\
                            source = Contact.SOURCE_TWITTER,
                            twitter_screen_name = extras['twitter_sender_screen_name'],
                            name = extras['twitter_sender_name'],
                            account = assistant_account,
                            stats = stats
                        )
                    else:
                        raise Exception('Cannot process this Twitter mail')
                except:
                    log(traceback.format_exc())
                    raise Exception('Cannot process this Twitter mail')
            else:
                raise Exception('Cannot process this Twitter mail type: %s' % extras['twitter_email_type'])

        elif len(extras['tocc_noted']) >= 1:            
            domains = uniqify([addr.lower().strip().split('@')[-1] for name, addr in extras['tocc_noted']])
            if len(domains) > 1:
                # TODO: Remove this restriction
                raise NotImplementedError("Email may not contain multiple To/Cc addresses from different domains:", extras['tocc_noted'])
            
            #assistant_base_url = '.'.join(domains[0].split('.')[:-2])    # Ignore third-level domains in assistant addresses [eg 12/4]
            
            if assistant_base_url:
                try:
                    assistant_account = Account.objects.get(name=assistant_base_url)
                except:
                    pass
            else:
                all_addresses = [e.from_address.lower()] + [x[1].lower() for x in extras['tocc_other']]
                 # Don't filter for active accounts here! This way, if we email to an inactive account, the mail will be ignored
                 # (below) instead of doing a new registration.
                assistant_email = normalize_assistant_address(extras['tocc_noted'][0][1]) # Ignore third-level domains in assistant addresses [eg 12/4]
                #assistant_email = assistant_email.split('@')[0] + '@'   # search for assistant email addresses starting with this, but ignore the specific site
                alias_queryset = Alias.objects.filter(Q(email__in=all_addresses) | Q(contact__extrainfo__data__in=all_addresses)).filter(account__assistant_set__email=assistant_email).distinct() 
                try:
                    assistant_base_url = alias_queryset[0].account.base_url
                    assistant_account = alias_queryset[0].account
                except:
                    # Handle replies to the assistant in an individual account
                    parent_email = threader.find_parent_email(e, ics_files=ics_files, assistant_email=assistant_email)
                    if parent_email:
                        assistant_account = parent_email.owner_account
                        assistant_base_url = assistant_account.base_url
                    else:
                        assistant_account = None
                                 
            # Mails forwarded to the assistant (literally, with Fwd:)  
            if assistant_base_url and mail.is_subject_forward(e.subject_normalized) and len(extras['tocc_other']) == 0:
                mode['forwarded_mode'] = 'fwd'
                                   
            if e.crawled_alias:
                e.owner_alias = e.crawled_alias
            elif e.from_contact and e.from_contact.alias:
                e.owner_alias = e.from_contact.alias

            if e.owner_alias:
                assistant_account = e.owner_alias.account
                account = assistant_account
            
        elif len(extras['tocc_noted']) == 0:
            
            stats.start('procmail_intro_3')     
            
            # No assistant mentioned in the To: or Cc:, so it's either crawled or Bcc:   
                                 
            if e.source in [Email.SOURCE_CRAWLED, Email.SOURCE_API]:
                assistant_account = e.crawled_alias.account
                account = assistant_account
                e.owner_alias = e.crawled_alias
                
                # If the mail is definitely sent from the user, but not using their usual email address,
                # add them as a secondary contact
                if (e.server_emailfolder and e.server_emailfolder.sent) or mode.get('from_user'):
                    e.from_contact, mode['autoextract_contact_fields'] = e.crawled_alias.contact, False
                    stats.start('call_gcs')
                    if extras['addr_space'] == ADDR_SPACE_RFC822 and (e.from_address != e.from_contact.email):
                        Contact.objects.get_or_create_smart(\
                            update_alias=True,
                            replace_extrainfo=False,        # must be false -- don't overwrite the main email address!!
                            contact_to_update=e.from_contact,
                            email=e.from_address,
                            viewing_alias=e.crawled_alias,
                            stats=stats
                        )
                    # TODO: Should we add the twitter user in this case, also?
                    stats.end('call_gcs')
            else:
                log("Assistant not in email to/cc: setting silent to True")   
                mode['silent'] = True                       # Bcced emails don't send out reminders or other emails, period (eg 1/12/10)
                forwarded_to = uniqify(extras.get('forwarded_to', []) + extras.get('delivered_to', []))
                if forwarded_to:
                    forwarded_to = [addr.lower().strip() for addr in forwarded_to]
                    all_addresses = [e.from_address.lower()] + [x[1].lower() for x in extras['tocc_other']]
                    queryset = Alias.objects.active().filter(Q(email__in=all_addresses) | Q(contact__extrainfo__data__in=all_addresses)).filter(account__assistant_set__email__in=forwarded_to)
                    try:
                        e.owner_alias = queryset[0]
                        mode['forwarded_mode'] = 'gmail'
                    except:
                        raise NotImplementedError("Email cannot be forwarded to us for a non-account holder")
                else:
                    log("is_bcc: True")    
                    mode['is_bcc'] = True
                    # TODO: Determine the bcc address from postfix
                    e.owner_alias = Alias.objects.get_preferred_alias_by_email(e.from_address, include_pending=True)
                    if not e.owner_alias or not e.owner_alias.account:                
                        raise NotImplementedError("Email cannot be Bcced to us from a non-account holder")                                                      
                assistant_account = e.owner_alias.account
                account = assistant_account
            
            stats.end('procmail_intro_3')     
    
        stats.start('procmail_intro_4')     

        # Determine account 
        log("mode['source']: ", mode['source'])
        log("e.crawled_alias: ", e.crawled_alias)
        log("assistant_account: ", assistant_account)
        
        if mode['source'] in [Email.SOURCE_CRAWLED, Email.SOURCE_API]:
            account = e.crawled_alias.account
        else:
            account = assistant_account
        
        log("account: ", account)
        
        # For account EXPIRATION
        #  TODO: What if account expired (but still in DB) and someone is trying to register in that account?
        #  TODO: Should we check assistant_account here?
        if account and (account.status != ACCOUNT_STATUS_ACTIVE) and not mode.get('master_override', False):
            log("Mail sent to inactive account %s, ignoring" % account.base_url)
            mode['success'] = False
            return mode
            
        # Find an owner_alias if we still don't know it.
        if not e.owner_alias and (e.source != Email.SOURCE_TWITTER) and account:
            addr = e.from_address.strip().lower()
            queryset = Alias.objects.active().filter(account=account).filter(Q(email=addr) | Q(contact__extrainfo__data=addr))
            try:
                e.owner_alias = queryset[0]
            except:
                for name, addr in extras['to'] + extras['cc'] + extras['bcc']:
                    addr = addr.strip().lower()
                    queryset = Alias.objects.active().filter(account=account).filter(Q(email=addr) | Q(contact__extrainfo__data=addr))
                    try:
                        e.owner_alias = queryset[0]
                        break
                    except:
                        pass
            if not e.owner_alias:
                e.owner_account = account
                parent_email = threader.find_parent_email(e, ics_files=ics_files)
                if parent_email:
                    if parent_email.original_email:
                        e.owner_alias = parent_email.original_email.owner_alias
                    else:
                        e.owner_alias = parent_email.owner_alias
        
        if account and not e.owner_alias:
            log("Can't determine owner_alias for mail %s ... returning" % e.message_id)
            mode['success'] = False
            return mode
                                
        stats.end('procmail_intro_4')     

        stats.start('procmail_intro_5')     

        # Get all the text from the message (converted from HTML if need be)
        # Includes footers, quoted mail, etc -- we filter those out later                
        content = mail.get_content_from_email(e, edata=edata, edata_is_rfc822=edata_is_rfc822)[0].strip()
        
        if e.source in [Email.SOURCE_CRAWLED, Email.SOURCE_API]:
            for pattern in EMAIL_IGNORE_PATTERNS:
                if re.search(pattern, e.subject_normalized + content):
                    log("Phrase %s found, skipping message %s" % (pattern, e.subject_normalized))
                    mode['success'] = False
                    return mode
                
        if debug:   pdb.set_trace()     # EXCUSE
   
        stats.end('procmail_intro_5')     
        
        stats.start('procmail_intro_6')     
        content, quoted_headers, quoted_lines, other_lines, footer, footer_fields = mail.get_content_sections(e, content, contact=e.from_contact)
        stats.end('procmail_intro_6')     

        e.message_body = (content + '\n\n' + footer).strip()[:MAX_CONTENT_BODY_LENGTH]
        
        # Ignore mail from mailing lists, unless there's a contact by that name in the account
        stats.start('procmail_intro_7')     
        
        if e.source in [Email.SOURCE_CRAWLED, Email.SOURCE_API]:
            if (('List-Unsubscribe' in extras['headers']) or \
                (extras['headers'].get('precedence', '').lower() == 'bulk') or \
                ('unsubscribe' in (footer + other_lines).lower())) and \
                (Contact.objects.active().filter(owner_account=account).filter(Q(email=e.from_address) | Q(extrainfo__data=e.from_address)).count() == 0) and \
                (Alias.objects.active().filter(Q(contact__email=e.from_address)|Q(contact__extrainfo__data=e.from_address)).count() == 0):
                log ("Ignoring mail from mailing list:", e.from_address, e.subject)
                mode['success'] = False
                return mode
        
            if account and account.block_confidential_mails and RE_CONFIDENTIAL.search(e.subject_normalized):
                log("Skipping confidential message: %s" % e.subject_normalized)
                mode['success'] = False
                return mode
                
        # For forwarded messages, handle the quoted message as a separate message
        if mode['forwarded_mode'] == 'gmail' and quoted_lines:
            quoted_mode = procmail_forwarded_gmail(e, quoted_headers, quoted_lines, mode)
            if quoted_mode:
                mode['quoted_email'] = quoted_mode['email']

        if mode['forwarded_mode'] == 'fwd' and quoted_lines:
            quoted_mode = procmail_forwarded(e, quoted_headers, quoted_lines, mode)
            if quoted_mode:
                mode['email'] = quoted_mode['email']   
                mode['success'] = True
                return mode         
                  
        stats.end('procmail_intro_7')     

        log("account: ", account)
        log("e.from_contact: ", e.from_contact)
        log("extras['addr_space']: ", extras and extras.get('addr_space'))

        if account and not e.from_contact:
            if extras['addr_space'] == ADDR_SPACE_RFC822:
                e.from_contact, mode['autoextract_contact_fields'] = Contact.objects.get_or_create_smart(\
                    name = e.from_name, email = e.from_address, viewing_alias = e.owner_alias, account=account,
                    status=Contact.STATUS_NEEDSACTIVATION) # Don't create an active contact for incoming mails (yet)
            elif extras['addr_space'] == ADDR_SPACE_TWITTER:
                e.from_contact, mode['autoextract_contact_fields'] = Contact.objects.get_or_create_smart(\
                    name = e.from_name, twitter_screen_name = e.from_address, viewing_alias = e.owner_alias, account=account,
                    status=Contact.STATUS_NEEDSACTIVATION) # Don't create an active contact for incoming mails (yet)                
        
        # Add the sender as a secondary email, if any                
        if e.from_contact and 'sender' in extras and e.source in [Email.SOURCE_DIRECT, Email.SOURCE_API, Email.SOURCE_CRAWLED]:
            sender_name, sender_address = extras['sender']
            sender_address = sender_address.strip().lower()
            if sender_address != e.from_contact.email and (not is_ignorable_from_address(extras['addr_space'], sender_address)) and (sender_address not in SENDER_NO_SECONDARY_EMAIL) and (not is_assistant_address(sender_address)):
                # Don't create the secondary email if there's already a contact with that email address -- we don't want to merge!
                if Contact.objects.filter(owner_account=account).filter(Q(email=sender_address) | Q(extrainfo__data=sender_address)).count() == 0:
                    Contact.objects.get_or_create_smart(\
                        replace_extrainfo=False,        # must be false -- don't overwrite the main email address!!
                        contact_to_update=e.from_contact,
                        email=sender_address,
                        viewing_alias=e.owner_alias,
                        stats=stats
                    )
            
        if debug:   pdb.set_trace()     # EXCUSE
        
        log("procmail [1]")
                
        mode['account'] = account
        mode['usage'] = None
                    
        mode['is_guest'] = (not e.from_contact) or (not e.from_contact.alias)
                
        mode['footer_fields'] = footer_fields
        if (mode['source'] in [Email.SOURCE_CRAWLED, Email.SOURCE_API]):
            mode['crawled_alias'] = e.crawled_alias
            mode['usage'] = e.crawled_alias
        else:
            if mode['is_guest']:
                mode['usage'] = account
            elif account:
                mode['usage'] = e.owner_alias
            else:
                mode['usage'] = None
        mode['display_more'] = any([quoted_headers.strip(), quoted_lines.strip(), other_lines.strip(), footer.strip()])
                        
        if 'ref_contact_id' in extras:
            mode['ref_contact_id'] = extras['ref_contact_id']
        if 'ref_bucket_slug' in extras:
            mode['ref_bucket_slug'] = extras['ref_bucket_slug']
        if 'reply_to_all' in extras:
            mode['reply_mode'] = 'all_thread' if extras['reply_to_all'] else 'nearby_only'
            mode['reply_to_all'] = extras['reply_to_all']
        elif len(extras['tocc_noted']) > 0: 
            mode['reply_mode'] = 'email_assistant'
        else:
            mode['reply_mode'] = 'default'
        if 'private' in extras:
            mode['private'] = extras['private']
        
        e.owner_account = mode['account']
        stats.end('procmail_intro')

        log("procmail [11]")

        # Process mails from mailer daemons that indicate bounced addresses
        
        if is_mailer_daemon(e.from_address) or \
            (e.from_name and RE_DELIVERY.search(e.from_name)) or \
            (e.subject and RE_DELIVERY.search(e.subject)):
            if edata is not None:
                bounced_address = get_bounced_email_address(e, edata, extras)
                if bounced_address and mode['account']:
                    for c in Contact.objects.filter(owner_account=mode['account'])\
                        .filter(Q(email=bounced_address) | Q(extra_info__data=bounced_address)):
                        c.status = Contact.STATUS_INACTIVE
                        c.save()
                                    
            # Don't do anything else with these mails, just ignore them
            mode['success'] = False
            return mode
                
        rule = EmailRule.objects.find_procmail_rule(e)
        if rule:
            if rule.action == EmailRule.ACTION_DELETE:
                log("procmail: Mail %s is ignored according to rules -- returning" % str_or_none(e.message_id))
                mode['success'] = False       
                return mode  
        log("procmail [12]")

        stats.start('threading')
                
        # Don't process the same mail again if it already exists in this account
        if not mode.get('debug_ignoredups', False) and is_duplicate(e, account):
            log("procmail: Mail %s is a dup -- returning" % str_or_none(e.message_id))
            mode['success'] = False       
            return mode
                
        # Find parent/child emails
        if 'quoted_email' in mode:
            parent_email = mode['quoted_email']
        else:
            parent_email = threader.find_parent_email(e, ics_files=ics_files)
        child_emails = threader.find_child_emails(e)
        mode['is_reply'] |= bool(parent_email)
        mode['parent_email'] = parent_email
        mode['child_emails'] = child_emails    
        e.parent = parent_email
        if parent_email:
            if parent_email.original_email:
                e.original_parent = parent_email.original_email
            elif parent_email.message_type != EMAIL_TYPE_NOTED:
                e.original_parent = parent_email
            else:
                e.original_parent = None
        else:
            e.original_parent = None
        
        log("procmail [13]")
        
        if e.original_parent:
            e.original_parent.set_newsfeed_thread_date(e.newsfeed_thread_date)
            
        mode['original_parent_email'] = e.original_parent
        mode['nostore_files'] = (mode['account'].store_email_howlong == 0)
        
        # When processing the quotes inside of forwarded mails, we ignore the subject if the quote turns out to
        # be the child of another mail
        if mode.get('ignore_subject_if_has_parent', False) and e.original_parent:
            e.subject_normalized = ''
        
        # Past this point, we know we're keeping the message, so save it
                
        if any([is_ignorable_to_address(extras['addr_space'], addr) for name, addr in extras['to'] + extras['cc']]):
            mode['success'] = False
            return mode            
            
        e.save() 
        
        log("procmail [14]")

        # This must happen after the save so that the child's foreign key can point to this email
        if child_emails:
            for child in child_emails:
                old_original_parent = child.original_parent
                child.original_parent = e
                child.parent = e
                child.save()
                # If we've unlinked the child from another parent, have that parent recalculate its newsfeed_thread_date
                if old_original_parent and (old_original_parent != child.original_parent):
                    log("procmail: unlinking child from another parent and recalculating newsfeed_thread_date")
                    
                    old_original_parent.set_newsfeed_thread_date()
            e.set_newsfeed_thread_date()
                
        stats.end('threading')

        if debug:   pdb.set_trace()         # EXCUSE 
              
        log("procmail [15]")
              
        stats.start('procmail_e2c')
        # Store the cc, to and bcc relationships
        if e.from_contact:
            e.create_email2contact_by_contact(e.from_contact, account, Email2Contact.TYPE_FROM, footer_fields = mode['footer_fields'] if mode['autoextract_contact_fields'] else None)
        
        if mode['account']:
            e.owner_account = mode['account']
        update_date = e.date_sent if not mode['is_guest'] else None
                        
        if e.source != Email.SOURCE_TWITTER:
            status = Contact.STATUS_NEEDSACTIVATION
            for name, addr in extras['to']:
                e.create_email2contact_by_nameaddr(name, addr, e.owner_alias, account, Email2Contact.TYPE_TO, update_date=update_date, status=status, addr_space=extras['addr_space'])
            for name, addr in extras['cc']:
                e.create_email2contact_by_nameaddr(name, addr, e.owner_alias, account, Email2Contact.TYPE_CC, update_date=update_date, status=status, addr_space=extras['addr_space'])
            for name, addr in extras['bcc']:
                e.create_email2contact_by_nameaddr(name, addr, e.owner_alias, account, Email2Contact.TYPE_BCC, update_date=update_date, status=status, addr_space=extras['addr_space'])
                
            if e.crawled_alias and Email2Contact.objects.filter(email=e, contact=e.crawled_alias.contact).count() == 0:
                e.create_email2contact_by_contact(e.crawled_alias.contact, account, Email2Contact.TYPE_CRAWLED)
                
        stats.end('procmail_e2c')
        
        log("procmail [16]")
        
        # Try to process it as a reminder, even if it's already been processed as registration
        if (not processed) and (not processed_reply) and (mode['account']): 
            # TODO: Refactor this when we have more message types that can include files
                    # TODO: Refactor this when we have more message types that can include files

            if e.type == Email.TYPE_RFC822 and py_message and not mode['nostore_files']:
                files = mail.get_files_from_message(py_message, e, store=True, account=mode['account'])
            else:
                files = []

            mode['is_file'] = bool(files)
            if mode['is_file']:
                # Save file sizes
                size = sum(f[0].size for f in files if f[0].size)
                if mode['usage']:
                    if type(mode['usage']) is Alias:
                        mode['usage'].storage_used_mb_files += float(size)/ONE_MEG
                        mode['usage'].save()
                    elif type(mode['usage']) is Account:
                        mode['usage'].anon_storage_used_mb_files += float(size)/ONE_MEG
                        mode['usage'].save()                       
                     
            log("procmail [16a]")       
            processed, private = reminder.procmail_reminder(e, extras, content, quoted_headers, quoted_lines, files, ics_files, e.from_contact, mode, debug=debug)
            log("procmail [16b]")       

                
        if not processed and not processed_registration and not processed_files:
            e.message_type = EMAIL_TYPE_USER
            e.message_subtype = EMAIL_SUBTYPE_USER_PARSE_FAILED
        
        # Save the Email again in case it's been modified
        
        log("procmail [17]")
        
        stats.start('procmail_usage')
        if mode['usage']:
            if type(mode['usage']) is Alias:
                mode['usage'].storage_used_mb_msgs += float(len(edata))/ONE_MEG
                mode['usage'].save()
            elif type(mode['usage']) is Account:
                mode['usage'].anon_storage_used_mb_msgs += float(len(edata))/ONE_MEG
                mode['usage'].save()     
        stats.end('procmail_usage')

        stats.start('procmail_endgame')
        
        e.save()
        mode['email'] = e

        if e.owner_alias and e.source in [Email.SOURCE_CRAWLED, Email.SOURCE_API, Email.SOURCE_TWITTER]:
            log("Calling notify_all_widgets...")
            e.owner_alias.notify_all_widgets()
        
        stats.end('procmail_endgame')
        
        log("procmail [18]")
        
        mode['success'] = True
                          
    except:
        log(traceback.format_exc())
        mode['success'] = False
        mode['traceback'] = traceback.format_exc()
        
    return mode


def procmail_direct(stream, rcpttos=None, mode=None, debug=False):
    '''Process an email sent directly to the assistant'''
    try:
        # TODO: Use the rpcttos to handle Bcc: mail
                   
        if type(stream) is str:
            text = stream
        else:
            text = stream.read()
                
        parser = email.Parser.Parser()
        edata = strip_nonascii(text)
        message = parser.parsestr(edata)
        e, edata, extras = Email.email_from_rfc822_message(message, source=Email.SOURCE_DIRECT)
        return procmail(e, edata, extras, mode=mode, debug=debug, edata_is_rfc822=True)
    except:
        log(traceback.format_exc())  

        
def procmail_quickbox(**kwargs):
    '''Process something entered into the quickbox'''
    try:
        kwargs['type'] = Email.TYPE_OTHER        
        kwargs['source'] = Email.SOURCE_QUICKBOX
        e, edata, extras = Email.email_from_content(**kwargs)
        return procmail(e, edata, extras, mode=kwargs.get('mode', None))
    except:
        log(traceback.format_exc())
    

def procmail_forwarded_gmail(e, quoted_headers, quoted_lines, mode):
    '''Process the quoted lines of an email as a separate message'''
    # TODO: Check if old message already exists somehow

    try:
        if (not quoted_lines) or (not quoted_lines.strip()):
            return None                     # No quote
        
        if quoted_headers:
            
            # Don't bother parsing dates for right now... it doesn't show up in the newsfeed anyway.
            # Just use e.date_sent - ONE_SECOND.
            
#            words = [m[0] for m in RE_TOKENIZE.findall(quoted_headers)]
#            from assist.parsedate.extended import ParseDateExtended
#            p = ParseDateExtended(options={'now': e.date_sent})
#            orig_chronic_tokens, chronic_tokens, spans = p.find_spans(words)
#            if spans:
#                quoted_date = spans[0].start
#                if quoted_date >= e.date_sent or quoted_date < e.date_sent - ONE_WEEK:
#                    quoted_date = None
        
            m = RE_EMAIL_SEARCH.search(quoted_headers)
            if m:
                quoted_address = m.group(0).strip().lower()
                if Contact.objects.filter(alias=e.owner_alias).filter(Q(email=quoted_address) | Q(extrainfo__data=quoted_address)).count() == 0:
                    return None             # Quote is by somebody else
            
        e, em, extras = Email.email_from_content(\
            source = Email.SOURCE_DIRECT,
            source_subtype = Email.SOURCE_SUBTYPE_EMAIL_QUOTED,
            alias=e.owner_alias, 
            content=quoted_lines,
            subject = mail.strip_subject_reply_or_forward(e.subject_normalized),
            date = e.date_sent - ONE_SECOND,
            message_id = e.in_reply_to,
            to = formataddr_unicode((e.from_name, e.from_address)))
        
        # Only handle the subject if it's the start of a thread.
        # Do NOT propagate the mode from the caller!
        return procmail(e, em, extras, mode={'ignore_subject_if_has_parent': True})
    except:
        log(traceback.format_exc())
        

def procmail_forwarded(e, quoted_headers, quoted_lines, mode):
    '''Process the quoted lines of an email as a separate message'''
    # TODO: Check if old message already exists somehow

    try:
        if (not quoted_lines) or (not quoted_lines.strip()) or (not quoted_headers) or (not quoted_headers.strip()):
            return None                     # No quote
                
        m = re.search(r"(?i)from:\s*([\w '@.-]+).*", quoted_headers)
        if m:
            line = m.group(0)
            from_name = m.group(1).strip()
            if RE_EMAIL_SEARCH.search(from_name):
                from_name = ''
            m = RE_EMAIL_SEARCH.search(line)
            if m:
                from_address = m.group(0).strip().lower()
            else:
                return None
        else:
            return None
                
        m = re.search(r"(?i)subject:(.*)", quoted_headers)
        if m:
            subject = m.group(1).strip()
        else:
            subject = mail.strip_subject_reply_or_forward(e.subject)
                                
        e, edata, extras = Email.email_from_content(\
            source = Email.SOURCE_DIRECT,
            source_subtype = Email.SOURCE_SUBTYPE_EMAIL_QUOTED,
            alias = e.owner_alias,
            content = quoted_lines,
            subject = mail.strip_subject_reply_or_forward(e.subject_normalized),
            date = e.date_sent - ONE_SECOND,
            message_id = e.in_reply_to,
            from_name = from_name,
            from_address = from_address,
            to_name = e.owner_alias.contact.get_full_name(),
            to_address = e.owner_alias.email)
        
        # Only handle the subject if it's the start of a thread.
        # Do NOT propagate the mode from the caller!
        return procmail(e, edata, extras, mode={'ignore_subject_if_has_parent': True})
    except:
        log(traceback.format_exc())
        
            
        
def procmail_twitter(tweet, source_subtype, to_tu):
    '''Process something we got from the Twitter API
    (Not used for the email notifications Twitter sends us)'''
    
    # TODO: Get rid of this function and use procmail_message_dict below,
    #  and use tweepy
    
    try:
        if source_subtype == Email.SOURCE_SUBTYPE_TWITTER_REPLY:
            # if 'in_reply_to_user_id' in tweet:
            twitter_email_type = 'reply'
            # to_tu is filled in here
        elif source_subtype == Email.SOURCE_SUBTYPE_TWITTER_RETWEET:
            twitter_email_type = 'retweet'
        else:
            raise Exception()       # Don't handle DMs yet (TODO)
        
        dt, timezone = parsedate_from_email(tweet.get('created_at'))
                           
        if 'retweeted_status' in tweet:
            in_reply_to = str(tweet['retweeted_status']['id'])
        elif tweet.get('in_reply_to_status_id'):
            in_reply_to = str(tweet['in_reply_to_status_id'])
        else:
            in_reply_to = None
        
        kwargs = {
            'type':                             Email.TYPE_TWITTER,
            'source':                           Email.SOURCE_TWITTER,
            'source_subtype':                   source_subtype,
            'content':                          decode_htmlentities(tweet['text']).strip(),
            'date':                             dt,
            'twitter_email_type':               twitter_email_type,
            'twitter_sender_id':                str(tweet['user']['id']),
            'twitter_sender_name':              tweet['user']['name'],
            'twitter_sender_screen_name':       tweet['user']['screen_name'],
            'twitter_recipient_id':             str(to_tu.user_id),
            'twitter_recipient_name':           to_tu.name,
            'twitter_recipient_screen_name':    to_tu.screen_name,
            'in_reply_to':                      in_reply_to,
            'message_id':                       str(tweet['id']),
            'from_name':                        tweet['user']['name'],
            'from_address':                     tweet['user']['screen_name']
        }
        
        if to_tu:
            kwargs['to_name'] = to_tu.name
            kwargs['to_address'] = to_tu.screen_name
                
        e, edata, extras = Email.email_from_content(**kwargs)
                
        return procmail(e, edata, extras, mode=kwargs.get('mode', None))
    except:
        log(traceback.format_exc())
        
        
def procmail_fakeevent(**kwargs):
    '''Create a fake email/event that confirms a form email you sent out'''
    try:         
        alias = kwargs['alias']
        span = kwargs.get('span', None)
        e, edata, extras = Email.email_from_content(**kwargs)
        e.message_type = EMAIL_TYPE_USER
        e.message_subtype = kwargs.get('message_subtype', None)
        e.newsfeed_thread_date = e.date_sent
        e.subject = kwargs.get('subject', '')
        e.subject_normalized = kwargs.get('subject', '')
        e.message_body = kwargs.get('body') or edata
        e.date_processed = e.date_sent
        e.owner_account = alias.account
        e.from_contact = alias.contact
        e.owner_alias = alias
        e.summary = kwargs['subject'] + '\n' + kwargs['body']
        
        e.save()
        
        e.create_email2contact_by_contact(alias.contact, alias.account, Email2Contact.TYPE_FROM)
        
        for c in kwargs.get('ref_contacts', []):
            e.create_email2contact_by_contact(c, alias.account, Email2Contact.TYPE_REF)
        
        content = kwargs['subject'] + '\n' + kwargs['body']
        
        if kwargs['event_status']:
            # Create an event if there is a scheduled event to be created
            event = reminder.generic_event(alias.contact, e, kwargs['event_status'], content, False, True, {'account': alias.account})
            event.owner_account = alias.account
            event.creator = alias
            event.owner = alias
            event.send_reminders = kwargs.get('send_reminders', False)
        
            if span:
                event.start = span.start
                event.end = span.end
            event.save()

            e.events.add(event)
            
        for c in kwargs['contacts']:
            e.create_email2contact_by_contact(c, alias.account, kwargs['contacts_type'])

        return e
    
        # return procmail(e, em, extras, mode=kwargs.get('mode', None))
    except:
        log(traceback.format_exc())
                       

# ==================================================
# Functions for processing new-style message_dicts
#  Eventually, most of procmail.py / reminder.py can be replaced by the code below

def is_duplicate_message_dict(message_dict, account):
    '''Return True if this message dict is a dup of an existing message in the DB for this account'''
    # Avoid using count() here -- that's very slow in Postgres
    
    try:
        # Existing message with this message ID?
        Email.objects.get(message_id=message_dict['message_id'], owner_account=account)
        return True
    except:
        pass
        
    try:
        # Existing message with the same subject, form address and date sent?
        date_sent = pytz.utc.localize(datetime.strptime(message_dict['date_sent'], "%Y-%m-%d %H:%M:%S"))
        Email.objects.get(from_address=message_dict['from']['addr'], subject=message_dict['subject'], date_sent=date_sent, owner_account=account)
        return True
    except:
        pass
    
    return False


def is_processable_message_dict(message_dict, account):
    '''
    Determine whether this message_dict (to be owned by the given account) can be processed.
    Returns False if the message_dict is a dup, is to/from the assistant, etc.
    '''
    
    fromm = message_dict['from']
    if is_ignorable_from_address(fromm.get('addr_space', ADDR_SPACE_RFC822), fromm['addr'], ignore_mailer_daemon=False):
        log(" - ignoring (no-reply, etc) message from ", fromm['addr'])
        return False
    
    # Ignore mail from the assistant... unless it's a forwarded message.
    #  (We handle forwarded messages because these are usually quickbox replies from another account.
    #  If they are from the same account, we'll detect it as a dup later on and ignore it.)
    headers = message_dict.get('headers', {})
    if 'X-Mailer' in headers and headers['X-Mailer'] == EMAIL_X_MAILER_NOTED:
        if not 'X-Noted-Forward-ID' in headers:
            log(" - ignoring message with X-Mailer = ", EMAIL_X_MAILER_NOTED)
            return False
    
    #if any(is_assistant_address(to['addr']) for to in message_dict['to_all']):
    #    log(" - ignoring message to assistant")
    #    return False
    
    if is_duplicate_message_dict(message_dict, account):
        log(" - ignoring dup: ", message_dict['message_id'])
        return False
    
    return True
        

def procmail_message_dict(message_dict, alias, cloud_manager=None):
    '''Process an email from a Vijul-style message_dict, quickly.
    This is used to process messages passed to our callback API by the celery_worker crawler.
    
    This is meant to be quick and bypasses most of the mechanism in procmail and reminder.py,
    by creating Email and Event objects directly.
    
    Some very minimal threading and checking for message dups is done in this routine.
    '''
    
    account = alias.account
    n = now()
    threader = mailthread.EmailThreader()

    log("procmail_message_dict: alias = ", alias, " subject = ", message_dict['subject'])
     
    if not is_processable_message_dict(message_dict, alias.account):
        log(" - done")
        return None
    
    # Create the Email object
    e = Email()        
    e.version = Email.VERSION
    e.type = message_dict.get('type', Email.TYPE_RFC822)
    e.message_type = EMAIL_TYPE_USER
    e.message_subtype = EMAIL_SUBTYPE_USER_TAGONLY 
    e.message_id = message_dict['message_id']
    e.owner_alias = alias
    e.crawled_alias = alias
    e.owner_account = alias.account
    e.in_reply_to = message_dict.get('in_reply_to')
    e.references = message_dict.get('references')
    e.summary = message_dict['summary']['body'] if message_dict['summary']['body'] else message_dict['summary']['subject'] #' '.join([message_dict['summary']['subject'], message_dict['summary']['body']])
    
    fromm = message_dict.get('from')
    if fromm:
        e.from_name = fromm['name']
        e.from_address = fromm['addr']
    to = message_dict.get('to')
    if to:
        e.to_name = to['name']
        e.to_address = to['addr']
            
    date_sent_str = message_dict['date_sent']
    if date_sent_str:
        date_sent = pytz.utc.localize(datetime.strptime(date_sent_str, "%Y-%m-%d %H:%M:%S"))
    else:
        date_sent = None
    e.date_sent = date_sent
    e.newsfeed_thread_date = date_sent
    e.date_processed = n
    
    e.subject = message_dict['subject']
    e.subject_normalized = message_dict['subject_normalized']
    
    headers = {}        
    if message_dict.get('headers'):
        headers = message_dict['headers']
    if message_dict.get('to_all'):                      # New-style JSON structure for representing to/cc folks, see desktoppy API for structure
        headers['to_all'] = message_dict['to_all']
    if message_dict.get('extra'):
        headers.update(message_dict['extra'])
    e.headers = json.dumps(headers)
    
    e.source = Email.SOURCE_CRAWLED
    e.source_subtype = None
    
    #e.message_body = message_dict['body']
    
    # Set the folder and UID (or OWA "message path") that this email came from, so that we can go back later to
    #  download file attachments on demand
    if 'extra' in message_dict:
        if 'path' in message_dict['extra']:
            e.server_msg_path = message_dict['extra']['path']
        if 'folder' in message_dict['extra']:
            try:
                e.server_emailfolder = EmailFolder.objects.get(alias=alias, name=message_dict['extra']['folder']['name'])
            except:
                pass
            
    parent_email = threader.find_parent_email(e)            # TODO: handle ics files?
    if parent_email: 
        e.parent = e.original_parent = parent_email
                   
    e.save()            # We need to save the email in order to do create_email2contact_by_nameaddr below
        
    # Save the message_dict in Cloud Files
    # This has to happen after the first e.save() so that we have the message ID in the database
    if settings.MOSSO_MESSAGES_ENABLED:
        from lib.cloud import CloudFilesManager
        if not cloud_manager:
            cloud_manager = CloudFilesManager()
            
        filename = '%s.%d' % (alias.email, e.pk)
        
        # Actually save to Cloud Files
        container_name = account.get_cloud_files_container_name()
        e.message_dict_path = cloud_manager.save_file(json.dumps(message_dict), container_name, filename)
        e.save()
            
    # Get or create the Contacts and link them to the email
    status = Contact.STATUS_NEEDSACTIVATION
    extras = e.get_extras()
    
    e.from_contact = e.create_email2contact_by_nameaddr(fromm['name_normalized'], fromm['addr'], alias, alias.account, Email2Contact.TYPE_FROM, addr_space=extras['addr_space'], update_date=date_sent, status=status)
    
    for name, addr in extras['to']:
        e.create_email2contact_by_nameaddr(name, addr, e.owner_alias, account, Email2Contact.TYPE_TO, update_date=date_sent, status=status, addr_space=extras['addr_space'])
    for name, addr in extras['cc']:
        e.create_email2contact_by_nameaddr(name, addr, e.owner_alias, account, Email2Contact.TYPE_CC, update_date=date_sent, status=status, addr_space=extras['addr_space'])
        
    # If the alias whose mailbox we're crawling isn't one of the contacts (From:, To: Cc:) add it as the
    #  special type TYPE_CRAWLED 
    if e.crawled_alias and Email2Contact.objects.filter(email=e, contact=alias.contact).count() == 0:
        e.create_email2contact_by_contact(e.crawled_alias.contact, account, Email2Contact.TYPE_CRAWLED)
    
    e.save()
        
    # Deal with file attachments by creating File objects which point to the IMAP section of the original email containing the file
    for fp in message_dict.get('file_placeholders', []):
        # Create the File
        f = File()
        f.slug = create_slug(length=20)
        f.name = fp['name']
        f.mimetype = fp['mimetype']
        f.mimepath = fp['mimepath']
        f.date = e.date_sent
        f.email = e
        f.save()
    
        # Create the FileToken so the Alias who crawled this can always access it (TODO: Eliminate most FileTokens)
        ft, created = FileToken.objects.get_or_create_token(alias, f, type=FILETOKEN_TYPE_CRAWLED)
  
    return e
    
    
    
