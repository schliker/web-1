from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
import pdb, traceback
from utils import *


class Plugin(Model):
    
    STATUS_ACTIVE = 'sACT'
    STATUS_BETA = 'sBET'
    STATUS_INACTIVE = 'sINA'
    
    STATUS_CHOICES = (
        (STATUS_ACTIVE, 'Active'),
        (STATUS_BETA, 'Beta'),
        (STATUS_INACTIVE, 'Inactive')
    )
    
    status =            models.CharField(max_length=4, choices=STATUS_CHOICES, null=True, blank=True)
    base_name =         models.CharField(max_length=32)                         # all lowercase, alphanumeric or underscores only
    name =              models.CharField(max_length=64, null=True, blank=True)  # Name for humans
    description =       models.TextField(null=True, blank=True)
    vendor =            models.CharField(max_length=64, null=True, blank=True)
    vendor_url =        models.URLField(null=True)
    target =            models.CharField(max_length=16, null=True, blank=True)  # only 'desktop' for now
    required_install =  models.BooleanField(default=False)                      # target app should automatically install this one
    date_created =      UTCDateTimeField(default=now)
    date_updated =      UTCDateTimeField(default=now)
            
    def get_svn_url(self):
        return settings.DESKTOP_PLUGIN_SVN_URL + self.base_name
    
    def get_absolute_url(self):
        "For the RSS feed"
        return '/download/connectors/%s' % self.base_name
    
    def get_latest_revision(self):
        revs = PluginRevision.objects.filter(plugin=self).order_by('-svn_revision')
        if revs.count() > 0:
            return revs[0]
        return None
    
    def as_json(self):
        return {
            'base_name':    self.base_name,
            'name':         self.name,
            'description':  self.description,
            'vendor':       self.vendor,
            'vendor_url':   self.vendor_url,
            'target':       self.target,
            'required_install': self.required_install   
        }
        
    def __unicode__(self):
        return u'<%s>' % self.name
    
    def __str__(self):
        return '%s' % self.name
    
class PluginRevision(Model):
    
    STATUS_ACTIVE = 'sACT'
    STATUS_BETA = 'sBET'
    STATUS_INACTIVE = 'sINA'            
    
    STATUS_CHOICES = (
        (STATUS_ACTIVE, 'Active'),
        (STATUS_BETA, 'Beta'),
        (STATUS_INACTIVE, 'Inactive')
    )
    
    slug =              models.SlugField(unique=True)
    status =            models.CharField(max_length=4, choices=STATUS_CHOICES, null=True, blank=True)
    plugin =            models.ForeignKey(Plugin)
    version =           models.CharField(max_length=16, null=True, blank=True)
    filename =          models.CharField(max_length=64, null=True, blank=True)
    icon_filename =     models.CharField(max_length=64, null=True, blank=True)
    description =       models.TextField(null=True, blank=True)  
    target_version_min = models.CharField(max_length=16, null=True, blank=True)
    target_version_max = models.CharField(max_length=16, null=True, blank=True)
    date_created =      UTCDateTimeField(default=now)
    svn_revision =      models.IntegerField(null=True, blank=True)

    def local_path(self):
        return os.path.join(settings.DESKTOP_PACKAGE_DIR, self.filename)
    
    def download_url(self):
        return '%s/%s' % (settings.DESKTOP_PACKAGE_URL, self.filename)

    def icon_url(self):
        return '%s/%s' % (settings.DESKTOP_PACKAGE_URL, self.icon_filename)

    def as_json(self):
        d = {
            'download_url':         self.download_url(),
            'icon_url':             self.icon_url(),
            'filename':             self.filename,
            'version':              self.version,
            'target_version_min':   self.target_version_min,
            'target_version_max':   self.target_version_max,
            'svn_revision':         self.svn_revision
        }
        d.update(self.plugin.as_json())
        return d
        
        
    def __unicode__(self):
        return '<%s ver: %s>' % (self.plugin.name, self.version)
    

admin.site.register(Plugin)
admin.site.register(PluginRevision)
