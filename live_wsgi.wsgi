#!/usr/bin/python
import os, sys, site

sys.stdout = sys.stderr

# thanks to http://74.125.155.132/search?q=cache:ZbpmQsuQ4yEJ:www.arlongpark.net/blog/deploying-django-mod-wsgi-virtualenv/+mod_wsgi+virtualenv&cd=3&hl=en&ct=clnk&gl=us
all_packages = None
try:
#	all_packagages = site.getsitepackages()
	print "skipping import attempt"
except:
	all_packages = None
#all_packages = None

prev_sys_path = list(sys.path)
sys.path.append('/home/calenvy/web')
sys.path.append('/home/calenvy/web/lib')
sys.path.append('/home/calenvy/web/src')

#for p in all_packagages:
#    sys.path.append(p)

# reorder sys.path so new directories from the addsitedir show up first
new_sys_path = [p for p in sys.path if p not in prev_sys_path]
for item in new_sys_path:
    sys.path.remove(item)
sys.path[:0] = new_sys_path

os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
os.environ['CELERY_LOADER'] = "django"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
