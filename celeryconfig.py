from detect_server_type import server_type

if server_type == 'live': 
    REDIS_HOST = BROKER_HOST = '10.176.32.205'
else:
    REDIS_HOST = BROKER_HOST = 'localhost'

CARROT_BACKEND = "ghettoq.taproot.Redis"        # use Redis as the Celery backend    
REDIS_PORT = BROKER_PORT = 6379
REDIS_DB = BROKER_VHOST = '/'
CELERY_RESULT_BACKEND = ''
CELERY_IMPORTS = ('crawl.tasks',)
CELERY_CONCURRENCY = 4        # Stick with default value for now
REDIS_PASSWORD = BROKER_PASSWORD = 'DKu47738d&#&@*dDSsjj$$$$'
CELERY_SEND_EVENTS = True       # for use with Celerymon

