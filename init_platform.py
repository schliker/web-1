print "Initializing Platform..."

import sys, os, traceback
from os.path import *

from detect_server_type import server_type

library_dir = False

project = dirname(abspath(__file__))
basepath = dirname(project)
            
if server_type == "live":
    import site
    os.chdir(basepath)
    os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
    
from django.core.management import setup_environ
try:
    conf = __import__('settings')
    setup_environ(conf)
except:
    print traceback.format_exc()
    print "init_platform.py: Can't set up configuration"
    sys.exit(1)