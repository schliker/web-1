#!/usr/bin/env python2.6

from init_platform import conf
from django.core.management import execute_manager
if __name__ == "__main__":   
    execute_manager(conf)