# THIS FILE IS DEPRECATED!
# Use the South migration scripts, assist/migrations to do new migrations



from assist.models import *
from assist import register
from assist import reminder
from utils import *

from django.template import Context, loader
from django.views.decorators.http import require_http_methods
from django.template import Context, loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.core import serializers
import hashlib
import re
from datetime import *
import random
import json
import traceback, pdb
from collections import defaultdict
from assist import parsedate
from assist import mail, mailthread

'''Migration scripts to convert stuff in the DB when we add or change fields,
   need to fix broken data introduced as result of a bug, etc.'''

# 2/6/09 (don't run this anymore)
def migrate_passwords():
    import keyczar
    crypter = keyczar.Crypter.Read(settings.KEYS_DIR)
    for i, account_user in enumerate(AccountUser.objects.all()):
        if account_user.server_password:
            print "Migrating ", i
            account_user.server_password_aes = crypter.Encrypt(account_user.server_password)
            account_user.save()


# 2/8/09 (don't run this anymore)
def migrate_assistants():
    l = []
    for i, account in enumerate(Account.objects.all()):
        print "Converting account %d: %s" % (i, repr(account))
        admins = list(Alias.objects.active().filter(account = account, is_admin = True))
        if not admins:
            print "Warning: account has no admins, can't convert"
        else:
            assistant = admins[0].most_recent_assistant
            l.append((account.base_url, assistant.email))
    print l
    
# 2/16/09 (don't run this anymore)
def migrate_aliashash():
    aliases = Alias.objects.all()
    for a in aliases:
        if not a.hash:
            a.hash = hashlib.md5(str(random.getrandbits(200))).hexdigest()
            a.save()
        
# 2/20/09 (don't run this anymore)
def migrate_delete_eventrelation_dups(do_it=False):
    '''Delete duplicate tags in the DB'''
    all_delete = []
    for e in Event.objects.all():
        erer = EventRelation.objects.filter(event=e)
        erer_asst = EventRelation.objects.filter(event=e, distinction='assistant')
        if erer_asst.count() > 1:
            print "-----------------"
            print "Event %s has %d assistants and %d ERs" % (str(e), erer_asst.count(), erer.count())
            tups = []
            to_delete = []
            for er in erer:
                tup = (er.distinction, er.content_type, er.object_id)
                if tup in tups:
                    to_delete.append(er.pk)
                else:
                    tups.append(tup)
            all_delete.extend(to_delete)

    for pk in all_delete:
        er = EventRelation.objects.get(pk=pk)
        if do_it:
            print "Disabling EventRelation:", er
            er.distinction = '~' + er.distinction
            er.save()
        else:
            print "Would disable EventRelation:", er

# 2/22/09 (don't run this again)
def migrate_aliasscores():
    for alias_score in AliasScore.objects.all():
        # See if there's already a ContactRelation by that name
        for alias in Alias.objects.filter(account_user=alias_score.account_user_from):
            cr, created = ContactRelation.objects.get_or_create(alias=alias, contact=alias_score.alias_to)
            cr.score += alias_score.score
            cr.date_updated = now() - ONE_DAY
            cr.save()

# 2/23/09 (don't run this again)
def migrate_eventtitles():
    for e in Event.objects.all():
        e.title_long = e.title
        e.save()

# 2/24/09 (don't run this again)
def migrate_aliasscores_2():
    for cr in ContactRelation.objects.filter(score=None):
        cr.score = 0
        cr.save()

# 2/25/09 (don't run this again)
def migrate_ez_names(force=False):    
    for account in Account.objects.all():
        aliases = list(Alias.objects.active().filter(account=account).order_by('date_created'))
        for i in range(len(aliases)):
            a = aliases[i]
            if (not a.ez_name) or force:
                ez = a.calc_ez_name(aliases)
                a.ez_name = ez
                a.save()

# 2/26/09 (don't run this again)
def migrate_fix_ez_names(do_it=False):
    aliases = list(Alias.objects.active())
    for a in aliases:
        if a.ez_name:
            fixed_name = ''.join([ch for ch in a.ez_name if ch in EZ_NAME_ALLOWED_CHARS])
            print "Before: %s   After: %s" % (a.ez_name, fixed_name)
            if do_it:
                a.ez_name = fixed_name
                a.save()

# 2/28/09  (don't run this again)            
def migrate_eventresponse_slugs():
    for er in EventResponse.objects.all():
        if er.slug is None:
            er.slug = create_slug(length=10)
            er.save()
    
# 2/28/09 (don't run this again)
def migrate_message_ids():
    for e in Email.objects.all():
        if e.message_id is None:
            e.message_id = mail.make_msgid()
            e.save()
            
# 3/5/09 (don't run this again)
def migrate_tags():
    for t in Tag.objects.all():
        if t.text:
            if t.text[0] == '@':
                newtext = t.text[1:]
                queryset_tag = Tag.objects.filter(text=newtext)
                if queryset_tag.count() == 0:
                    # just change the tag in place
                    t.text = newtext
                    t.save()
                elif queryset_tag.count() == 1:
                    newtag = queryset_tag.get()
                    # Change all refs to the old tag to the new tag
                    queryset_er = EventRelation.objects.filter(distinction="tag", object_id=t.pk)
                    for er in queryset_er:
                        er.content_object=newtag
                        er.save()
                    queryset_er = EventRelation.objects.filter(distinction="tag_private", object_id=t.pk)
                    for er in queryset_er:
                        er.content_object=newtag
                        er.save()
                    t.text = "~" + newtext
                    t.save()
                else:
                    log("Warning: more than one tag with text %s" % newtext)

# 3/5/09 (don't run this again)
def migrate_first_last_names(do_it = False):
    for a in Alias.objects.all():
        if (a.first_name.startswith("=?")):
            log("Skipping %s" % a.first_name)
            continue
        if (not a.first_name) and (not a.last_name):
            continue
        old_first_name, old_last_name = a.first_name, a.last_name
        a.first_name = re.sub('^(\\s|\'|\")*', '', a.first_name)
        a.last_name = re.sub('(\\s|\'|\")*$', '', a.last_name)
        a.first_name = a.first_name.title()
        a.last_name = a.last_name.title() 
        if ',' in a.first_name:
            a.first_name = a.first_name.replace(',', '').strip()
            if a.last_name and ')' not in a.last_name:
                a.first_name, a.last_name = a.last_name, a.first_name
        if ')' in a.last_name:
            a.last_name = ''
        if '}' in a.last_name:
            a.last_name = ''
        a.first_name = a.first_name.strip().title()
        a.last_name = a.last_name.strip().title() 
        if (a.first_name != old_first_name) or (a.last_name != old_last_name):
            log("Changing pk %d: %s %s --> %s %s" % (a.pk, old_first_name, old_last_name, a.first_name, a.last_name))
            if do_it:
                a.save()
        
# 3/6/09 (don't run this again)
def migrate_questiononly_crawled_events(do_it = False):
    
    note = Tag.objects.get(text='note')
    
    queryset = Email.objects.filter(is_crawled=True).filter(event__status=EVENT_STATUS_TAGONLY)
    for e in queryset:
        event = e.event
        er = EventRelation.objects.filter(event=event, distinction='tag')
        er2 = EventRelation.objects.filter(event=event, distinction='tag_private')
        if (er.count() == 1 and er.get().content_object == note) or (er2.count() == 1 and er2.get().content_object == note):
            if ('?' in event.title_long) and not ('@note' in e.message):
                alias_creator = event.get_unique_object(distinction="alias_creator")
                log("Found event: ", event.pk, event, alias_creator)
                if do_it:
                    event.status=EVENT_STATUS_PARSEFAILED
                    event.save()

# 3/7/09 (this is just informational)
def migrate_incorrect_email_from_alias(do_it = False):
    for e in Email.objects.all():
        if e.from_alias and e.from_alias.account:
            from_account = e.from_alias.account
            extras = e.get_extras()
            if extras and extras['tocc_noted']:
                files = File.objects.filter(email=e)
                if files.count() > 0:
                    noted_domain = ''.join(extras['tocc_noted'][0][1].split('@')[-1].split('.')[:-2])
                    if e.from_alias.account.base_url != noted_domain:
                        log ("Problem: email %d %s, from_account=%s, noted_domain=%s" %\
                                (e.pk, str(e), str(from_account), noted_domain))

                
                
# 3/11/09 (don't do this anymore)
def migrate_file_collection_dates(do_it = False):
    for fc in FileCollection.objects.all():
        fc.date_created = fc.get_first_date()
        fc.date_created = fc.get_last_date()
        if do_it:
            fc.save()

# 3/17/09
def migrate_remove_null_eventrelations(do_it = False):
    count = EventRelation.objects.count()
    i = 0
    for er in EventRelation.objects.all():
        if (i % 100) == 0:
            log("Fixing EventRelation %d" % i)
        if er.content_object is None:
            log("Found broken EventRelation", er.pk, er.event, er.distinction)
            event = er.event
            if ('creator' in er.distinction) and event:
                log("Neutering event: pk=%d, %s" % (event.pk, str(event)))
                event.status = EVENT_STATUS_PARSEFAILED
                if do_it: event.save()
            if do_it: er.delete()
        i += 1
        
