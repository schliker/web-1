{% load i18n %}
{% load account_tags %}
{% minifyjs %}

/* 
 * ---------------------------------------------------------------
 * Title: 		Calenvy js for injection into Gmail
 * Author: 		Calenvy
 * URL: 		http://calenvy.com
 * 
 * Description:	This gets injected into any Gmail page upon loading
 * 
 * Created:		Monday 3/29/2010 @ library
 * 
 * ---------------------------------------------------------------
 */ 

/*
 * "mini" Selector Engine
 * Copyright (c) 2009 James Padolsey
 * -------------------------------------------------------
 * Dual licensed under the MIT and GPL licenses.
 *    - http://www.opensource.org/licenses/mit-license.php
 *    - http://www.gnu.org/copyleft/gpl.html
 * -------------------------------------------------------
 * Version: 0.01 (BETA)
 */

var CalenvySelector=(function(){var b=/(?:[\w\-\\.#]+)+(?:\[\w+?=([\'"])?(?:\\\1|.)+?\1\])?|\*|>/ig,g=/^(?:[\w\-_]+)?\.([\w\-_]+)/,f=/^(?:[\w\-_]+)?#([\w\-_]+)/,j=/^([\w\*\-_]+)/,h=[null,null];function d(o,m){m=m||document;var k=/^[\w\-_#]+$/.test(o);if(!k&&m.querySelectorAll){return c(m.querySelectorAll(o))}if(o.indexOf(",")>-1){var v=o.split(/,/g),t=[],s=0,r=v.length;for(;s<r;++s){t=t.concat(d(v[s],m))}return e(t)}var p=o.match(b),n=p.pop(),l=(n.match(f)||h)[1],u=!l&&(n.match(g)||h)[1],w=!l&&(n.match(j)||h)[1],q;if(u&&!w&&m.getElementsByClassName){q=c(m.getElementsByClassName(u))}else{q=!l&&c(m.getElementsByTagName(w||"*"));if(u){q=i(q,"className",RegExp("(^|\\s)"+u+"(\\s|$)"))}if(l){var x=m.getElementById(l);return x?[x]:[]}}return p[0]&&q[0]?a(p,q):q}function c(o){try{return Array.prototype.slice.call(o)}catch(n){var l=[],m=0,k=o.length;for(;m<k;++m){l[m]=o[m]}return l}}function a(w,p,n){var q=w.pop();if(q===">"){return a(w,p,true)}var s=[],k=-1,l=(q.match(f)||h)[1],t=!l&&(q.match(g)||h)[1],v=!l&&(q.match(j)||h)[1],u=-1,m,x,o;v=v&&v.toLowerCase();while((m=p[++u])){x=m.parentNode;do{o=!v||v==="*"||v===x.nodeName.toLowerCase();o=o&&(!l||x.id===l);o=o&&(!t||RegExp("(^|\\s)"+t+"(\\s|$)").test(x.className));if(n||o){break}}while((x=x.parentNode));if(o){s[++k]=m}}return w[0]&&s[0]?a(w,s):s}var e=(function(){var k=+new Date();var l=(function(){var m=1;return function(p){var o=p[k],n=m++;if(!o){p[k]=n;return true}return false}})();return function(m){var s=m.length,n=[],q=-1,o=0,p;for(;o<s;++o){p=m[o];if(l(p)){n[++q]=p}}k+=1;return n}})();function i(q,k,p){var m=-1,o,n=-1,l=[];while((o=q[++m])){if(p.test(o[k])){l[++n]=o}}return l}return d})();

/*
 * JSON stringifier and parser
 * From http://www.json.org/json2.js
 */
if(!this.CalenvyJSON){this.CalenvyJSON={};}(function(){function f(n){return n<10?'0'+n:n;}if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+f(this.getUTCMonth()+1)+'-'+f(this.getUTCDate())+'T'+f(this.getUTCHours())+':'+f(this.getUTCMinutes())+':'+f(this.getUTCSeconds())+'Z':null;};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf();};}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);})+'"':'"'+string+'"';}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}if(typeof rep==='function'){value=rep.call(holder,key,value);}switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){k=rep[i];if(typeof k==='string'){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}if(typeof CalenvyJSON.stringify!=='function'){CalenvyJSON.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('CalenvyJSON.stringify');}return str('',{'':value});};}if(typeof CalenvyJSON.parse!=='function'){CalenvyJSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}return reviver.call(holder,key,value);}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);});}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}throw new SyntaxError('CalenvyJSON.parse');};}}());

var CalenvyGlobal = {
	load_js: function (doc, url, id, callback, reload) {
	
		/* Summary:		load a specific js file into the document remotely. 
		 * 				Do nothing if that js has already been loaded.
		 * 				Append 'rand='+number to avoid caching problems.
		 * 
		 * Params: 		doc - the Document object into which to load the js
		 * 				url - the URL of the js we wanted to 
		 * 				id (optional) the ID of the script we want to inject
		 * 					if set, check for its existence before injecting script again
		 * 				callback (optional) - function to call when the load is done
		 * 					This function is an event handler and takes an Event object as its parameter
		 * 				reload (optional) - if the script is already loaded (the id is there),
		 * 					reload it (default: false)
		 * Return: 
		 *  
		 */
		
		// Fill in default values
		if (typeof(id) == 'undefined') id = null;
		if (typeof(callback) == 'undefined') callback = null;
		if (typeof(reload) == 'undefined') reload = false;
		
		var oldElement = (id != null) ? doc.getElementById(id) : null;
		
		if ((oldElement != null) && reload) {
			oldElement.parentNode.removeChild(oldElement);
			oldElement = null;
		}
		
		if (oldElement == null) {
			var head = doc.getElementsByTagName('head')[0];
			var script = doc.createElement('script');
			var url = url + ((url.indexOf('?') == -1) ? '?' : '&') + 'rand=' + new Date().getTime();
			script.setAttribute('src', url);
			if (id != null)
				script.setAttribute('id', id);
			if (callback != null)
				script.addEventListener('load', callback, false);
			head.appendChild(script);
		} else if (callback != null) {
			// Script already loaded, just call the callback immediately
			callback(null);
		}
	},
	
	insert_css: function() {
		
	}

}

var CalenvyPage = {
			
	BASE_URL: 'http{% if has_ssl %}s{% endif %}://{{site.domain}}{{url_prefix}}',
	
	chrome: /Chrome/.test(navigator.userAgent),				// True if Chrome, False if Firefox
	current_doc: null,										// Document to look for contacts and inject the sidebar
	script_html_doc: null,									// Document to append <script> tags to (for loading HTML into sidebar)
	script_js_doc: null,									// Document to append <script> tags to (for loading javascript)
	current_sidebar: null,
	interval_handle: null,									// handle of the setInterval, in case we need to clear it later
	js_ready: false,										// whether all the javascript has finished loading
	sidebar_ready: false,									// whether main_account.html has started loading the widgets
	/*
	 * Display a status message at the top
	 */
	display_status: function(html, doc) {
		var status = doc.getElementById('calenvy_status');
		if (status == null) {
			var self_email = CalenvySelector('span#gbi4m1', doc);
			if (self_email.length > 0) {
				self_email = self_email[0];
			
				// Insert our product name before the user's name
				var status = doc.createElement('span');
				status.id = 'calenvy_status';
				status.innerHTML = html + ' | ';
				self_email.parentNode.insertBefore(status, self_email);
			}
		} else {
			status.innerHTML = html + ' | ';
		}
	},
	
	/*
	 * Determine the gmail user's email address
	 * Returns null if it can't find it
	 */
	get_self_email: function(doc) {
		var self_email = null;
		
		// First try to get the email address from a certain node
		var email_nodes = CalenvySelector('span#guser > nobr > b', doc);
		
		// if at first we don't succeed, try and try again.
		if (email_nodes.length == 0) {
			var email_nodes = CalenvySelector('span#gbps2', doc);
		}
		
		// if at first we don't succeed, try and try again.
		if (email_nodes.length == 0) {
			var email_nodes = CalenvySelector('span#gbi4m1', doc);
		}
		
		// grab the value!
		if (email_nodes.length > 0) {
			self_email = email_nodes[0].firstChild.nodeValue;
		}
				
		// If that didn't work, try to get it from the title
		if (typeof(self_email) != 'string') {
			// title is something like "Foo, Inc. Mail - Subject Line - mary.jones@foo.com"
			var match_results = window.top.document.title.match(/ - [^ ]+@[^ ]+ *$/);
			if (match_results != null) {
				// strip off the " - " from the front and any extra spaces from the end
				self_email = match_results[0].replace(/^\s*-\s+/, "").replace(/\s+$/, "").toLowerCase();
			}
		}
		
		if (typeof(self_email) != 'string') {
			self_email = null;
		}
		
		return self_email;
		
	},
	
	/*
	 * Add an email/name to a list, but only if it's not already in the list
	 *  (keep the list unique)
	 *  
	 *  span: the DOM element <span> to parse, containing a name and an email address
	 *  self_email: the user's Gmail email address (if the span has this email address,
	 *  	ignore it)
	 *  contact_list: the list of contacts to append to. 
	 */
	add_to_contact_list: function(span, self_email, contact_list) {

		var email = span.getAttribute("email");
		var name = span.firstChild.data;
				
		if (email == null) return email;
		if (email == self_email) return email;		
		for (var i=0; i < contact_list.length; i++) {
			if (contact_list[i].email == email) return email;
		}
		
		contact_list.push({
			name:	name,
			email:	email
		});
		
		return email;
	},
	
	/*
	 * get the list of unique contacts for the currently viewing thread
	 *  Each contact is an Object with properties 'name', 'email'
	 *  
	 *  self_email: user's own email address -- avoid grabbing contacts with this email address
	 *  doc: the document of the iframe from which we extract contacts
	 */
	get_contact_list: function(self_email, doc) {
		var contact_list = [];
		var i;
		var email_includes_self = false;
		
		// Grab email addresses from old mails in the thread
		// td.gF > span
		var old_spans = CalenvySelector('span.gD', doc);
		for (i = 0; i < old_spans.length; i++) {
			if (CalenvyPage.add_to_contact_list(old_spans[i], self_email, contact_list) == self_email)
				email_includes_self = true;
		}		
				
		// Grab email addresses from the latest email in the thread
		// div.iw > * > span
		var latest_spans = CalenvySelector('span.g2', doc);
		for (i = 0; i < latest_spans.length; i++) {
			if (CalenvyPage.add_to_contact_list(latest_spans[i], self_email, contact_list) == self_email)
				email_includes_self = true;
		}	
		
		// If our own email address is in the email, add it, but to the end of the list
		//  (a last resort)
		if (email_includes_self) {
			contact_list.push({
				name:	'',
				email:	self_email
			});
		}
		
		return contact_list;
	},
		
	/*
	 * load_sidebar_js
	 * 
	 * Start loading a sequence of .js files needed for the sidebar.
	 * .js are loaded in order by using the load_js callback mechanism.
	 * Set js_ready to true when we're done loading everything.
	 */
	
	load_sidebar_js: function() {
		CalenvyPage.js_ready = false;
		CalenvyGlobal.load_js(CalenvyPage.script_js_doc, 'https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', 'calenvy_jquery', CalenvyPage.load_sidebar_js_2);
	},
	
	load_sidebar_js_2: function() {
		CalenvyGlobal.load_js(CalenvyPage.script_js_doc, CalenvyPage.BASE_URL + '/dash/main_javascript.js?agent=firefox', null, CalenvyPage.load_sidebar_js_3);
	},

	load_sidebar_js_3: function() {
		CalenvyGlobal.load_js(CalenvyPage.script_js_doc, CalenvyPage.BASE_URL + '/dash/main_account.js?agent=firefox', null, CalenvyPage.load_sidebar_js_done);
	},
		
	load_sidebar_js_done: function() {
		CalenvyPage.js_ready = true;
		CalenvyPage.fill_sidebar();				// Load the sidebar itself
	},
	
	/*
	 * Event handler for the hidden button, reloads the sidebar.
	 */
	reload_sidebar: function(e) {
		CalenvyPage.fill_sidebar();		// reload the sidebar
	},
		
	/*
	 * Navigate the sidebar to the given URL
	 * If URL not given, use /dashboard/ (assumes user is already logged in)
	 * Handle redirects as necessary
	 */
	fill_sidebar: function(url) {
		
		// Navigate the sidebar to the given URL
		if (typeof(url) == 'undefined')
			url = '/dashboard/';
		
		// Grab the user's own email address, so we can exclude this from the contact list
		var self_email = CalenvyPage.get_self_email(CalenvyPage.current_doc);
		var contact_list = CalenvyPage.get_contact_list(self_email, CalenvyPage.current_doc);
		
		// Use JSONP to get the page
		//var contact_string = CalenvyJSON.stringify(contact_list);
		
		if (!(/^https?:\/\//.test(url))) {
			url = CalenvyPage.BASE_URL + url;
		}
		
		url = url + '?user_email=' + encodeURIComponent(self_email);
		if (contact_list.length > 0) {
			url += '&contact_email=' + encodeURIComponent(contact_list[0].email);
			url += '&contact_name=' + encodeURIComponent(contact_list[0].name);
		} else {
			url += '&contact_email=&contact_name=';
		}
		url += '&agent=firefox&callback=CalenvyPage.fill_sidebar_callback';
					
		CalenvyGlobal.load_js(CalenvyPage.script_html_doc, url, null); // TODO: Auto-popup to login when gmail loads
		
		// If user_email is not the currently logged-in user, this page will return a 302 redirect
		//  and we'll end up on the OpenID login page.
	},
	
	fill_sidebar_callback: function(data) {
				
		if (data.status_code == 200) {
			CalenvyPage.current_sidebar.innerHTML = data.content;
			CalenvyPage.sidebar_ready = false;						// We just reloaded the sidebar

			// Some pages have a hidden button they can click to tell us to reload the sidebar
			//  Add the click handler for this button
			var reload_sidebar_button = CalenvyPage.current_doc.getElementById('calenvy_reload_sidebar_button');
			if (reload_sidebar_button != null) {
				reload_sidebar_button.onclick = CalenvyPage.reload_sidebar;
			}
			
			// Tell the login form what our Google email address is (if we're on the login form)
			var login_email = CalenvyPage.current_doc.getElementById('login_email');
			if (login_email != null) {
				var email = CalenvyPage.get_self_email(CalenvyPage.current_doc);
				login_email.value = email;
			}		
			
			CalenvyPage.load_widgets_if_needed();
			
		} else if (data.status_code == 301 || data.status_code == 302) {	// redirect
			CalenvyPage.fill_sidebar(data.next);
		} else {
			alert("error: " + data.status_code);
		}
	},
	
	load_widgets_if_needed: function() {
		// Called when the sidebar and javascript are finished loading,
		//  attempts to call the ready() function to start the AJAX call that loads the Calenvy widgets
		if (!CalenvyPage.sidebar_ready && CalenvyPage.js_ready) {
			var ready_button = CalenvyPage.current_doc.getElementById('ready_button');
			if (ready_button != null) {
				// Tell the document to start loading the widgets
				CalenvyPage.sidebar_ready = true;
				ready_button.click();
			}
		}
	},	
	
	/*
	 * interval_handler
	 * 
	 * For Firefox, this gets called every 50ms, for each of the 6 (or so) Document objects comprising gmail.
	 * For Chrome, only gets called every 50ms for the top-level document.
	 * 
	 * If the sidebar isn't already inserted and the sidebar insertion point exists, insert the sidebar.
	 */
	interval_handler: function() {
		
		if (CalenvyPage.chrome) {
			// Chrome
			try {
				var canvas = window.top.document.getElementById('canvas_frame').contentDocument;
				CalenvyPage.current_doc = canvas;
				CalenvyPage.script_js_doc = canvas;
				CalenvyPage.script_html_doc = document; //used to be document
			} catch (err) { 		// Silently catch errors (if canvas frame not loaded yet).
				return;
			}
			
			
		} else {
			// Firefox
			try {
				var canvas_element = window.top.document.getElementById("canvas_frame");
				if (canvas_element == null || canvas_element.contentWindow != window) {
					return;
		        }
				var canvas = canvas_element.contentWindow.document;
				
				CalenvyPage.current_doc = canvas;
				CalenvyPage.script_js_doc = canvas;
				CalenvyPage.script_html_doc = canvas;
				
			} catch (err) {
				return; 	//if canvas not loaded, fail nicely.
			}
			
		}
		
		CalenvyPage.display_status('<i>{{settings.PRODUCT_NAME}}</i> {% trans "loaded" %} <input style="font-size: .75em; display: none;" type="button" value="{% trans "Login" %}"/>', canvas);

		var replacement = CalenvySelector('#calenvy_side', canvas);	// If we've already inserted the calenvy robot div, don't do it again
		if (replacement.length == 0) {
			
			/* Code to replace ads
			var div_before_sidebar = CalenvySelector('div.Pj', canvas);	// This comes right before the sidebar in the thread view
			if (div_before_sidebar.length > 0) {							// This is only true for one of the several Documents making up Gmail
				
				div_before_sidebar = div_before_sidebar[0];					// We're replacing the nodes after this one
				
				// Delete all sibling nodes that come after the div_before_sidebar
				var parent = div_before_sidebar.parentNode;
						
				while ((parent.lastChild != div_before_sidebar) && (parent.lastChild != null)) {
					parent.removeChild(parent.lastChild);
				}
								
				// Append our sidebar 
				var side = document.createElement('div');
				side.setAttribute('id', 'calenvy_side');
							
				CalenvyPage.current_sidebar = side;
				parent.appendChild(side);
				CalenvyPage.load_sidebar_js();						// Load the Javascript needed for the sidebar, this only happens once
				CalenvyPage.fill_sidebar();							// Load the sidebar itself, this can happen again later if the sidebar "reloads" itself
				
			}*/
			
			CalenvyPage.js_ready = false;
			CalenvyPage.sidebar_ready = false;					// Sidebar is just now loading again ...

			var tools_div = CalenvySelector('div.u5', canvas);	// This comes right before the sidebar in the thread view
			if (tools_div.length > 0) {							// This is only true for one of the several Documents making up Gmail
														
				tools_div = tools_div[0];					// We're replacing the nodes after this one
				tools_div.innerHTML = '';
				
				tools_div.style.display = 'none';				// Hide the nav tools ("Print", etc.) since they take up space				

				// Append our sidebar
				var parent = tools_div.parentNode;
				var side = document.createElement('div');
				side.setAttribute('id', 'calenvy_side');
							
				CalenvyPage.current_sidebar = side;
				parent.insertBefore(side, tools_div.nextSibling);	// Insert the sidebar after tools_div
																	//  (this works even if tools_div.nextSibling is null)
				
				CalenvyPage.load_sidebar_js();			// Load the Javascript needed for the sidebar, this only happens once
														//  This calls fill_sidebar when it's done
			}
			
		} else {
			// Sidebar already exists -- start loading the widgets, if all Javascript has been loaded
			CalenvyPage.load_widgets_if_needed();
		}
	},
		
	init: function() {
		CalenvyPage.interval_handle = setInterval(CalenvyPage.interval_handler, 200);
	}
		
}

CalenvyPage.init();
{% endminifyjs %}
