"""
Javascript embed middleware.

This module provides a middleware that allows any page to be accessed with 
the GET parameter "format=js", causing the page to return a JavaScript file
that contains the HTML embedded as part of a JSON structure.

This allows any page on the site to be embedded, cross-site, into things like
our Firefox/Chrome plugin. To do this, javascript appends a <script> tag to the
head of the current page.

"""

import re
import itertools
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.3, 2.4 fallback.

from django.conf import settings
from django.http import *
from django.utils.safestring import mark_safe
import hashlib  
from utils import *                             # ADDED [eg]                     


_JSON_TYPES = ('application/json/')

_HTML_TYPES = ('text/html', 'application/xhtml+xml')


class JSONPMiddleware(object):
    """
    Middleware that post-processes a response to format it as Javascript,
     if "callback" is specified among the parameters
    """
        
    def process_response(self, request, response):
                        
        if 'callback' in request.REQUEST:       # the JSONP callback function
            
            content_type = response['Content-Type'].split(';')[0]
            
            if content_type in _JSON_TYPES:
                # Result of an AJAX call, just wrap the JSON directly
                content = response.content
                
            elif content_type in _HTML_TYPES:
                # Encapsulate the HTML page in a JSON structure
                                
                log("JSONPMiddleware: returning %d %s" % (response.status_code, request.path))
                content = json.dumps({
                    'status_code':  response.status_code,
                    'content':      response.content,
                    'next':         response['Location'] if 'Location' in response else None    # redirect to here
                })
                
            else:
                # Don't support XML, other stuff yet
                raise NotImplementedError
                  
            result = '%s(%s)' % (request.REQUEST['callback'], content)
            return HttpResponse(result)
        
        else:
            return response
    

_SRC_URL_RE = \
    re.compile(r'(?P<attr>src)\s*=\s*(\'|")(?!(javascript|file|mailto))(?P<url>[^\'"]+)(\'|")')
    
# Matcher for HREF urls -
#  don't match javascript: urls because those might contain quote characters, which are tricky
#  (and we're not processing those anyway)
_HREF_URL_RE = \
    re.compile(r'(?P<attr>href)\s*=\s*(\'|")(?!(javascript|file|mailto))(?P<url>[^\'"]+)(\'|")')
    
_CSS_URL_RE = \
    re.compile(r'(?P<attr>url)\((?P<url>[^)]+)\)')
    
class URLMiddleware(object):
    """
    Middleware that post-processes a response to change URL links so that, as necessary:
        - they include the '/followup' prefix 
        - they are absolute URLs
        - they propagate the ?agent=whatever
        - href links call the embedding Javascript to change the page
    This needs to come after EmbedMiddleware in the settings!
    """
    
    def modify_html(self, html, request, make_urls_absolute):
        """
        Helper function for process_response that takes HTML and modifies it as needed.
        Can be used by itself, for code that just needs this function without the middleware
        (for example, widgets returned by ajax_feeds using AJAX)"""

        site = get_site(request) if make_urls_absolute else None
        agent = get_agent(request)
        
        def escapequotes(s):
            # Some HREF links (Javascript: urls) have quotes in them, escape them
            return s.replace('"', '\\"')
        
        def _get_absolute_url(url, request):
            # First, if it's a relative url, make it relative to the root of the site
            if (url != '#') and not re.match(r'(?i)[a-z]+:', url):
            
                if not url.startswith('/'):
                    url = request.path + url
                                
                url = 'http%s://%s%s' % ('s' if site.domain in settings.SSL_DOMAINS else '', site.domain, url)
                
            return url

        def modify_href_url(match):
            """Modifies the href attribute (if any) of the <a> tag,
            but only if it's an absolute URL on our own site"""
                 
            url = match.group('url')
                            
            #url = get_prefixed_url(url, request)        # this only prefixes the url if necessary
            if make_urls_absolute:
                url = _get_absolute_url(url, request)
            
            # Need to surround the url with single quotes, not double quotes --
            #  some urls (like the ones returned by the personlink tag in account_tags.py) include double quotes in them
            result = "%s='%s'" % (match.group('attr'), url)
                        
            return result

        def modify_src_url(match):
            """Modifies the href attribute (if any) of the <a> tag,
            but only if it's an absolute URL on our own site"""
                            
            url = match.group('url')
                            
            #url = get_prefixed_url(url, request)        # this only prefixes the url if necessary
            if make_urls_absolute:
                url = _get_absolute_url(url, request)
                            
            result = "%s='%s'" % (match.group('attr'), url)
            return result
        
        def modify_css_url(match):
            """Modifies the href attribute (if any) of the <a> tag,
            but only if it's an absolute URL on our own site"""
                
            url = match.group('url')
            #url = get_prefixed_url(url, request)        # this only prefixes the url if necessary
            if make_urls_absolute:
                url = _get_absolute_url(url, request)
                      
            result = '%s(%s)' % (match.group('attr'), url)
            return result        
        
        if make_urls_absolute:
            # For now, all transformations we do only do anything if make_urls_absolute is True -
            # thus, bail out early if it's not true [eg 3/31/10]
            html = _SRC_URL_RE.sub(modify_src_url, html)
            html = _HREF_URL_RE.sub(modify_href_url, html)
            html = _CSS_URL_RE.sub(modify_css_url, html)
            
        return html
        
    def process_response(self, request, response):
                
        if response['Content-Type'].split(';')[0] in _HTML_TYPES:

            make_urls_absolute = ('callback' in request.REQUEST)        # JSONP cross-site request, make everything absolute
                        
            html = unicode(response.content, 'utf-8')     
            html = self.modify_html(html, request, make_urls_absolute)
            response.content = html.encode('utf-8')

        return response
