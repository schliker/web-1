from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
import traceback, pdb, pprint
from django import forms
from django.contrib.syndication.feeds import Feed
from utils import *


def gmail_view(request):
    ''' 
    This is the gmail.js file loaded by our Firefox and Chrome extensions
    '''
        
    log("gmail_view: init")
    
    try:
        t = loader.get_template('browser/gmail.js')
        c = RequestContext(request)
        
        return HttpResponse(t.render(c))    
        
    except:
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')

