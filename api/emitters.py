from piston.emitters import Emitter
from django.utils.xmlutils import SimplerXMLGenerator
import StringIO, string
from django.utils.encoding import *
from utils import *
import pdb, traceback

# Like Piston's XMLEmitter, except:
#  * it can also emit CDATA 
#        (used automatically when strings contain any characters that wouldn't appear in an email address)
#  * it can set attributes for XML tags
#        (pass a 2-tuple as the value for the tag, with the first parameter being a dict of attributes)


class XMLCDATAEmitter(Emitter):
      
    def _to_xml(self, xml, data):
        GOODCHARS = string.letters + string.digits + '.@-+_'

        if isinstance(data, list):
            for item in data:
                self._to_xml(xml, item)
        elif isinstance(data, dict):
            for key, value in data.iteritems():
                if isinstance(value, list) and len(value)==2 and isinstance(value[0], dict) and value[0].get('ATTRS', False):
                    xml.startElement(key, dict( [(k, str(v or '')) for k, v in value[0].items() if k != 'ATTRS'] ))
                    self._to_xml(xml, value[1])
                else:
                    xml.startElement(key, {})
                    self._to_xml(xml, value)                    
                xml.endElement(key)
        elif isinstance(data, (str, unicode)) and any([c not in GOODCHARS for c in data]):
            xml.ignorableWhitespace(encode_cdata(data))
        elif data is None:
            pass
        else:
            xml.characters(smart_unicode(data))

    def render_data(self, data, enclose_tag="response"):
        """"Not a method of standard Piston emitters, but used by us to render something into XML"""
        
        stream = StringIO.StringIO()
        
        xml = SimplerXMLGenerator(stream, "utf-8")
        xml.startDocument()
        xml.startElement(enclose_tag, {})
                
        self._to_xml(xml, data)
        
        xml.endElement(enclose_tag)
        xml.endDocument()
        
        return stream.getvalue()
    
    def render(self, request):
        return self.render_data(self.construct())


# This always needs to run, so we import this from the outermost urls.py
try:
    Emitter.register('xml_cdata', XMLCDATAEmitter, 'text/xml; charset=utf-8')
except:
    log(traceback.format_exc())
    log('WARNING: Could not register the XML emitter for Piston API classes')

    