import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from remote.models import *
from django.utils.translation import ugettext as _
from piston.emitters import Emitter
from django.utils.xmlutils import SimplerXMLGenerator
from django.http import HttpRequest
from xml.dom import minidom


class FollowupHandler(BaseHandler):
    """A handler that does the following:
        - forwards followup requests to a followup server (with campaign number from our stored EmailRule table)
        - unmarks any failed Salesforce synced emails, if a lead/contact was just created with that email address
        
        Salesforce package calls this on the "after insert" of a Lead/Contact.
        Eventually we may handle "after update" as well.
    """
    
    allowed_methods = ('POST')

    def _parse_person_xml(self, node):
        result = {
            'email':        '',
            'first_name':   '',
            'last_name':    '',
            'company':      '',
            'source':       '',
            'full_name':    ''
        }
        
        for node in node.childNodes:
            if node.nodeType == node.ELEMENT_NODE:
                if node.nodeName in ['email', 'first_name', 'last_name', 'company', 'source']:
                    text = get_xml_text(node)
                    
                    # Strip out garbage from names; avoid placeholder names like '*'
                    if node.nodeName in ['first_name', 'last_name']:
                        text = strip_nonalpha(text)
                    else:
                        if len(strip_nonalpha(text)) == 0:
                            text = ''
                    
                    result[node.nodeName] = text
        
        result['full_name'] = ' '.join([result['first_name'], result['last_name']]).strip()                
        return result
    
    def _is_valid_person(self, lead):
        """Don't process leads that are the result of Calenvy creating leads on Salesforce"""
        for r in settings.LEADSOURCE_IGNORE_PATTERNS:
            if re.match(r, lead['source']):
                return False
            
        # For now, we require email addresses for leads
        if not lead.get('email'):
            return False
        
        return True
    
         
    def create(self, request):
        try:                        
            alias = request.session['alias']
            account = alias.account
            remote_user = request.session['remote_user']  
            n = now()
            
            log("FollowupHandler.create: trying to forward: alias=", alias, "remote_user.pk=", remote_user.pk)
            log("raw_post_data=", request.raw_post_data)
            account = request.session['remote_user'].account
                        
            data = request.raw_post_data
            if request.META.get('HTTP_CONTENT_ENCODING') == 'gzip':
                log("MessagesHandler.create: decoding gzip")
                data = zlib.decompress(data)
            
            log(data)
            
            doc = minidom.parseString(data)
            
            outer_node = doc.firstChild
            if outer_node.nodeName != 'followup':
                raise Exception()
            
            # Parse all the followup fields -- but the only field we care about is email (for now) --
            #  the campaign number that we forward to the followup server comes from our stored EmailRule table,
            #  not from this XML
            
            campaign_id, template_id, step_num, sender, leads, contacts = 'auto', 'auto', 'auto', 'auto', [], []
                  
            for node in outer_node.childNodes:
                if node.nodeType == node.ELEMENT_NODE:
                    if node.nodeName == 'campaign':
                        campaign_id = get_xml_text(node)
                        
                    elif node.nodeName == 'template':
                        template_id = get_xml_text(node)                    
                    
                    elif node.nodeName == 'step':
                        step_num = get_xml_text(node)
                    
                    elif node.nodeName in ['email_from', 'sender']:
                        sender = get_xml_text(node)
                    
                    elif node.nodeName == 'lead':
                        leads.append(self._parse_person_xml(node))         

                    elif node.nodeName == 'contact':
                        contacts.append(self._parse_person_xml(node))       
                        
            log("FollowupHandler.create:")
            log("campaign: ", campaign_id)
            log("template: ", template_id)
            log("step_num: ", step_num)
            log("sender: ", sender)
            log("leads: ", leads)
            
            leads = [l for l in leads if self._is_valid_person(l)]
            contacts = [c for c in contacts if self._is_valid_person(c)]
            
            # Unmark any failed SF emails, if applicable
            emails = [p['email'] for p in (leads+contacts)]
            queryset_sync = Email.objects.filter(owner_account=alias.account)                        # These are the only messages you can see, ever
            queryset_sync = queryset_sync.filter(Q(owner_alias=alias) | Q(contacts=alias.contact) | Email.objects.twitter_q())    # TODO: Revisit this
            queryset_sync = queryset_sync.filter(date_sent__gte=n-7*ONE_DAY) 
            queryset_sync = queryset_sync.filter(email2remoteuser__remote_user=remote_user, email2remoteuser__status=Email2RemoteUser.STATUS_FAILED)
            queryset_sync = queryset_sync.filter(contacts__email__in=emails)
            
            for e in queryset_sync:
                log("FollowupHandler.create: Unmarking email ", e.pk, " as failed")
                e.email2remoteuser_set.filter(remote_user=remote_user, status=Email2RemoteUser.STATUS_FAILED).delete()
                            
            # Forward request to the followup server, if applicable
            
            if leads:
                rules = EmailRule.objects.active().filter(owner_account=alias.account, type=EmailRule.TYPE_FOLLOWUP, enabled_leads=True)
                for rule in rules:
                    api = rule.remote_user.get_api()
                    
                    for a in rule.actions():
                        if a['action'] == 'followup':
                            api.followup(leads, campaign=str(a.get('campaign', 'auto')), raw_data=None)
                            log("FollowupHandler.create: successfully forwarded to ", rule.remote_user.pk)
                            
            if contacts:
                rules = EmailRule.objects.active().filter(owner_account=alias.account, type=EmailRule.TYPE_FOLLOWUP, enabled_contacts=True)
                for rule in rules:
                    api = rule.remote_user.get_api()
                    
                    for a in rule.actions():
                        if a['action'] == 'followup':
                            api.followup(contacts, campaign=str(a.get('campaign', 'auto')), raw_data=None)
                            log("FollowupHandler.create: successfully forwarded to ", rule.remote_user.pk)            

        
            return {'success': True}
        
        except:
            log (traceback.format_exc())
            return {'success': False, 'msg': 'Cannot forward to followup server'}

                     