import re, pdb, traceback
from piston.handler import BaseHandler, AnonymousBaseHandler
from piston.utils import *
from assist.models import *
from piston.emitters import Emitter
from assist.payment import *

# API calls that don't require authentication

class PaymentNotifyHandler(AnonymousBaseHandler):
    '''
    Notification of a payment (or a failed payment) from the payment handler.
    Mark the given account appropriately (resetting the date_expires date) and
    send an invoice email.
    '''
        
    allowed_methods = ('POST')
    
    def create(self, request):
                
        # Do our own authentication here, caller just needs to provide an API key
        #  as the password part of the HTTP_AUTHORIZATION header
                
        try:            
            auth_string = request.META.get('HTTP_AUTHORIZATION', None)
            if not auth_string:
                raise NotifyUserException("You must provide an API key.")
            
            (authmeth, auth) = auth_string.split(" ", 1)
            if not authmeth.lower() == 'basic':
                return NotifyUserException("You must provide an API key.")
            
            auth = auth.strip().decode('base64')
            (username, secret) = auth.split(':', 1)
            
            if hashlib.md5(secret).hexdigest() != settings.PAYMENT_NOTIFY_APIKEY_HASH:
                raise NotifyUserException("You must provide an API key.")
                        
            result = handle_payment_notify(request.data)
            return result

        except:
            return {'success': False}
    
        