'''
Functions to communicate with the Calenvy.com API on another Calenvy server or on a followup server.
'''

import urllib2, urllib
import sys, re, json, base64, pdb, traceback, pytz, zlib
from urlparse import urlparse
from xml.dom import minidom
import utils
import settings
from emitters import XMLCDATAEmitter

API_PREFIX = '/api/'

API_LIST_PLUGINS = 'list_plugins'
API_GET_CREDENTIAL = 'get_credential'
API_LOGIN_API = 'login_api'
API_FOLLOWUP = 'followup'
API_CAMPAIGNS = 'campaigns'
#API_MESSAGES = 'messages'
#API_REGISTER = 'register'

TYPE_LEGACY             =   'tLEG'              # Outlook, Salesforce, Firefox; can also be used for web login
TYPE_BASIC_AUTH_TOKEN   =   'tBAS'              # Used in place of the password field for BasicAuth, but can't be used for web login
TYPE_BASIC_AUTH_PASSWORD =  'tPWD'              # Ordinary username/password auth
TYPE_OAUTH_ACCESS_TOKEN =   'tOAA'              # All plugins going forward will use this
TYPE_WEB_ONETIME_TOKEN  =   'tWEB'              # Token for logging into the website; can only be used once
TYPE_WEB_ONETIME_TOKEN_USED  =   'tWBx'         # Used-up token for logging into the website


class DomainError(Exception):       # bad URL passed to APIConsumer
    pass

class LoginError(Exception):        # bad username/password passed to APIConsumer (BasicAuth)
    def __init__(self, data=None):
        self.data = data

class TokenError(Exception):        # bad username/access_token passed to APIConsumer (OAuth)
    def __init__(self, data=None):
        self.data = data

class DataError(Exception):         # invalid (or no) JSON or XML returned from the other end
    pass

class FailureError(Exception):      # data failure from the other end
    pass

class OperationError(Exception):    # JSON/XML returned from other end, but it says success=False
    def __init__(self, data=None):
        self.data = data
    
class APIConsumer(object):
       
    def __init__(self, domain=None, username=None, password=None, token=None, anonymous=False):
        # Pass in domain, username and password if we're logging in to get the auth token
        
        assert(domain is not None)
        
        if '//' not in domain:
            domain = 'http://' + domain
        
        if username and domain and (password or token):
            self.domain = domain
            self.username = username
            self.password = password
            self.token = token
        elif anonymous:
            self.domain = domain
            self.username = None
            self.password = None
            self.token = None
        else:
            raise NotImplementedError
            
    def _add_auth(self, request, auth_type):
        if auth_type is None:           # some API calls don't need auth
            return
        elif auth_type == TYPE_BASIC_AUTH_PASSWORD:
            if (not self.username) or (not self.password):
                raise LoginError()
            authstr = base64.b64encode('%s:%s' % (self.username, self.password)).strip() # need strip() because encodestring adds \n
        elif auth_type == TYPE_BASIC_AUTH_TOKEN:
            if (not self.username) or (not self.token):
                raise TokenError()
            authstr = base64.b64encode('%s:%s' % (self.username, self.token)).strip()    # need strip() because encodestring adds \n
        #utils.log("Adding authorization header: ", authstr)
        request.add_header('Authorization', 'Basic %s' % authstr)
        
    def _get_text(self, element):
        if len(element.childNodes) > 0:
            return element.childNodes[0].wholeText
        else:
            return ''
        
    def _is_xml_ok_string(self, s):
        ctrl_chars = '\x09\x0A\x0D'
        return not any ((ord(c) < 32 and c not in ctrl_chars) or ord(c) > 127 for c in s) 
    
    def _set_text(self, doc, element, text):
        '''
        Set the text of a document element using CDATA or base64.
        If using CDATA, and text contains the special string ]]>, split it across 
        multiple CDATA sections to avoid trouble.
        Call this *before* appending the element to the document.
        '''
        if self._is_xml_ok_string(text):
            text_parts = text.split(']]>')
            for i, node_text in enumerate(text_parts):
                node_text = \
                    ('' if i == 0 else '>') +\
                    node_text +\
                    ('' if i == len(text_parts)-1 else ']]')
                cdata = doc.createCDATASection(node_text)
                element.appendChild(cdata)
        else:
            element.setAttribute('encoding', 'base64')
            if type(text) is unicode:
                text = text.encode('utf-8')
            text_node = doc.createTextNode(text.encode('base64'))
            element.appendChild(text_node)
        
    def _is_success_xml(self, doc):
        nodes = doc.getElementsByTagName('success')
        if nodes:
            return self._get_text(nodes[0]).lower() == 'true'
        return False        
    
    def _is_success_json(self, doc):
        return doc.get('success', False)
    
    def _decode(self, data, format):
        utils.log("_decode:", format, data)
        if data is None:
            return None
        elif format == 'xml':
            try:
                doc = minidom.parseString(data)
            except:
                raise DataError
            if not self._is_success_xml(doc):
                raise OperationError(doc)
            return doc
        elif format == 'json':
            try:
                doc = json.loads(data)
            except:
                raise DataError
            if not self._is_success_json(doc):
                raise OperationError(doc)
            return doc
    
    def _encode(self, data, format, use_gzip=True):
        if data is None:
            return None
        elif format == 'xml':
            data = str(data.toxml())
        elif format == 'json':
            data = str(json.dumps(data))
        if use_gzip:
            data = zlib.compress(data)
        return data
    
       
    def _add_content_type(self, request, is_post, format, use_gzip=True):
        if is_post:
            if format == 'xml':
                request.add_header('Content-Type', 'text/xml')
            elif format == 'json':
                request.add_header('Content-Type', 'application/json')  # text/json doesn't work with Piston
        if use_gzip:
            request.add_header('Content-Encoding', 'gzip')
        
    def _build_url(self, url, params):
        if not url.endswith('/'):
            url += '/'
        if params:
            url += '?' + urllib.urlencode(params)
        return url
            
    def _request(self, path, auth_type=TYPE_BASIC_AUTH_TOKEN, params=None, data=None, raw_data=None, format='json', use_gzip=True):
        try:
            url = self.domain + API_PREFIX + path
            if not url.endswith('/'):
                url += '/'
            url = self._build_url(url, params)
            if raw_data:
                # Send this literal data (preformatted XML, JSON etc.)
                request = urllib2.Request(url, raw_data)
            else:
                # data = None for GET request, otherwise POST request
                request = urllib2.Request(url, self._encode(data, format, use_gzip=use_gzip))
            #request.add_header('User-Agent', '%s/%s %s' % (settings.NAME, settings.VERSION, settings.DEFAULT_HELP_DOMAIN))
            self._add_auth(request, auth_type)              # Does nothing if auth_type is None
            self._add_content_type(request, bool(raw_data or data), format, use_gzip=use_gzip)   # Does nothing if data is None                    
            handle = urllib2.urlopen(request)
            data = handle.read()
            return self._decode(data, format)
        except urllib2.HTTPError, e:
            if e.code == 401:           # unauthorized
                data = e.read()
                raise LoginError(data=self._decode(data, format))
            else:
                raise DomainError
        except (urllib2.URLError, ValueError), e:
            log(e.reason)
            raise DomainError
        
    def _get(self, path, auth_type=TYPE_BASIC_AUTH_TOKEN, params=None, format='json', use_gzip=True):
        return self._request(path, auth_type=auth_type, params=params, format=format, use_gzip=use_gzip)
    
    def _post(self, path, data, auth_type=TYPE_BASIC_AUTH_TOKEN, params=None, format='json', use_gzip=True, raw_data=None):
        return self._request(path, auth_type=auth_type, params=params, format=format, data=data, use_gzip=use_gzip, raw_data=raw_data)
    
#    def _post(self, path, auth=True, format='json'):
#        try:
#            url = self.domain + API_PREFIX + path
#            if not url.endswith('/'):
#                url += '/'
#            request = urllib2.Request(url)
#            if auth:
#                self._add_basic_auth(request)
#            handle = urllib2.urlopen(request)
#            data = handle.read()
#            return self._decode(data, format)
#        except urllib2.HTTPError, e:
#            if e.code == 401:           # unauthorized
#                raise LoginError
#            else:
#                raise DomainError
#        except (urllib2.URLError, ValueError):
#            raise DomainError
        
    def list_plugins(self, required_install=False):
        return self._get(API_LIST_PLUGINS, auth_type=None, params={'target': settings.TARGET, 'version': settings.VERSION, 'required_install': str(int(required_install))}, format='json')
    
#    def followup(self, raw_data):
#        """Send a list of leads to a followup server.
#        For now, only passes through the given xml -- we're just forwarding a request from another server"""
#        
#        return self._post(API_FOLLOWUP, None, raw_data=raw_data, use_gzip=False, auth_type=TYPE_BASIC_AUTH_TOKEN, params={}, format='xml')

    def followup(self, leads, campaign='auto', step='auto', template='auto', raw_data=None):
        """
        Send a list of leads to a followup server.
        leads is a list of dictionaries as follows:
            {
                'first_name':        'Bob',
                'last_name':         'Jones',
                'company':           'MyCo',
                'source':            'web',
            }
        """

        if raw_data:
            return self._post(API_FOLLOWUP, None, raw_data=raw_data, use_gzip=False, auth_type=TYPE_BASIC_AUTH_TOKEN, params={}, format='xml')
        else:
            # We'll encode the data using XMLCDATAEmitter, which is better for this job than
            # _encode above 
            emit = XMLCDATAEmitter(None, None, None)
            data = [
                {'campaign': campaign},
                {'step': step},
                {'template': template}]
            for l in leads:
                data.append({'lead': l})
            
            xml = emit.render_data(data, enclose_tag='followup')    
            return self._post(API_FOLLOWUP, None, raw_data=xml, use_gzip=False, auth_type=TYPE_BASIC_AUTH_TOKEN, params={}, format='xml')

    def get_campaigns(self):
        """
        Get a list of followup campaigns from the server.
        Returns a structure like this:
            {
                "campaigns": [
                    {
                        "campaign": {
                            "id": 2, 
                            "name": "Testing"
                        }
                    }
                ], 
                "success": true
            }
        """
        return self._get(API_CAMPAIGNS, auth_type=TYPE_BASIC_AUTH_TOKEN, params={'format': 'json'}, format='json')
        
    def get_credential(self, remote_usertype, remote_userid='12345', remote_username='', cred_type=TYPE_BASIC_AUTH_TOKEN, account_wide=False):
        params = dict(
            remote_usertype=remote_usertype,
            remote_userid=remote_userid,
            remote_username=remote_username,
            cred_type=cred_type,
            account_wide='1' if account_wide else '0'
        )           
        return self._get(API_GET_CREDENTIAL, auth_type=TYPE_BASIC_AUTH_PASSWORD, params=params, format='json')
    
    def login_api(self):
        params = dict(
            version=settings.VERSION
        )           
        return self._get(API_LOGIN_API, auth_type=TYPE_BASIC_AUTH_TOKEN, params=params, format='json')
        
    def _format_dt(self, dt):
        if dt is None:
            return ''
        return dt.astimezone(pytz.UTC).strftime('%Y-%m-%d %H:%M:%S')
    