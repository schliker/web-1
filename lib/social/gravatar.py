import urllib, urllib2, hashlib
import base

GRAVATAR_URL = 'https://secure.gravatar.com/avatar.php'

class GravatarFinder(base.BaseFinder):
    "Look up an email address on Gravatar, just to find out whether Gravatar has a profile picture or not"
    
    def __init__(self, **kwargs):
        super(GravatarFinder, self).__init__(**kwargs)
        self.name = 'gravatar'
        
    
    def find(self, d):
        
        if 'email' not in d:
            return {}                      # without the email address, we can't do anything on gravatar
        
        try:
            email = d.get('email').strip().lower()
            
            # Do an image fetch from Gravatar that returns 404 if they don't have the image --
            #  this way we know whether to store the Gravatar URL or not
            params = {
                'gravatar_id':  hashlib.md5(email).hexdigest(), 
                'size':         str(self.default_image_size),
                'default':      '404'               # 404 error if the email address isn't recognized                                           
            }
            url = GRAVATAR_URL + '?' + urllib.urlencode(params)
                    
            response = urllib2.urlopen(url)
            
            #img_data = response.read()             # we don't need the raw image data here, it's the browser that will be
                                                    # rendering the image
            
            # If we get here (and there's no 404 error), Gravatar has the image
            # Construct another URL, one that doesn't specify a default URL, and return that
            #  (Later, the runtime data extractors will append a default URL as necessary)
            
            del params['default']
            url = GRAVATAR_URL + '?' + urllib.urlencode(params)
            
            result = {
                'image': {
                    'url':  url,
                    'size': (self.default_image_size, self.default_image_size)
                }
            }
            
            return result
        
        except urllib2.HTTPError:
            # Gravatar doesn't have that email address
            return {}
        
        except:
            # Some other problem
            return {}
        
        
        
            
