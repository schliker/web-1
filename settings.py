# Main Django settings file, for localhost
# At the end, we load the differences for our configuration from 'staging' or 'live'

import os, sys
print "Loading settings.py..."

SESSION_COOKIE_NAME = 'calenvy'                     # Do this to avoid conflicting with other Django projects on the same domain
SESSION_COOKIE_SECURE = False                       # For local site, can't require that all cookies use SSL
SESSION_COOKIE_AGE = 604800                         # One week, in seconds
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'   # Use Memcached as a write-through cache (if available)
# MUST RUN: "django-admin.py cleanup" to cleanup old sessions if you use the cache & db combo

# SESSION_SAVE_EVERY_REQUEST = True                   # TODO: Fix the Ajax calls so this isn't always necessary
CSRF_COOKIE_DOMAIN = None

PRODUCT_NAME = 'Calenut'                            # for the Firefox/Chrome plugin

DEBUG = False
TEMPLATE_DEBUG = DEBUG
PAYPAL_DEBUG = True
DEBUG_CHRONIC = True
ENABLE_SPAMD = False
LOG_ENABLED = True
MOSSO_FILES_ENABLED = False                          # by default, store files on Mosso
DISABLE_SMTP_EMAILS = False                          # to disable all outbound emails using some cheaper server setup (by cheaper, lazy)


PROFILER_ENABLED = True
PROFILER_KEY = 'kissprof'
PROFILER_MEM_KEY = 'kissmem'

from detect_server_type import server_type

# Set the API key for talking to the payment server globally
try:
    import payment_client
    payment_client.domain = 'http://payment.coml:7000'
    payment_client.apikey = 'UFYPZXXSWIVINVTUQOGR'
    payment_client.customer_id_prefix = sys.platform[0] + '.calenvy'  # prefix all remote customer ids with this prefix
except:
    print 'WARNING! Missing payment_client'                                                 #  (for local testing, include the first char of sys.platform)
PAYMENT_GATEWAY = 'authorize_net'                   # name of the default payment gateway on the payment server

OUTLOOK_PLUGIN_WIDTH = 230

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(PROJECT_DIR, "lib"))      # needed for our patched OpenID library to work right

LOG_DIR = os.path.normpath(os.path.join(PROJECT_DIR, os.pardir, 'logs'))
KEYS_DIR = os.path.join(PROJECT_DIR, 'data', 'keys')
KEYS_TRACKER_DIR = os.path.join(PROJECT_DIR, 'data', 'keys_tracker')
#KEYS_RSA_DIR = os.path.join(PROJECT_DIR, 'data', 'keys_rsa')

ADMINS = (
    ('Calenvy', 'calenvy@master.com'),
    # ('Your Name', 'your_email@domain.com'),
)

DEBUG_MASTER_PW_ENABLED = True
DEBUG_MASTER_PW_HASH = 'b095545465e6b69fbd89a82e234410eb'

PAYPAL_NOTIFY_URL = '/ipn/'                                         # PayPal notifies us at this URL of a payment
PAYMENT_NOTIFY_APIKEY_HASH = '5af72e528afe370ded1f5cacc3e7e61b'     # The (non-PayPal) payment server uses this API key to notify us of a payment

YAHOO_APPID = '4el2Z2LIkY3Qglv6nOzqw.v66gTOcGfBkJu8'
YAHOO_SHARED_SECRET = '05e08e706f33513986332dc858a29b5f'
YAHOO_NOTIFY_URL = 'yahoo_bbauth/'

SERVER = 'local'

API_SERVICE_DEFAULT = 'Pro'     # The service we give to people if they register over the API

NOTED_ROOT_URL = 'http://localhost:3636'
NOTED_ROOT_DOMAIN = 'localhost:3636'
NOTED_ROOT_NAME = 'Scheduler'

INTERNAL_CALLBACK_URL = 'http://localhost:3636'  # The API used by celery_worker to call us with messages

WARN_MAX_USERS_EXCEEDED = True     # Don't send warnings about too many users, until we're ready to enable this feature

MANAGERS = ADMINS

AUTH_PROFILE_MODULE = 'assist.AccountUser'


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.

INTRO_EMAIL_PREFIX = 'calenvy'
INTRO_NAME = 'Calenvy'

SERVER_EMAIL = 'calenvy@calenvy.coml'

# Where we send promotional emails from
if sys.platform == 'win32':
    SALES_ACCOUNT = 'calenvy'
    SALES_NAME = 'Moshe Rebbeinu'
    SALES_EMAIL = 'moishe@localhost'
else:
    SALES_ACCOUNT = 'calenvy'
    SALES_NAME = 'Moshe Rebbeinu'
    SALES_EMAIL = 'moishe@localhost'

# Contact that gets created automatically in everyone's account upon registration
SALES_AUTO_CONTACT = {
    'first_name':           'Calenvy',
    'last_name':            '',
    'company':              'Calenvy.com',
    'email':                'founders@calenvy.com',
    'twitter_screen_name':  '',
    'phone':                '',
    'website':              'calenvy.com',
    'title':                'Support',
    'description':          'Contact for technical support.'
}

TECH_AUTO_CONTACT = {
    'first_name':           'Calenvy',
    'last_name':            '',
    'company':              'Calenvy.com',
    'email':                'founders@calenvy.com',
    'twitter_screen_name':  '',
    'phone':                '',
    'website':              'calenvy.com',
    'title':                'Support',
    'description':          'Contact for technical support.'
}

DEBUG_ACCOUNTS = ['master']       # Refresh button is always enabled for these accounts

# On registration, don't allow auto-joining these accounts
DISALLOW_JOIN_ACCOUNT = ['cc', 'vijul']

# email settings
FROM_EMAIL = 'calenvy@calenvy.com'
TO_EMAIL = 'windysurf@mail.ee'
NOREPLY_EMAIL = 'calenvy@calenvy.com'

if sys.platform in ['darwin', 'win32']:
    ALL_DEBUG_EMAIL = 'bob@zmail.com'
    ALL_DEBUG_EMAIL_LOCAL = 'bob'
    INCOMING_DEBUG_EMAIL = 'alice@zmail.com'
    TWITTER_DEBUG_EMAIL = 'mary@zmail.com'
else:
    ALL_DEBUG_EMAIL = None
    ALL_DEBUG_EMAIL_LOCAL = None
    INCOMING_DEBUG_EMAIL = None

NOTED_DOMAIN_MAIN = 'localhost'               # For localhost testing

LEADSOURCE_IGNORE_PATTERNS = [
    r"(?i)calenvy\s*\(email\)",
]

SSL_DOMAINS = []                                 # None for local testing

SSL_URLS = ()

VALID_REDIRECT_URLS = [
    r'/dash/',
    r'/dashboard/',
    r'/info/',
    r'/login/',
    r'/dash/[a-z0-9_]+/settings/',
    r'/$'
]

IGNORABLE_404_STARTS = (                        # We get hits here all the time from spambots, just ignore them
    '/blog/',                                   #  and don't send mails to the admins
    '/2009/',
)


NOTED_USER_ADDRESSES = [                        # Actual people at noted.cc, don't ignore mail from these folks
    'calenvy',
    'founders',
    'support',
    'root',
    'abuse',
    'postmaster',
    'noted',
    'calenvy'
]

NOTED_DEBUG_ADDRESSES = [                       # @whatever.com, where whatever is any of our sites
    'digest.debug',
    'incoming.debug',
    'root.debug',
    'digest.debug',
    'twitter.debug',
    'noreply',
]


TWITTER_NOTED_ACCOUNTS = [
    'ccnoted',
    'notedcc',
    'testnoted2',
    'testnoted3',
    'calenvy'
]


IGNORE_OUTGOING_DOMAINS = []

IGNORE_INCOMING_DOMAINS = [\
    # Don't add twitter to this list; we need to get Twitter email notifications
    r'facebook\.com$', r'amazon\.com$', r'facebookmail\.com$', r'buy\.com$', r'linkedin\.com$', r'myspace\.com$', r'mint\.com$', r'posterous\.com$', r'otherinbox\.com', r'emailpizzahut\.com', r'craigslist\.(com|org|net)', r'paypal\.com'
]

ADMINS_SEE_EVERYTHING = False                 # Whether admins see all events in the newsfeed


# No desktop packaging locally
DESKTOP_PACKAGE_SVN_ROOT = None
if sys.platform == 'darwin':
    DESKTOP_PACKAGE_DIR = '/Users/calenvy/Cure3.5/uploads/desktop'
DESKTOP_PACKAGE_URL = '%s/static/uploads/desktop' % NOTED_ROOT_URL
DESKTOP_PACKAGE_EXTENSION = 'pkg'

DATABASE_ENGINE = 'sqlite3'       # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'db.sqlite'               # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''                   # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''                            # Set to empty string for default. Not used with sqlite3.

# Assistant SMTP server: a simple Python SMTP server that runs on Crawl1 (or locally), that accepts mails to the assistant
ASSISTANT_SMTPD_HOST = '127.0.0.1'            # The Assistant SMTP server should listen on this interface
ASSISTANT_SMTPD_PORT = 8025                   #  ... and on this port

# Import the Celery settings
from celeryconfig import *

# Where we keep files that are only stored on the static0 server (for now, just the company logo urls)
STATIC_LOCAL_DIR = os.path.normpath(os.path.join(PROJECT_DIR, os.pardir, 'local'))

TIME_ZONE = 'US/Pacific'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# Default site: calenvy.com
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = 'static'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '2_bzg3vtd#2-#i%)xcn1r6i=9228w+_$qh@-55s)*whi^i*9&4LOCAL'

LOGIN_URL = '/login/'

BLOG_PAGESIZE = 4           # 4 posts per page on /blog

#Filebrowser:
FILEBROWSER_URL_WWW = '/static/uploads/'
FILEBROWSER_PATH_SERVER = os.path.join(MEDIA_ROOT, 'uploads')
if sys.platform == 'darwin':
    UPLOADS_ROOT = '/Users/calenvy/Cure3.5/uploads'
else:
    UPLOADS_ROOT = None

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'mediums.mobile.load_template_source',  # wrapper for django.template.loaders.filesystem.load_template_source
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    #'utils.WsgiLogErrors',
    'django.middleware.gzip.GZipMiddleware', # for compressing dynamic content.
    'embed.middleware.JSONPMiddleware',         # return JavaScript containing JSON, if &callback=(whatever) in the URL
    'embed.middleware.URLMiddleware',       # fix paths for links/images, for the Chrome/Firefox plugin
    'lib.common.common.CommonMiddleware',    # modified version of common middleware to deal with http/https behind a load balancer correctly
    #'lib.csrf.middleware.CsrfMiddleware',    # protection against CSRF attacks (from Django 1.1, to deal with AJAX)
    'utils.MiddlewareResponseInjectP3P',     # P3P Compact Policy http headers so that we can be embedded in an iframe for IE
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'utils.SSLRedirect',                    # This doesn't work well with the load balancer. [eg 11/9]
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'mediums.mobile.RequestMiddleware',
    'mediums.mobile.MobileMiddleware',
    'requestmessages.middleware.RequestMessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware', # http://docs.djangoproject.com/en/dev/ref/contrib/flatpages/ - good for about us pages, etc.
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware', # if there is a 404, redirectmiddleware will check database to see where to send a user
    #'utils.ValidSiteMiddleware', # requires a valid site! much pains...
    #'lib.pympler.memmiddle.MemoryMiddleware',
    #'lib.profiler.profmiddle.ProfileMiddleware'
)

AUTHENTICATION_BACKENDS = (
    'api.auth.LegacyDummyAuthBackend',          # dummy authentication backend -- we do password checking elsewhere
    'django.contrib.auth.backends.ModelBackend',
    #'openid_auth.auth_backends.OpenIDBackend',      # TODO: Enable this when we're ready for it
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    # "django.core.context_processors.static",
    # "django.contrib.static.context_processors.static",
    #'mobileadmin.context_processors.user_agent',
    'requestmessages.context_processors.messages',
    'utils.global_context',
)

TWITTER_KEYS = {
    'localhost': ('KDhGMpLNi2oeEbyu48gVHg', 'IhuOYNlzXlU0yrfTjrNqrDXKjGAV0iJTH1ApRJGJ8'),    # Noted.cc application [8/4]
    #'noted.ccl': ('YBfcclyM2XULlwWZoQYJA', 'qwlyWoquHg6WscXwgtmK7vrKPli7efvtsADX1kphAk'),
    #'noted.cc': ('2ovEt0fERjlDgZfcsg4ghA', 'JbHmMevmdIGLjFvVc7BrzYUjmezHjhmbYyK7cIQHg')
    'localhost': ('KDhGMpLNi2oeEbyu48gVHg', 'IhuOYNlzXlU0yrfTjrNqrDXKjGAV0iJTH1ApRJGJ8')     # Noted.cc application [8/4]
}

FOLLOWUP_CAMPAIGN_ANYQUESTIONS = 1                      # Someone signs up for Calenvy, or installs the app on Salesforce
FOLLOWUP_CAMPAIGN_CANCELLATION = 15                     # Someone cancels via Paypal
FOLLOWUP_CAMPAIGN_PAYMENT_FAILED = 263                  # A Paypal payment fails
FOLLOWUP_CAMPAIGN_CREDIT_CARD_EXPIRED = 410             # A credit card expires

# If people have installed any of these products from AppExchange, auto-create a followup lead
# These regular expressions are tested in order. If you need a more specific one to take priority over a more general one
#  (i.e., "Calenvy Desktop" vs. "Calenvy", it must go at the top!)
SFDC_FOLLOWUP_CAMPAIGNS = {
    r'Cure':                1,                          # The Calenvy app on Salesforce, or the Calenvy desktop app
    r'Magnify Gmail':       1,
    r'Email Follow-up':     5,
    r'Follow-up':           5                           # The Followup app in Salesforce, or the Followup app for Firefox/Chrome
}

# Check this Salesforce account for recently auto-created leads
SFDC_LOGIN_EMAIL = 'founders@calenvy.com'
SFDC_LOGIN_PASSWORD = 'salesforce45'
SFDC_LOGIN_TOKEN = '02fSY8kaBGfLoycPNeF75Hst'

FIREFOX_SERVICE_NAME = 'Magnify'                        # For Firefox/Chrome plugin, registrations automatically get this service level

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    './network/templates',
    './network/templates/flatpages',
    './network/templates/blog',
    './network/templates/admin',
    './network/templates/feeds',
    './embed/templates',
    './openid_auth/templates',
    '/home/bluetooth-speakers/.virtualenvs/schedulocity/lib/site-packages/Django/contrib/admin/templates',
    '/usr/local/lib/python2.7/dist-packages/django/contrib/admin/templates'

)

CACHE_BACKEND = 'memcached://127.0.0.1:11211/'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.admin',
    # 'django.contrib.static',
    'django.contrib.redirects',
    'basic.blog',
    'basic.inlines',
    'django.contrib.comments',
    'django.contrib.markup',
    'django.contrib.flatpages',
    'south',
    'mediums',
    'requestmessages',
    'assist',
    'network',
    'schedule',
    'twitter',
    'crawl',
    'remote',
    'embed',
    'openid_auth',
    'tagging',
    'piston',
    'api',
    'plugins',
    'paypal.standard',
    #####'djcelery',
    #'django.contrib.sitemaps',
    #'deploy',
    #'mobileadmin',          # to manage the /admin area via mobile phone
    #'sorl.thumbnail',
    #'filebrowser',
)

#try:
#    import lib.uwsgi_admin
#    INSTALLED_APPS += ('lib.uwsgi_admin',)
#except ImportError:
#    pass

MOSSO_API_USERNAME = 'noted'
MOSSO_API_KEY = '840c394e94da907451c72c9567dfd144'
MOSSO_MESSAGES_ENABLED = True       # whether to use Cloud files to store email message dictionaries
MOSSO_FILES_ENABLED = True         # whether to use Cloud Files to store file attachments
MOSSO_USE_SERVICENET = False        # whether to use internal IP when connecting to Cloud Files

DESKTOP_PLUGIN_SVN_URL = 'http://vijul.com/svn/'

if sys.platform == 'darwin':
    USE_MULTITHREADED_SERVER = True
    FILE_STORE_PATH = '/Users/Shared/Noted_files'
    QUEUE_PATH = '/Users/Shared/Noted_queue'
    ALLOWED_SEND_EMAIL_DOMAINS = ['master.com', 'disaster.com', 'plaster.com', 'zmail.com']
elif sys.platform == 'win32':
    USE_MULTITHREADED_SERVER = False
    FILE_STORE_PATH = 'C:\\dev\\snipd_django\\files'
    QUEUE_PATH = 'C:\\dev\\snipd_django\\queue'
    ALLOWED_SEND_EMAIL_DOMAINS = ['master.com', 'disaster.com', 'plaster.com']      # allow all
else:
    FILE_STORE_PATH = '/home/noted/files'
    QUEUE_PATH = '/home/noted/queue'
    ALLOWED_SEND_EMAIL_DOMAINS = True           # allow all

DISALLOWED_SEND_EMAIL_DOMAINS = ['twitter.com'] # Don't send anything to Twitter.com

ACCOUNT_DEMO = 'demo'                           # Name of the special demo account


# Load the differences from this configuration file on top
from detect_server_type import server_type
if server_type == 'live': from live import *
elif server_type == 'staging': from staging import *
elif server_type == 'local': pass
else: print 'Bad server_type %s' % server_type; sys.exit(1)


# Set any constants that depend on flags in the other configuration file
if PAYPAL_DEBUG:
    PAYPAL_RECEIVER_EMAIL = 'calenvy.asdfasdf@calenvy.com'        # Sandbox seller
    PAYPAL_URL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'
else:
    PAYPAL_RECEIVER_EMAIL = 'founders@calenvy.com'
    PAYPAL_URL = 'https://www.paypal.com/cgi-bin/webscr'


if LOG_ENABLED:
    filename = os.path.join(LOG_DIR, '%s.log' % SERVER)
    LOG_FILE = open(filename, 'a')
    try:
        # Both noted and www-data should be able to access this file
        os.chmod(filename, 0770)
    except:
        pass
else:
    # No logging, just dump to stdout (for interactive mode, etc.)
    LOG_FILE = sys.__stdout__
