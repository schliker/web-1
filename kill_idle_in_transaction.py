#!/usr/bin/env python2.6
# Run this as user postgres.

import os, time

PID_INDEX = 1                       # which column of ps -ef output contains the pid
TIME_INDEX = 6                      # which column of ps -ef output contains the elapsed time
HOWLONG_IDLE_TO_KILL = 30           # in minutes
CMD_TO_KILL = 'idle in transaction'


# Kill only one wayward "idle in transaction" at a time.
# Often killing one kills the rest.

def pid(line):
    return line.split()[PID_INDEX]
    
def cputime(line):
    parts = line.split()[TIME_INDEX].split(':')
    seconds = float(parts[-1]) if (len(parts) > 0) else 0
    minutes = int(parts[-2]) if (len(parts) > 1) else 0
    hours = int(parts[-3]) if (len(parts) > 2) else 0
    total_minutes = hours*60 + minutes    
    return total_minutes

def main():
    while True:       
        lines = os.popen('ps -ef').readlines()
        headers = lines[0].split()
        if headers[PID_INDEX] != 'PID' or headers[TIME_INDEX] != 'TIME':
            print "Error: ps -ef columns weren't what I was expecting!"
            sys.exit()
    
        ps_to_kill = [pid(line) for line in lines if CMD_TO_KILL in line and cputime(line) > HOWLONG_IDLE_TO_KILL]
    
        if ps_to_kill:
            # Kill the first 'idle in transaction' process we find
            cmd = 'kill -9 %s' % ps_to_kill[0]
            print "Doing this: ", cmd
            os.system(cmd) 
        
            # Maybe killing it will kill others... wait for a bit
            time.sleep(5)
        
        else:
            # No more processes to kill, we're done
            break       
        
if __name__ == '__main__':
    main()

        
        