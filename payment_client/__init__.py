'''
Library to communicate with the Payment API.
This is built as an egg file and included with other stuff.

Functions to communicate with the Payment API.
'''

import urllib2, urllib
import sys, re, json, base64, pdb, traceback, zlib
from urlparse import urlparse
from xml.dom import minidom
from functools import wraps

PAYMENT_DEFAULT_DOMAIN = 'https://billing.calenvy.com'
PAYMENT_API_PREFIX = '/api'
PAYMENT_API_CHARGE = '/charge'
PAYMENT_API_GATEWAY_PROFILE = '/gateway/profile'
PAYMENT_API_SUBSCRIPTION_STATUS = '/subscription/status'

# Transaction types
TRANSACTION_TYPE_AUTH_CAPTURE =          'auth_capture'          # default type
TRANSACTION_TYPE_AUTH_ONLY =             'auth_only'
TRANSACTION_TYPE_CAPTURE_ONLY =          'capture_only'
TRANSACTION_TYPE_CREDIT =                'credit'
TRANSACTION_TYPE_PRIOR_AUTH_CAPTURE =    'prior_auth_capture'
TRANSACTION_TYPE_VOID =                  'void'

# Subscription status
SUBSCRIPTION_STATUS_ACTIVE =            'sACT'
SUBSCRIPTION_STATUS_CANCELLED =         'sCAN'

class PaymentConsumer(object):
    '''
    Class for communicating with the payment API.
    Usually you'll just use one of the convenience functions defined below, outside of this class.
    '''
    
    def __init__(self, apikey, domain):
        self.domain = domain
        self.apikey = apikey
       
    def _add_auth(self, request):
        request.add_header('Authorization', self.apikey)
      
    def _get_text(self, element):
        if len(element.childNodes) > 0:
            return element.childNodes[0].wholeText
        else:
            return ''
        
    def _is_xml_ok_string(self, s):
        ctrl_chars = '\x09\x0A\x0D'
        return not any ((ord(c) < 32 and c not in ctrl_chars) or ord(c) > 127 for c in s) 
    
    def _set_text(self, doc, element, text):
        '''
        Set the text of a document element using CDATA or base64.
        If using CDATA, and text contains the special string ]]>, split it across 
        multiple CDATA sections to avoid trouble.
        Call this *before* appending the element to the document.
        '''
        if self._is_xml_ok_string(text):
            text_parts = text.split(']]>')
            for i, node_text in enumerate(text_parts):
                node_text = \
                    ('' if i == 0 else '>') +\
                    node_text +\
                    ('' if i == len(text_parts)-1 else ']]')
                cdata = doc.createCDATASection(node_text)
                element.appendChild(cdata)
        else:
            element.setAttribute('encoding', 'base64')
            if type(text) is unicode:
                text = text.encode('utf-8')
            text_node = doc.createTextNode(text.encode('base64'))
            element.appendChild(text_node)
        
    def _is_success_xml(self, doc):
        nodes = doc.getElementsByTagName('success')
        if nodes:
            return self._get_text(nodes[0]).lower() == 'true'
        return False        
    
    def _is_success_json(self, doc):
        return doc.get('success', False)
    
    def _decode(self, data, format):
        if data is None:
            return None
        elif format == 'xml':
            try:
                doc = minidom.parseString(data)
            except:
                raise DataError
            return doc
        elif format == 'json':
            try:
                doc = json.loads(data)
            except:
                raise DataError
            return doc
    
    def _encode(self, data, format, use_gzip=False):
        if data is None:
            return None
        elif format == 'xml':
            data = str(data.toxml())
        elif format == 'json':
            data = str(json.dumps(data))
        if use_gzip:
            data = zlib.compress(data)
        return data
    
    def _add_content_type(self, request, is_post, format, use_gzip=False):
        if is_post:
            if format == 'xml':
                request.add_header('Content-Type', 'text/xml')
            elif format == 'json':
                request.add_header('Content-Type', 'application/json')  # text/json doesn't work with Piston
        if use_gzip:
            request.add_header('Content-Encoding', 'gzip')
        
    def _build_url(self, url, params):
        if not url.endswith('/'):
            url += '/'
        if params:
            url += '?' + urllib.urlencode(params)
        return url
            
    def _request(self, path, params=None, data=None, raw_data=None, format='json', use_gzip=False):
        try:
            url = self.domain + PAYMENT_API_PREFIX + path
            if not url.endswith('/'):
                url += '/'
            url = self._build_url(url, params)
            if raw_data:
                # Send this literal data (preformatted XML, JSON etc.)
                request = urllib2.Request(url, raw_data)
            else:
                # data = None for GET request, otherwise POST request
                request = urllib2.Request(url, self._encode(data, format, use_gzip=use_gzip))
            #request.add_header('User-Agent', '%s/%s %s' % (settings.NAME, settings.VERSION, settings.DEFAULT_HELP_DOMAIN))
            self._add_auth(request)           
            self._add_content_type(request, bool(raw_data or data), format, use_gzip=use_gzip)   # Does nothing if data is None                    
            handle = urllib2.urlopen(request)
            data = handle.read()
            return self._decode(data, format)
        except urllib2.HTTPError, e:
            if e.code == 401:           # unauthorized
                data = e.read()
                raise LoginError(data=self._decode(data, format))
            else:
                raise DomainError
        except (urllib2.URLError, ValueError), e:
            print e.reason
            raise DomainError
        
    def _get(self, path, params=None, format='json', use_gzip=False):
        return self._request(path, params=params, format=format, use_gzip=use_gzip)
    
    def _post(self, path, data, params=None, format='json', use_gzip=False, raw_data=None):
        return self._request(path, params=params, format=format, data=data, use_gzip=use_gzip, raw_data=raw_data)
    
    def charge(self, price, remote_customer, item):
        """
        Charge a certain amount of money to a payer.
        
        remote_customer: a string uniquely identifying the payer (customer),
            format is whatever the caller wants
        
        item: a JSON-encodable (but not encoded) structure identifying the item,
            format is whatever the caller wants
            used later to generate invoices
            
        price: a Decimal object with the price in USD, may be positive or negative
        """
        
        data = {
            'remote_customer': str(remote_customer),
            'item':     item,
            'price':    str(price)
        }
                
        return self._post(PAYMENT_API_CHARGE, data)
    
    
    def get_profile(self, customer_profile_id):
        """
        Get a customer profile stored on the payment gateway.
        The credit card number and CVV number are partially masked in the result.
        
        customer_profile_id: the id used on the gateway.        
        """
        
        data = {
            'customer_profile_id': customer_profile_id
        }
           
        return self._get(PAYMENT_API_GATEWAY_PROFILE, data)
    
    def set_profile(self, **kwargs):
        """
        Create or update a customer profile stored on the payment gateway.
        If customer_profile_id=None (or not specified), create a new one,
         else update the specified profile
        """
        
        return self._post(PAYMENT_API_GATEWAY_PROFILE, kwargs)

    def set_subscription_status(self, **kwargs):
        """
        Change the status of a Subscription object.
        Can be used to disable (inactivate) a subscription, i.e., when the correponding account 
        on Calenvy or Follow-up Robot is deleted.
        """
            
        return self._post(PAYMENT_API_SUBSCRIPTION_STATUS, kwargs)
    
    def get_subscription_status(self, **kwargs):
            
        return self._get(PAYMENT_API_SUBSCRIPTION_STATUS, kwargs)
    
        
# Various errors that can be thrown by PaymentConsumer

class DomainError(Exception):       # bad URL passed to PaymentConsumer
    pass

class LoginError(Exception):        # bad apikey passed to PaymentConsumer (BasicAuth)
    def __init__(self, data=None):
        self.data = data

class DataError(Exception):         # invalid (or no) JSON or XML returned from the other end
    pass

class FailureError(Exception):      # data failure from the other end
    pass
    

# Global convenience functions to access the API

# Global domain and apikey
domain = 'https://billing.calenvy.com'      # default - don't change this
apikey = None                               # Set this to your apikey like this: client.apikey = 'BLAHBLAHBLAHBLAHBLAH'
customer_id_prefix = None                   # If set, prepend customer ids with this

def get_api():
    if not apikey:
        raise Exception("Must specify an apikey.")
        
    return PaymentConsumer(apikey, domain)


def _charge_api(price, remote_customer, item):
    """Helper function used by charge function below."""
    
    if not apikey:
        raise Exception("Must specify an apikey.")
        
    api = get_api()
    return api.charge(price, remote_customer, item)



def object_id(arg, prefix=None):
    """
    Try to extract a useful human-readable string identifier from a Python object
    Returns '' if it can't
    """
    
    # What to do with objects
    name_parts = [] 
    if hasattr(arg, '__class__'):
        name_parts.append(arg.__class__.__name__)
    if hasattr(arg, '__name__'):
        name_parts.append(arg.__name__)
    if hasattr(arg, 'id'):
        name_parts.append(str(arg.id))
    if name_parts:
        if prefix:
            name_parts[0:0] = [prefix]
        return '.'.join(name_parts)
    return None




def json_friendly(arg):
    """
    Turn an object into a structure suitable for turning it into a reasonable JSON unique identifier
    as best as it can (keeping strings to a reasonable length)
    """
    
    if isinstance(arg, basestring):
        return arg[:128]
    elif isinstance(arg, (int, float)):
        return arg
    elif isinstance(arg, (list, tuple)):
        return [json_friendly(x) for x in arg]
    elif isinstance(arg, dict):
        return dict((key, json_friendly(value)) for key, value in arg.items())
    
    return object_id(arg)

        
def get_item_for_decorator(target, args, kwargs):
    # Create a JSON-friendly structure out of a function name, its arguments and keyword arguments.
    
    item = {
        'name': object_id(target),
        'args': json_friendly(args),
        'kwargs': json_friendly(kwargs)
    }
    return item


def get_attribute(obj, attr):
    """
    Fancy version of Python's getattr, inspired by Django's template syntax.
    Takes a period-separated sequence of keys and performs a recursive lookup.
    
    - Callables are automatically called (they must take no parameters)
    - Dictionaries have their keys looked up, instead of an attribute lookup
    
    Example:
        get_attribute(obj, 'user.get_profile.account.id')
            returns obj.user.get_profile().account.id
            
        get_attribute(obj, '')
            returns obj
    """
         
    if not attr:
        return obj
    else:
        if '.' in attr:
            key, remain = attr.split('.', 1)
        else:
            key, remain = attr, ''
        if isinstance(obj, dict):
            value = obj.get(key)
        else:
            value = getattr(obj, key)
        if callable(value):
            value = value()
        return get_attribute(value, remain)
    

    
def charge(*args, **kwargs):
    """
    Convenience function to charge money to a payer using the global apikey.
    
    Can be called either as a regular function or as a decorator -- it auto-detects
    based on the number of parameters.
    
    Regular function:
        charge(price, remote_customer, item)
        Price: A string, float, or Decimal object
        Remote_customer: a string representing who is actually transacting the item
        Item: A Json-friendly structure representing the item being transacted
        
    Decorator:
        @charge(price, remote_customer_attr)
        remote_customer_attr: The name of the attribute to extract from the 
        wrapped function's first argument, determining the unique customer
        to whom we are billing this transaction.
    
    Raises exception if it can't do the charge, otherwise returns a dictionary
    as follows:charge 2
    {
        'success':    True,
        ... (other things returned from the billing system)
    }
    """
        
    if len(args) == 3:
        # Called as a regular function
        price, remote_customer, item = args
        return _charge_api(price, object_id(remote_customer, prefix=customer_id_prefix), json_friendly(item))
    
    elif len(args) == 2:
        # Called as a decorator
        price, remote_customer_attr = args
        
        def decorator(target):
    
            def wrapper(*args, **kwargs):
                        
                item = get_item_for_decorator(target, args, kwargs)
                remote_customer = object_id(get_attribute(args[0], remote_customer_attr), prefix=customer_id_prefix)                
                _charge_api(price, remote_customer, item)       # Raises exception if trouble
                
                return target(*args, **kwargs)
    
            return wrapper
    
        return decorator




